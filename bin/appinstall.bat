@echo off
@if not "%appinstall_bat_dbg%" geq "2" echo off
rem Copyright (C) 2022 Omid (gitlab.com/0mid)

rem This program is free software: you can redistribute it and/or
rem modify it under the terms of the GNU General Public License as
rem published by the Free Software Foundation, either version 3 of the
rem License, or (at your option) any later version.

rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.

rem You should have received a copy of the GNU General Public License
rem along with this program.  If not, see <http://www.gnu.org/licenses/>.

setlocal

rem Tested on Windows 10.0.19042.1645, which comes with tar (bsdtar
rem 3.5.2) and curl (curl 7.79.1 (Windows)) in C:\Windows\System32.
rem See https://stackoverflow.com/a/50200838 and
rem https://devblogs.microsoft.com/commandline/tar-and-curl-come-to-windows.

set app_list=7zip git emacs firefox sumatrapdf xournal cygwin libreoffice notepadpp python pythonmodules mpv vlc w64devkit conemu zeal keepassxc autohotkey sharpkeys aptcyg msys2 xournalpp miktex minttysolarized youtube-dl yt-dlp freetube startup.bat explorerpp wsl wsltty wslinit sudo quickaccess jpegview flameshot vscodium r rstudio envgui syncthing drmemory vscode PATH
set apps_with_path=git cygwin 7zip hunspell miktex python r drmemory

set app=%~1
if "%app%" == "" call :_usage& exit /b 1
set ver=%~2

set config=%AppData%
set startMenuProgs=%AppData%\Microsoft\Windows\Start Menu\Programs
set startMenuStartup=%startMenuProgs%\Startup
set startMenu_opt=%startMenuProgs%\opt

rem Just not to rely on init.bat having been run (or taken effect in
rem this cmd process) yet.
set home=%USERPROFILE%
set curdir=%~dp0

set software=%home%\software
set opt=%home%\opt
set bin=%home%\bin
call :_mkdir "%software%"
call :_mkdir "%opt%"
call :_mkdir "%bin%"

cd /d "%software%"

rem Do NOT quote %app%; the quotes are not removed and we are hit
rem by the error: The system cannot find the batch label specified -
rem "...".
call :%app%
exit /b %errorlevel%

:_usage
set batchFileWExt=%~nx0
echo Usage: %batchFileWExt% SOFTWARE
echo where SOFTWARE is one of "%app_list%".
echo In particular, PATH means just append the PATH user environment
echo variable with paths to "%apps_with_path%" (if not already on the PATH).
echo.
echo If needed, edit the PATH manually as needed through the Windows GUI
echo accessible, e.g., directly by running
echo    rundll32 sysdm.cpl,EditEnvironmentVariables
exit /b

:paths
:path
set setPathOnly=true
call :_pathappend "%bin%"
for %%p in (%apps_with_path%) do call :%%p
reg query HKCU\Environment /v Path
set path_bat=bin\path.bat
call "%path_bat%" delAppPaths
call :envgui
set envgui_bat=%bin%\envgui.bat
call "%envgui_bat%"
exit /b

:_pathappend
set p=%~1
path | find /i "%p%" >nul || set path=%path%;%p%
rem setx writes variables to the master environment in the registry.
rem Variables set with setx are available in future command windows
rem only, NOT in the current command window.
rem
rem There's a limit of 1024 characters when assigning
rem contents to a variable using setx. To get around the 1024-char
rem limitation of setx, we will modify the appropriate key/val in
rem registery directly here.
rem
rem Note that env vars in Windows are NOT case-sensitive; 'path' and
rem 'PATH' represent the same env var. Here we're using our own
rem convention, with lower case 'path' to denote a locally set var (due to
rem the 'setlocal' above and the per-cmd-console effect of 'set') and
rem UPPER-CASE 'PATH' when we pass the value formed in 'path' to setx
rem to be set permanently and globally.
reg add HKCU\Environment /f /v Path /t REG_SZ /d "%path%" >nul

rem Trigger a broadcast of WM_SETTINGCHANGE message, which
rem (hopefully!) should cause non-cmd (Windowed/GUI) programs to read
rem the new PATH env var. setx, e.g., does broadcast this message
rem after it adds PATH data to registery; we are using this
rem otherwise-useless setx of a var (e.g., USERNAME) to itself solely
rem for setx's WM_SETTINGCHANGE side-effect [See comments in
rem https://superuser.com/a/387625].
setx USERNAME "%USERNAME%" >nul
exit /b

rem Usage example: in a regular (non-Admin) cmd:
rem sudo taskkill /f /im wslservice.exe
rem
rem https://stackoverflow.com/a/40321310
rem https://webinstall.dev/sudo
:sudo
:sudo.bat
if "%setPathOnly%" == "true" goto :_sudo_path
call :_sudo.bat >"%bin%\sudo.bat"
:_sudo_path
call :_pathappend "%bin%"
exit /b

:_sudo.bat
echo.@echo off
echo.powershell -Command "Start-Process cmd -Verb RunAs -ArgumentList '/c cd /d %%CD%% && %%*'"
echo.@echo on
exit /b

:envgui
:envgui.bat
if "%setPathOnly%" == "true" goto :_envgui_path
call :_envgui.bat >"%bin%\envgui.bat"
:_envgui_path
call :_pathappend "%bin%"
exit /b

:_envgui.bat
echo.start rundll32 sysdm.cpl,EditEnvironmentVariables
exit /b

:quickaccess
set defaultfolders=%home%\Documents,%home%\Downloads,%home%\Desktop,%home%\Pictures,%home%\Videos,%home%\Music
set pinfolders=%SYSTEMDRIVE%,%home%,%home%\docs,%home%\opt,%home%\software,%home%\backup,%home%\literature,%home%\multimedia,%TEMP%,%home%\temp
set quickaccess_bat=%curdir%quickaccess.bat
call "%quickaccess_bat%" default
call "%quickaccess_bat%" unpin %defaultfolders%
call "%quickaccess_bat%" pin %pinfolders%
exit /b

rem function for creating shortcuts
:_shortcut
call :_mkdir "%startMenu_opt%"
set srcFilePath=%~1
set shortcutFilePathNoExt=%~2
set vbscript=%TEMP%\shortcut-%RANDOM%-%RANDOM%.vbs

if not exist "%srcFilePath%" exit /b
set lnk=%shortcutFilePathNoExt%.lnk
if exist "%lnk%" del "%lnk%"
call :_shortcut_vbscript >"%vbscript%"
cscript /nologo "%vbscript%"
del "%vbscript%"
exit /b

:_shortcut_vbscript
echo.Set oWS = WScript.CreateObject("WScript.Shell")
echo.sLinkFile = "%shortcutFilePathNoExt%.lnk"
echo.Set oLink = oWS.CreateShortcut(sLinkFile)
echo.oLink.TargetPath = "%srcFilePath%"
echo.oLink.Save
exit /b

:_mkdir
set d=%~1
if not exist "%d%" mkdir "%d%"
exit /b

:_ln-f
set src=%~1
set dst=%~2
if exist "%dst%" del "%dst%"
:_ln
set src=%~1
set dst=%~2
rem symlink (no /h) would require admin! (Windows: proprietary &
rem stupid, exhibit N)
mklink /h "%dst%" "%src%"
exit /b

:_curl
set url=%~1
set file=%~2

rem -L: follow redirects
set curl=curl -L --ssl-revoke-best-effort
if not exist "%file%" %curl% "%url%" -o "%file%"
exit /b

rem The key mappings done by Autohotkey have no effect in programs run
rem as Admin.
:autohotkey
set app=autohotkey
set ver=2.0.12
set zip=%app%_%ver%.zip
set releases_url=https://github.com/autohotkey/autohotkey/releases
set url=%releases_url%/download/v%ver%/%zip%
set dir=%opt%\%app%-%ver%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
set autohotkey_exe=%dir%\%app%64.exe
call :_shortcut "%autohotkey_exe%" "%startMenu_opt%\%app%"

set configdir=%config%\%app%
call :_mkdir "%configdir%"
set mappingScript=%configdir%\mappings.ahk
call :_autohotkey_mappings >"%mappingScript%"

set startupvbs=%configdir%\run_mappings.vbs
call :_autohotkey_startupvbs >"%startupvbs%"
cscript /b /nologo "%startupvbs%"

set startupShortcut=%startMenuStartup%\autohotkey_mappings
call :_shortcut "%startupvbs%" "%startupShortcut%"
exit /b

rem Intentionally writing this fundamental mapping here, as part of
rem this self-contained batch file, (rather than a separate file to be
rem copied) to make sure a Windows system could be bootstrapped
rem conveniently (e.g., without/before having git to be able to clone
rem the dotfiles repo, or otherwise having to download multiple
rem files).
:_autohotkey_mappings
rem do NOT quote; Windows' cmd (a joke of a 'shell') preserves quotes
rem there and echo "x" stupidly gives "x", which would not be accepted
rem by Autohotkey.
echo.#SingleInstance
echo.#Warn ; Enable warnings to assist with detecting common errors.
echo.
echo.Capslock::LCtrl
echo.RWin::LCtrl
exit /b

:_autohotkey_startupvbs
echo.Set WshShell = WScript.CreateObject("WScript.Shell")
echo.WshShell.Run "%autohotkey_exe% %mappingScript%", 0
echo.Set WshShell = Nothing
exit /b

:sharpkeys
rem Needs admin, as it HAS TO (as in, Microsoft doesn't provide a
rem per-user setting) write under HKLM\...\Keyboard Layout.

rem Just to figure out the keycodes for my keyboard mappings more
rem easily and use in my stand-alone init-keyboard.bat.
set version=3.9.4
set ver=394
set zip=sharpkeys%ver%.zip
set releases_url=https://github.com/randyrants/sharpkeys/releases
set url=%releases_url%/download/v%version%/%zip%
set dir=%opt%\sharpkeys-%version%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\SharpKeys.exe" "%startMenu_opt%\sharpkeys"
exit /b

rem non-free (non-libre) software by Microsoft, only to be used for
rem mitigating/disabling Windows' malfeatures.
:procmon
set version=4.01
set zip=ProcessMonitor.zip
set zipversion=ProcessMonitor-%version%.zip
set url=https://download.sysinternals.com/files/%zip%
set dir=%opt%\non-libre\procmon-%version%
call :_mkdir non-libre
cd non-libre
call :_curl "%url%" "%zipversion%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zipversion%"
call :_shortcut "%dir%\Procmon64.exe" "%startMenu_opt%\procmon"
exit /b

:firefox-esr
rem To get Firefox to work on an enterprise/corporate network and its
rem internal sites, in =about:config= add/modify boolean entry
rem =security.enterprise_roots.enabled= and set it to =true=. Without
rem that, internal sites appear untrusted. Also, set
rem 'network.negotiate-auth.delegation-uris' and
rem 'network.negotiate-auth.trusted-uris' to the right values for the
rem enterprise so that Firefox does not prompt for login credentials
rem for the organization's internal websites.
rem
rem To use a profile (usually stored in
rem %appdata%\Mozilla\Firefox\Profiles on Windows) from a different
rem Firefox, 'browse' about:profiles.
set app=firefox-esr
set ver=128.7.0esr
set app_ver=firefox-%ver%
set setup=Firefox%%20Setup%%20%ver%.exe
set setuplocal=%app_ver%.exe
@rem In browser, https://ftp.mozilla.org/pub/firefox/releases gives '404 Not Found'
@rem In browser, https://ftp.mozilla.org/pub/firefox/releases/ works!
set releases_url=https://ftp.mozilla.org/pub/firefox/releases
set url=%releases_url%/%ver%/win64/en-US/%setup%
rem %%20 in setup (replacing spaces Mozilla dummies put in the .exe
rem name for curl) needs extra escaping to correctly go to call
rem :_curl, a headache I didn't want to have.
curl -L "%url%" -o "%setuplocal%"
rem Firefox self updates; do NOT put %ver% in dir
set dir=%opt%\%app%
call :_mkdir "%dir%"
7z x -y "%setuplocal%" -o"%dir%"
call :_shortcut "%dir%\core\firefox.exe" "%startMenu_opt%\%app%"
del /f "%dir%\setup.exe"
exit /b

rem To install extensions, install
rem https://github.com/NeverDecaf/chromium-web-store, change the flag
rem chrome://flags/#extension-mime-request-handling to 'Always prompt
rem for install' and restart ungoogled-chromium. Then, go to Chrome
rem web store (https://chromewebstore.google.com/) as usual. If you do
rem not see the 'Add to Chrome' button in the web store (or if it's
rem disabled), you can use the context menu option instead: Right
rem click anywhere on the extension's page and select 'Add to
rem Chromium.'
:ungoogled-chromium
set app=ungoogled-chromium
set ver=132.0.6834.110-1.1
set app_ver=%app%-%ver%
set zip=%app%_%ver%_windows_x64.zip
set releases_url=https://github.com/ungoogled-software/ungoogled-chromium-windows/releases
set url=%releases_url%/download/%ver%/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\chrome.exe" "%startMenu_opt%\%app%"
exit /b

:cygwin
set dir=%opt%\cygwin
set cachedir=%software%\cygwin_cache
set url=https://www.cygwin.com/setup-x86_64.exe
set setup=cygwin_setup-x86_64.exe
call :_curl "%url%" "%setup%"
rem the_silver_searcher: for ag.
rem
rem procps: for top, ps (these only interact w/ processes of programs
rem under Cygwin, not those of native Windows programs.)
"%setup%" --no-admin ^
--local-package-dir "%cachedir%" --quiet-mode ^
--no-startmenu --delete-orphans ^
--site https://mirrors.kernel.org/sourceware/cygwin/ ^
--root "%dir%" ^
--packages openssh,bzip2,unzip,make,bc,^
wget,curl,rsync,nano,grep,nc,tmux,xclip,ncdu,the_silver_searcher,^
cygrunsrv,cron,procps

set desktoplnk=%home%\Desktop\Cygwin64 Terminal.lnk
if exist "%desktoplnk%" move /y "%desktoplnk%" "%startMenu_opt%\cygwin.lnk"

rem Although HOME dir is set correctly (by install.bat, e.g.), ssh
rem tries ~/.ssh but ~ expands to either /home/username (which would
rem require that to be a Cygwin symlink to HOME) or NOTHING (in the
rem case of ssh in a script run by cron), where ssh hits /.ssh,
rem resulting in its failure to find one's config file and keys; e.g.,
rem 'ssh -vvv' would show
rem
rem debug3: expanded UserKnownHostsFile '~/.ssh/known_hosts' -> '/.ssh/known_hosts'
rem debug3: expanded UserKnownHostsFile '~/.ssh/known_hosts2' -> '/.ssh/known_hosts2'
rem
rem https://serverfault.com/a/876936
rem https://www.gnu.org/software/bash/manual/html_node/Tilde-Expansion.html
rem https://cygwin.com/cygwin-ug-net/ntsec.html#ntsec-mapping-nsswitch
rem
rem %% is to escape %, as /%H is needed in nsswitch.conf.
echo db_home: /%%H >> "%dir%\etc\nsswitch.conf"
call :aptcyg
exit /b

:aptcyg
set file=apt-cyg
set url=https://raw.githubusercontent.com/transcode-open/apt-cyg/master/%file%
call :_curl "%url%" "%file%"
call :_ln-f "%file%" "%home%\bin\%file%"
exit /b

:msys2
rem NOT going to install msys2 (base does not come with either git or
rem ssh. Git for Windows below already comes with a big part of MinGW,
rem including, e.g., mintty (also used by Cygwin) and ssh.
rem
rem For ssh and $HOME dir, see comment on nsswitch.conf (applicable
rem both to cygwin and msys2).
rem
rem Most of Cygwin tools (and programs compiled with Cygwin's gcc,
rem e.g.) have to go through a Windows <-> POSIX translation layer
rem (cygwin1.dll). Apart from parts shared with (built on top of a
rem modified) Cygwin, most of MinGW{32,64} (and programs compiled with
rem MinGW's gcc, e.g.) go through a native Windows API.
set version=2022-05-03
set ver=20220503
set zip=msys2-base-x86_64-%ver%.tar.xz
set releases_url=https://github.com/msys2/msys2-installer/releases
set url=%releases_url%/download/%version%/%zip%
set dir=%opt%\msys2-%ver%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
7z x -y "%zip%" -so | 7z x -aoa -si -ttar -o"%dir%"
exit /b

:w64devkit
set ver=2.0.0
set exe7z=w64devkit-x64-%ver%.exe
set releases_url=https://github.com/skeeto/w64devkit/releases
set url=%releases_url%/download/v%ver%/%exe7z%
call :_curl "%url%" "%exe7z%"
"%exe7z%" -y -o"%opt%"
set dir=%opt%\w64devkit-%ver%
cd "%opt%"
ren w64devkit w64devkit-%ver%
cd "%software%"
call :_shortcut "%dir%\w64devkit.exe" "%startMenu_opt%\w64devkit"
@rem Link GNU Make under our %bin% dir (which is put on PATH) to be
@rem able to use make without having to provide a full path or having
@rem to put the whole %dir%\bin dir on PATH. (N.B. We already put
@rem Cygwin's bin dir on PATH and Cygwin also comes with GNU Make so
@rem this is for redundancy only.)
call :_ln-f "%dir%\bin\make.exe" "%bin%\make.exe"
exit /b

:emacs
rem Fix (possible) bluryness on Windows 10/11:
rem 1. Right click on Emacs shortcut Properties->Compatibility:
rem
rem   - Check "Use this setting to fix scaling problems for this program
rem     instead of the one in Settings"
rem     - for "Use the DPI that's set for my main display when" pick "I
rem     open this program"
rem
rem   - Check "Override high DPI scaling behavior".
rem     - for "Scaling performed by:" pick "Application"
rem
rem 2. Restart Emacs
set version=28.2
set ver=28
set zip=emacs-%version%.zip
set url=https://ftp.gnu.org/gnu/emacs/windows/emacs-%ver%/%zip%
set dir=%opt%\emacs-%version%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
set emacs_bat=%dir%\bin\emacs.bat
call :_emacs_bat >%emacs_bat%
call :_shortcut "%emacs_bat%" "%startMenu_opt%\emacs"
rem Spell checker
call :hunspell
exit /b

:_emacs_bat
echo.@echo off
@rem Add Cygwin's bin dir to PATH ONLY for Emacs. Otherwise,
@rem Cygwin's will be picked up from the PATH by GNU Make & del not
@rem found!
echo.set PATH=%opt%\cygwin\bin;%%PATH%%
echo.start "" "%dir%\bin\runemacs.exe"
exit /b

:hunspell
set app=hunspell
set ver=1.7.2
set zip=hunspell-v%ver%.7z
set releases_url=https://github.com/iquiw/hunspell-binary/releases
set url=%releases_url%/download/v%ver%/%zip%
set dir=%opt%\%app%-%ver%
set dicsdir=%software%\hunspell_dics
if "%setPathOnly%" == "true" goto :_hunspell_path
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
rem tar can decompress 7z, too, and has --strip-components.
tar -C "%dir%" -xkf "%zip%" --strip-components=1
set dicsurl=https://github.com/LibreOffice/dictionaries/raw/master
set lang=en
set country=US
set lang_country=%lang%_%country%
call :_mkdir "%dicsdir%"
cd /d "%dicsdir%"
call :_curl "%dicsurl%/%lang%/%lang_country%.dic" %lang_country%.dic
call :_curl "%dicsurl%/%lang%/%lang_country%.aff" %lang_country%.aff
cd /d "%software%"
:_hunspell_path
setx DICPATH "%dicsdir%"
call :_pathappend "%dir%\bin"
exit /b

:sumatrapdf
set ver=3.5.2
set zip=SumatraPDF-%ver%-64.zip
set url=https://www.sumatrapdfreader.org/dl/rel/%ver%/%zip%
set dir=%opt%\sumatrapdf-%ver%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\SumatraPDF-%ver%-64.exe" "%startMenu_opt%\sumatrapdf"
exit /b

rem Not repeatable; overwriting some files fail with permission issues.
:libreoffice
set version=24.8.3
set setup=LibreOffice_%version%_Win_x86-64.msi
set url=https://download.documentfoundation.org/libreoffice/stable/%version%/win/x86_64/%setup%
set dir=%opt%\libreoffice-%version%
call :_curl "%url%" "%setup%"
msiexec /a "%setup%" /qb TARGETDIR="%dir%"
del "%dir%"\*.msi
call :_shortcut "%dir%\program\soffice.exe" "%startMenu_opt%\libreoffice"
exit /b

:notepadpp
set version=8.4
set zip=npp.%version%.portable.x64.zip
set releases_url=https://github.com/notepad-plus-plus/notepad-plus-plus/releases
set link=%releases_url%/download/v%version%/npp.%version%.portable.x64.zip
set dir=%opt%\notepadpp-%version%
call :_curl "%link%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\notepad++.exe" "%startMenu_opt%\notepad++"
exit /b

:7zip
set ver=2301
set setup=7z%ver%.msi
set url=https://www.7-zip.org/a/%setup%
set dir=%opt%\7zip-%ver%
if "%setPathOnly%" == "true" goto :_7zip_path
call :_curl "%url%" "%setup%"
call :_mkdir "%dir%"
msiexec /a "%setup%" /qb TARGETDIR="%dir%"
rem left over in TARGETDIR after msiexec is done!
del "%dir%"\*.msi
set subdir=%dir%\Files\7-Zip
robocopy "%subdir%" "%dir%" /move /s >nul
rmdir /s /q "%dir%\Files"
:_7zip_path
call :_pathappend "%dir%"
call :_shortcut "%dir%\7zFM.exe" "%startMenu_opt%\7zip"
exit /b

rem Needs 7z.exe (7zip cmd-line interface) to be available on the
rem PATH, e.g., from appinstall.bat 7zip.
:git
set version=2.43.0
set setup=PortableGit-%version%-64-bit.7z.exe
set releases_url=https://github.com/git-for-windows/git/releases
set url=%releases_url%/download/v%version%.windows.1/%setup%
set dir=%opt%\git-%version%
if "%setPathOnly%" == "true" goto :_git_path
call :_curl "%url%" "%setup%"
call :_mkdir "%dir%"
7z x -y "%setup%" -o"%dir%"
rem From README.portable: "*NOTE*: if you decide to unpack the archive
rem using 7-Zip manually, you must run the `post-install.bat` script.
rem Git will not run correctly otherwise." However, I didn't notice
rem any issues using Git in Emacs (Windows-native build) without
rem having run post-install.bat first. And git-bash runs it upon first
rem run. So, the following should not matter, but it's here for 'good
rem measure.'
cd /d "%dir%"
git-bash.exe --no-cd --command=post-install.bat
cd /d "%software%"
call :_shortcut "%dir%\git-bash.exe" "%startMenu_opt%\git-bash"
:_git_path
rem cmd\ (and not bin\, which would pollute the PATH) is the directory
rem to add if other tools (like diff, e.g.) are otherwise present on
rem the PATH.
call :_pathappend "%dir%\cmd"
rem Link connect (for use, e.g., in ssh ProxyCommand) under %bin% dir
rem to be able to use it without having to provide a full path or
rem having to put the whole mingw64\bin dir in PATH.
call :_ln-f "%dir%\mingw64\bin\connect.exe" "%bin%\connect.exe"
exit /b

:xournal
set version=0.4.8.2016
set zip=xournal-%version%-win32.zip
set url=https://downloads.sourceforge.net/project/xournal/xournal-win32-binaries/%version%/xournal-%version%-win32.zip
call :_curl "%url%" "%zip%"
tar -C "%opt%" -xkf "%zip%"
set dir=%opt%\xournal-%version%-win32
call :_shortcut "%dir%\xournal.exe" "%startMenu_opt%\xournal"
exit /b

:xournalpp
set app=xournalpp
set ver=1.2.4
set app_ver=%app%-%ver%
rem NOT portable; seems to contains the same setup file as the
rem non-portable version. (packaging bug perhaps). Luckily, still
rem extracts with 7z; so, no GUI 'install' needed.
set zip=%app_ver%-windows-portable.zip
set releases_url=https://github.com/%app%/%app%/releases
set url=%releases_url%/download/v%ver%/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\bin\xournalpp.exe" "%startMenu_opt%\xournalpp"
exit /b

:python
set version=3.12.8
set linkversion=11.2.20241228final
set setup=Winpython64-%version%.0dot.exe
set releases_url=https://github.com/winpython/winpython/releases
set url=%releases_url%/download/%linkversion%/%setup%
set dir=%opt%\python-%version%
if "%setPathOnly%" == "true" goto :_python_path
call :_curl "%url%" "%setup%"
call :_mkdir "%dir%"
"%setup%" x -y -r -o"%dir%"
cd "%dir%\WPy*\python*"
rem Copy Python dirs + files from under python-* dir and remove the rest
rem of the stuff (Spyder, etc.) coming with Winpython.
xcopy /s /q /y * ..\..
cd ..\..

rem Workaround: rd doesn't work with wildcards! Windows: proprietary
rem and stupid, exhibit N.
for /f "delims=" %%d in ('dir /a:d /b WPy*') do (
    if exist "%%d" rd /s /q "%%d"
)

:_python_path
for %%f in (%LOCALAPPDATA%\Microsoft\WindowsApps\python.exe %LOCALAPPDATA%\Microsoft\WindowsApps\python3.exe) do (
    rem In their sheer stupidity, Microsoft puts
    rem %LOCALAPPDATA%\Microsoft\WindowsApps on user PATH; user-added
    rem dirs to PATH will, by default, go after that dir.
    rem %LOCALAPPDATA%\Microsoft\WindowsApps has several zero-byte,
    rem alias, 'Windows Apps', which only prompt the user to install
    rem the corresponding program through Microsoft's bs Store. And
    rem those alias bs 'Windows Apps' take precedence over the user's
    rem actual programs on PATH, including our Python installed here,
    rem resulting in 'Python was not found; run without arguments to
    rem install from the Microsoft Store, or disable this shortcut
    rem from Settings > Manage App Execution Aliases'.
    rem
    rem Delete the alias bs stuff if it exists. Deleting them is
    rem equivalent to turning of 'Settings/Manage App Execution
    rem Aliases' for python and python3 'App Installer'.
    if exist "%%f" del "%%f"
)

call :_pathappend "%dir%"
rem NO need to clutter the PATH by adding %dir%\Scripts to it just to
rem use pip; use 'python -m pip ...' instead of 'pip ...'.
exit /b

:pythonmodules
rem Upgrades {pip,pip3,wheel,...}.exe's and {msvc*,vc*}.dll's, which
rem are already present in %dir%\Scripts, and upgrades their .py
rem counterparts in their corresponding dirs under
rem %dir%\lib\site-packages.
python -m pip install --upgrade py pip setuptools wheel

rem For mouse automation
rem installs in pythondir\lib\site-packages
python -m pip install --upgrade pyautogui
exit /b

:miktex
set version=22.3
set setup=miktex-portable-%version%.exe
set url=https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/basic-miktex-%version%-x64.exe
set dir=%opt%\miktex-%version%
if "%setPathOnly%" == "true" goto :_miktex_path
call :_curl "%url%" "%setup%"
"%setup%" --portable="%dir%" --unattended --auto-install=yes
:_miktex_path
call :_pathappend "%dir%\texmfs\install\miktex\bin\x64"
exit /b

:mpv
set ver=20250126
set version=%ver%-git-3550ec5
set zip=mpv-x86_64-%version%.7z
set url=https://downloads.sourceforge.net/project/mpv-player-windows/64bit/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\mpv-%ver%
call :_mkdir "%dir%"
7z x -y "%zip%" -o"%dir%"
call :_shortcut "%dir%\mpv.exe" "%startMenu_opt%\mpv"
exit /b

:vlc
set version=3.0.21
set zip=vlc-%version%-win64.7z
set url=https://get.videolan.org/vlc/%version%/win64/%zip%
call :_curl "%url%" "%zip%"
7z x -y "%zip%" -o"%opt%"
set dir=%opt%\vlc-%version%
call :_shortcut "%dir%\vlc.exe" "%startMenu_opt%\vlc"
exit /b

:freetube
set app=freetube
set version=0.23.2
set zip=%app%-%version%-win-x64-portable.7z
set releases_url=https://github.com/FreeTubeApp/FreeTube/releases
set url=%releases_url%/download/v%version%-beta/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\%app%-%version%
call :_mkdir "%dir%"
7z x -y "%zip%" -o"%dir%"
call :_shortcut "%dir%\%app%.exe" "%startMenu_opt%\%app%"
exit /b

:conemu
rem drop-down, terminal emulator 'host'; can host any console
rem application developed either for WinAPI (cmd, powershell, far) or
rem Unix PTY (cygwin, msys, wsl bash).
set version=22.04.18
set ver=220418
set zip=ConEmuPack.%ver%.7z
set releases_url=https://github.com/Maximus5/ConEmu/releases
set url=%releases_url%/download/v%version%/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\conemu-%version%
call :_mkdir "%dir%"
7z x -y "%zip%" -o"%dir%"
call :_shortcut "%dir%\ConEmu.exe" "%startMenu_opt%\conemu"

rem ConEmu packages its own cygwin1.dll, which might be incompatible
rem with that of Cygwin, causing a heap corruption. Delete it!
del "%dir%/ConEmu/wsl/cygwin1.dll"
exit /b

:zeal
rem offline (GUI-based) docs
set version=0.7.2
set name=zeal-%version%-portable-windows-x64
set zip=%name%.7z
set releases_url=https://github.com/zealdocs/zeal/releases
set url=%releases_url%/download/v%version%/%zip%
call :_curl "%url%" "%zip%"
7z x -y "%zip%" -o"%opt%"
set dir=%opt%\zeal-%version%
if exist "%dir%" rmdir /s /q "%dir%"
cd "%opt%"
ren "%name%" zeal-%version%
call :_shortcut "%dir%\zeal.exe" "%startMenu_opt%\zeal"
cd /d "%software%"
call :_zeal_ini >"%dir%\zeal.ini"
exit /b

@rem Zeal shows the docsets dir with '\' dir delimiters in its GUI,
@rem but writes it to (and expects it from) its zeal.ini with '/'
@rem delimiters! So, below, instead of simply using our %software%
@rem var, we have to make it piecemeal using '/'s.
:_zeal_ini
echo.[docsets]
echo.path=%SYSTEMDRIVE%/Users/%USERNAME%/software/zeal_docsets
exit /b

:keepassxc
set app=keepassxc
set ver=2.7.9
set app_ver=%app%-%ver%
set zip=KeePassXC-%ver%-Win64.zip
set releases_url=https://github.com/keepassxreboot/keepassxc/releases
set url=%releases_url%/download/%ver%/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\KeePassXC.exe" "%startMenu_opt%\keepassxc"
exit /b

:findliterature
set url=https://gitlab.com/0mid/findliterature
git clone "%url%"
cd /d "findliterature"
set dir=%opt%\findliterature
call :_mkdir "%dir%"
for %%f in (*.sh *.config) do call :_ln-f "%%f" "%dir%\%%f"
exit /b

:youtube-dl
:yt-dlp
if "%setPathOnly%" == "true" goto :_youtube-dl_path
python -m pip install --user --upgrade yt-dlp
:_youtube-dl_path
cd "%AppData%\Python\Python*"
set appdata_pythondir=%cd%
call :_pathappend "%appdata_pythondir%\Scripts"
exit /b

:startup.bat
set startup_bat=%curdir%startup.bat
echo "%startup_bat%"
call :_shortcut "%startup_bat%" "%startMenu_opt%\startup"
exit /b

:explorerpp
set version=1.4.0-beta-2
set zip=explorerpp_x64.zip
set releases_url=https://github.com/derceg/explorerplusplus/releases
set link=%releases_url%/download/version-%version%/%zip%
set dir=%opt%\explorerpp-%version%
call :_curl "%link%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
set name=explorer++
call :_shortcut "%dir%\%name%.exe" "%startMenu_opt%\%name%"
exit /b

:jpegview
set app=jpegview
set ver=1.3.46
set zip=JPEGView64_%ver%.7z
set releases_url=https://github.com/sylikc/jpegview/releases
set link=%releases_url%/download/v%ver%/%zip%
set dir=%opt%\%app%-%ver%
call :_curl "%link%" "%zip%"
call :_mkdir "%dir%"
7z x -y "%zip%" -o"%dir%"
call :_shortcut "%dir%\JPEGView.exe" "%startMenu_opt%\%app%"
exit /b

:flameshot
set app=flameshot
set ver=12.1.0
set zip=flameshot-%ver%-win64.zip
set releases_url=https://github.com/flameshot-org/flameshot/releases
set link=%releases_url%/download/v%ver%/%zip%
call :_curl "%link%" "%zip%"
set dir=%opt%\%app%-%ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\bin\%app%.exe" "%startMenu_opt%\%app%"
set ControlPanel=HKCU\Control Panel
set key=%ControlPanel%\Keyboard
rem Disable Windows' Snipping Tool so Flameshot could take over. Make
rem sure Flameshot is closed and reopened for this change to take effect.
reg add "%key%" /f /v PrintScreenKeyForSnippingEnabled /t REG_DWORD /d 0
exit /b

@rem non-free (non-libre)
:vscode
set app=vscode
set ver=1.98.0
set zip=VSCode-win32-x64-%ver%.zip
set link=https://update.code.visualstudio.com/%ver%/win32-x64-archive/stable#/dl.7z
call :_mkdir non-libre
cd non-libre
call :_curl "%link%" "%zip%"
set dir=%opt%\non-libre\%app%-%ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\code.exe" "%startMenu_opt%\%app%"
exit /b

rem This is a liberated version of Microsoft's
rem non-libre/non-free/proprietary VS Code. N.B.: VS Code is part of
rem Microsoft's Embrace, EXTEND, and Extinguish (EEE) plan, as they
rem are releasing key 'EXTENSIONS' as proprietary software (e.g.,
rem Pylance and Cpptools for Python and C++, with hundreds of millions
rem of users).
:vscodium
set app=vscodium
set ver=1.97.2.25045
set zip=VSCodium-win32-x64-%ver%.zip
set releases_url=https://github.com/VSCodium/vscodium/releases
set link=%releases_url%/download/%ver%/%zip%
call :_curl "%link%" "%zip%"
set dir=%opt%\%app%-%ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\%app%.exe" "%startMenu_opt%\%app%"
exit /b

:r
:renv
set app=r
set ver=4.4.0
set exe=R-%ver%-win.exe
if "%setPathOnly%" == "true" goto :_r_path
set link=https://cloud.r-project.org/bin/windows/base/%exe%
call :_curl "%link%" "%exe%"
set dir=%opt%\%app%-%ver%
call :_mkdir "%dir%"
"%exe%" /silent /currentuser /dir="%dir%" /group=opt
cd "%startMenu_opt%"
ren "R %ver%.lnk" "rgui.lnk"
:_r_path
call :_pathappend "%dir%\bin"
exit /b

:rstudio
set app=rstudio
set ver=2024.04.2-764
set zip=RStudio-%ver%.zip
set link=https://download1.rstudio.org/electron/windows/%zip%
call :_curl "%link%" "%zip%"
set dir=%opt%\%app%-%ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%"
call :_shortcut "%dir%\%app%.exe" "%startMenu_opt%\%app%"
exit /b

rem Octave needs the terminal to be the 'classic cmd' (officially
rem 'Windows Console Host'); make sure that's set in 'Settings > For
rem Developers > Terminal'.
:octave
set app=octave
set ver=9.3.0
set zip=octave-%ver%-w64.7z
set url=https://ftpmirror.gnu.org/octave/windows/%zip%
call :_curl "%url%" "%zip%"
7z x -y "%zip%" -o"%opt%"
cd "%opt%"
ren "%app%-%ver%-w64" "%app%-%ver%"
set dir=%opt%/%app%-%ver%
call :_shortcut "%dir%\octave-launch.exe" "%startMenu_opt%\%app%"
exit /b

:sqlitedbrowser
:sqlitedbbrowser
set app=sqlite-dbbrowser
set ver=3.12.2
set zip=DB.Browser.for.SQLite-%ver%-win64.zip
set link=https://download.sqlitebrowser.org/%zip%
call :_curl "%link%" "%zip%"
set dir=%opt%\%app%-%ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
set exe=DB Browser for SQLite.exe
call :_shortcut "%dir%\%exe%" "%startMenu_opt%\%app%"
exit /b

:goldendict
set app=goldendict
set ver=1.5.0
set version=%ver%_.qt_5123.64bit
set app_ver=%app%-%ver%
set app_version=%app%-%version%
set zip=%app_version%.7z
set releases_url=https://github.com/goldendict/goldendict/releases
set url=%releases_url%/download/%ver%/%zip%
set exe=%app%.exe
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\%exe%" "%startMenu_opt%\%app%"
exit /b

:goldendict_dicts
set app=goldendict_dicts
set binaries_gitlab=https://gitlab.com/0mid/binaries
set binaries_branch=master
set binaries_url=%binaries_gitlab%/-/raw/%binaries_branch%
set zip=dicts_en.7z
set url=%binaries_url%/dicts/%zip%
call :_curl "%url%" "%zip%"
set dir=%software%\%app%
call :_mkdir "%dir%"
7z x -y "%zip%" -o"%dir%"
exit /b

:drmemory
set app=drmemory
set ver=2.6.20103
set app_ver=%app%-%ver%
set dir=%opt%\%app_ver%
if "%setPathOnly%" == "true" goto :_drmemory_path
set zip=DrMemory-Windows-%ver%.zip
set releases_url=https://github.com/DynamoRIO/drmemory/releases
set url=%releases_url%/download/cronbuild-%ver%/%zip%
call :_curl "%url%" "%zip%"
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
:_drmemory_path
call :_pathappend "%dir%\bin64"
exit /b

:simplewall
set app=simplewall
set ver=3.8.5
set app_ver=%app%-%ver%
set zip=%app_ver%-bin.7z
set releases_url=https://github.com/henrypp/simplewall/releases
set url=%releases_url%/download/v.%ver%/%zip%
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
set exe=%app%.exe
call :_shortcut "%dir%\64\%exe%" "%startMenu_opt%\%app%"
exit /b

:wsdk
set app=windows_sdk
set ver=10.0.26100.0
set iso=26100.1742.240904-1906.ge_release_svc_prod1_WindowsSDK.iso
set url=https://software-static.download.prss.microsoft.com/dbazure/888969d5-f34g-4e03-ac9d-1f9786c66749/%iso%
set app_ver=%app%-%ver%
set dir=%opt%\non-libre\%app_ver%
if "%setPathOnly%" == "true" goto :_wsdk_path
call :_curl "%url%" "%iso%"
call :_mkdir "%dir%"
set installers_dir=D:\Installers
start %iso% && timeout /t 3 >nul
for %%f in ("X64 Debuggers And Tools-x64_en-us.msi","Application Verifier x64 External Package (DesktopEditions)-x64_en-us.msi","Application Verifier x64 External Package (OnecoreUAP)-x64_en-us.msi") do (
msiexec /a "%installers_dir%\%%~f" /qn TARGETDIR=%dir%
if exist "%dir%\%%~f" del /f "%dir%\%%~f"
)
:_wsdk_path
call :_pathappend "%dir%\Windows Kits\10\Debuggers\x64"
exit /b

rem Windows Subsystem for GNU+Linux
rem
rem Installs in non-Admin cmd (but raises two pop-up permission
rem windows).
rem
rem Without restart after installation, trying to start Debian
rem gives the error below (so a restart is in fact necessary):
rem
rem Installing, this may take a few minutes...
rem WslRegisterDistribution failed with error: 0x80370114
rem Error: 0x80370114 The operation could not be started because a required feature is not installed.
rem
rem Installed OS goes under \\wsl$\
rem
rem After coming back from hiberation, WSL may hang.
rem No hangs have happened at least as of
rem
rem $ wsl --version
rem WSL version: 2.3.24.0
rem Kernel version: 5.15.153.1-2
rem WSLg version: 1.0.65
rem MSRDC version: 1.2.5620
rem Direct3D version: 1.611.1-81528511
rem DXCore version: 10.0.26100.1-240331-1435.ge-release
rem Windows version: 10.0.22631.4169
rem
rem If a hang happens, in an Admin cmd, kill wslservice.exe with
rem
rem taskkill /f /im wslservice.exe
:wsl
wsl --list --online
wsl --install --distribution "%ver%"
call :wslinit.sh
exit /b

:wslinit
set dir=\\wsl.localhost\%ver%\home\%USERNAME%
set dir_file=%dir%\init.sh.dos
call :_wsl_init_sh >"%dir_file%"
echo.In a WSL terminal, run
echo.tr -d '\r' ^<init.sh.dos ^>init.sh ^&^& bash init.sh
call :_debian.bat >"%bin%\debian.bat"
exit /b

rem Caveat: The line endings that cmd's echo puts in will be Windows'
rem (i.e., CRLF, '\r\n'). They need to be changed to Unix's (i.e., LF,
rem '\n'), hence the 'help' echo above.
:_wsl_init_sh
echo.cp -a /mnt/c/Users/%USERNAME%/dot* ~
echo.cd ~/dotfiles
echo../install.sh bin
echo../install.sh dotfiles
echo.for d in $(cd ~/dotfiles_*); do $d/install.sh; done
echo.cd ~/dotemacs
echo../install.sh
echo.source ~/.bashrc
echo.cd ~/dotfiles/bin
echo../appinstall.sh baseutils
echo.sudo apt install libgnutls28-dev
echo../appinstall.sh --minimal --own-libs emacs
echo.sudo apt install silversearcher-ag gdb
rem Open Source Architecture Code Analyzer (IACA replacement)
rem https://github.com/RRZE-HPC/OSACA
echo.pipx install osaca
echo../appinstall.sh ctags
echo../appinstall.sh cmake
echo.sudo apt install ffmpeg parallel
echo../appinstall.sh youtube_dl
echo../appinstall.sh likwid
echo.sudo apt install clinfo
exit /b

@rem In below, =wsl= is started via bash.exe just to pre-exec =stty=
@rem command. Otherwise, mintty's pty and WSL's pty will operate
@rem redundantly, causing problems when typing =C-O= in Emacs, etc.
@rem That is, =stty raw= is REQUIRED. Based on
@rem https://github.com/mintty/wsltty/issues/356#issuecomment-2404401487
@rem (N.B. there is no need for =--config wsltty\config= or anything
@rem else from wsltty or wslbridge2). See also VDISCARD under =man 3
@rem termios=.
:_debian.bat
echo.@echo off
echo.start "" "%opt%\cygwin\bin\mintty.exe" "%opt%\cygwin\bin\bash.exe" -c "/usr/bin/stty raw; wsl bash --login"
exit /b

:wsltty
set app=wsltty
set version=3.7.6
set app_ver=%app%-%version%
set exe=wsltty-%version%-x86_64-install-portable.exe
set releases_url=https://github.com/mintty/wsltty/releases
set link=%releases_url%/download/%version%/%exe%
set dir=%opt%\%app_ver%
call :_curl "%link%" "%exe%"
call :_mkdir "%dir%"
"%exe%" /q /c /t:"%dir%"
cd /d "%dir%"
set instdir=%dir%\portable
call :_mkdir "%instdir%"
call install.bat "%instdir%" "%instdir%" /p >nul 2>&1
cd /d "%instdir%"
set instpath=%instdir:~2%
set target=%%COMSPEC%%
set minttyargs=/C bin\mintty.exe --WSL=Debian --icon=/wsl.ico --configdir=. -~
set bridgeargs= -
set wdir=%instpath%
set icon=%instpath%\wsl.ico
set name=debian
cscript /nologo mkshortcut.vbs "/name:%name%"
copy /y "%name%.lnk" "%startMenu_opt%"

rem                          FOR TESTING ONLY
rem Manually pull in a different version of wslbridge2
rem cd /d "%software%"
rem set releases_url=urlstem=https://github.com/Biswa96/wslbridge2/releases
rem set %releases_url%/download
rem set ver=0.12
rem set zip1=wslbridge2-backend.zip
rem set zip2=wslbridge2-cygwin.zip
rem set url1=%urlstem%/v%ver%/%zip1%
rem set url2=%urlstem%/v%ver%/%zip2%
rem call :_curl "%url1%" "%zip1%"
rem call :_curl "%url2%" "%zip2%"
rem set bindir=%dir%\portable\bin
rem tar -C "%bindir%" -xkf "%zip1%"
rem tar -C "%bindir%" -xkf "%zip2%"

cd /d "%software%"
exit /b

:syncthing
set app=syncthing
set ver=1.29.2
set app_ver=%app%-%ver%
set zip=%app%-windows-amd64-v%ver%.zip
set releases_url=https://github.com/syncthing/syncthing/releases
set url=%releases_url%/download/v%ver%/%zip%
set exe=%app%.exe
call :_curl "%url%" "%zip%"
set dir=%opt%\%app_ver%
call :_mkdir "%dir%"
tar -C "%dir%" -xkf "%zip%" --strip-components=1
call :_shortcut "%dir%\%exe%" "%startMenu_opt%\%app%"
exit /b

#!/bin/bash

# Copyright (C) 2022 Omid (gitlab.com/0mid)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

source dotfiles_common.sh

mkbasedirs() {
    ${mkdir} ${localstow} ${localopt} ${localbin} \
             ${localdir}/share/applications \
             ${softsrc} ${softsrc_android} ${softsrc_binaries}

    local confdir=~/.config/appinstall
    ${mkdir} ${confdir}
    echo 'localdir='${localdir} >${confdir}/env.sh
}

prereqs_ensure() {
    cmdensure wget
    cmdensure stow
    cmdensure unzip
}

tar_deflate_opt() {
    local ext="$1"
    local taropt
    if [[ ${ext} == 'gz' ]]; then
        taropt='--gzip'
    elif [[ ${ext} == 'xz' ]]; then
        taropt='--xz'
    elif [[ ${ext} == 'bz' || ${ext} == 'bz2' ]]; then
        taropt='--bzip2'
    else
        echo 'UNKNOWN compression.'
        exit 1
    fi
    echo "${taropt}"
}

unstow() {
    local app_ver
    for app_ver in "$@"; do
        [[ -d ${app_ver} ]] && stow -D ${app_ver} || continue
    done
}

cmake_config() {
    local app_ver=$1
    cmake -DCMAKE_INSTALL_PREFIX=${localstow}/${app_ver} ../../${app_ver}
}

install_baseutils() {
    sudo apt install                                                \
         build-essential automake pkg-config                        \
         git git-svn gitk git-gui git-email                         \
         wget rsync gawk tmux openssh-server                        \
         bash-completion `# includes git completions`               \
         ncat `# for nc`                                            \
         file bc man ispell pciutils `# lspci`                      \
         zstd `# zstdcat`                                           \
         locate patchelf pmount strace                              \
         ltrace `# library call trace`                              \
         `# tools to create, check and label exFAT filesystems`     \
         `# (free software, maintained by Samsung)`                 \
         exfatprogs                                                 \
         p7zip-full                                                 \
         `# find pkgs containing a file ('apt-file find envsubst')` \
         apt-file                                                   \
         python3 python3-pip python3-venv virtualenv                \
         info zile                                                  \
         `# for https in apt source files`                          \
         apt-transport-https                                        \
         iputils-ping                                               \
         clinfo

    install_pipx
}

install_utils() {
    install_baseutils
    # Wicd must be uninstalled for the network manager to handle
    # the network and vice versa.
    sudo apt purge wicd

    sudo apt install                                               \
         network-manager network-manager-gnome                     \
         keepassxc                                                 \
         xautolock i3lock                                          \
         `# dnsutils has dig (for my lsip)`                        \
         dnsutils openvpn resolvconf live-build                    \
         bison                                                     \
         xclip inotify-tools xinput xserver-xorg-input-synaptics   \
         inkscape vlc mpv zathura pdftk                            \
         ecryptfs-utils guake                                      \
         `# isync has mbsync (one way to get mail as maildir)`     \
         isync msmtp html2text w3m                                 \
         units                                                     \
         dialog                                                    \
         cups `# printer`                                          \
         img2pdf                                                   \
         hdparm lshw                                               \
         gfortran                                                  \
         redshift-gtk                                              \
         adb                                                       \
         `# for QT_STYLE_OVERRIDE=Adwaita{,-Dark} keepassxc, e.g.` \
         adwaita-qt                                                \
         `# English phrases <=> C or C++ declarations`             \
         cdecl                                                     \
         ufw `# uncomplicated firewall`                            \
         devscripts `# dget & other deb dev/maintainer helpers`    \
         maim `# TUI screenshot (improved scrot) w/ geometry`
}

install_python_utils() {
    pip3 install --user --upgrade matplotlib
    pip3 install --user --upgrade bottleneck
}

# pipx: is the new, recommended way to install & use python 'apps' in
# isolated environments.
install_pipx() {
    local deb_ver_maj=$(cut -d. -f1 /etc/debian_version)
    # pipx is not in Debian <= 11 repos.
    if [[ ${deb_ver_maj} -le 11 ]]; then
        # pipx requires python3-venv
        sudo apt install python3-venv
        python3 -m pip install --user --upgrade pipx
    else
        sudo apt install pipx
    fi
}

install_wifi_realtek_rtl88XXau() {
    # Realtek RTL8812AU/21AU and RTL8814AU Wireless drivers
    local ver=5.6.4.2
    local branch=v${ver}

    # Remove iwlwifi (Intel proprietary WiFi firmware) if it is
    # installed.
    if [[ ${remove_conflicts} == t ]] && lsmod | grep -i iwlwifi; then
        # iwlwifi is used by iwldvm; so, remove latter first otherwise
        # error (in use)
        sudo modprobe -r iwldvm
        sudo modprobe -r iwlwifi
        sudo apt remove firmware-iwlwifi
        sudo sh -c \
             'echo blacklist iwlwifi >/etc/modprobe.d/iwlwifi.conf'
    fi

    if [[ ${from_binaries} == t ]]; then
        local binaries_gitlab="https://gitlab.com/0mid/binaries"
        local binaries_branch='master'
        local binaries_url="${binaries_gitlab}/-/raw/${binaries_branch}"

        local app_subdir="wifi/realtek/rtl88XXau/${ver}"
        local module_name='88XXau.ko'
        local url="${binaries_url}/${app_subdir}/${module_name}"

        local app_dir="${softsrc_binaries}/${app_subdir}"
        ${mkdir} -p "${app_dir}" && cd $_
        ${wget} "${url}"
        [[ $? == 0 ]] || return

        local kernel_ver="$(uname -r)"
        local module_destdir="/lib/modules/${kernel_ver}/kernel/drivers/net/wireless/"

        sudo install -p -m 644 "${app_dir}/*" "${module_destdir}" && \
            sudo /sbin/depmod -a ${kernel_ver} # Generate modules.dep
                                               # and map files
        return
    fi

    if [[ ${install_deps} == t ]]; then
        sudo apt install \
             build-essential libelf-dev dkms linux-headers-`uname -r`
    fi

    ${mkdir} ${softsrc}/firmware && cd $_
    local dname=rtl8812au
    if [[ ! -d ${dname} ]]; then
        local url="https://github.com/aircrack-ng/${dname}"
        git clone --depth=1 -b ${branch} "${url}"
    fi
    cd ${dname} && git stash && git checkout ${branch} && \
        git pull --rebase origin ${branch}

    sudo make dkms_install
    # Disable WiFi LED blinking when trasmitting data (up/download).
    sudo sh -c "echo options 88XXau rtw_led_ctrl=0 >/etc/modprobe.d/${dname}.conf"
}

install_wifi_intel_NONFREE() {
    local ver=20210315-3
    local debfile=firmware-iwlwifi_${ver}_all.deb
    local debianftp="http://ftp.us.debian.org/debian/pool"
    local url="${debianftp}/non-free/f/firmware-nonfree/${debfile}"

    ${mkdir} ${softsrc}/firmware/non-free && cd $_
    ${wget} ${url}

    sudo modprobe -r iwldvm
    sudo modprobe -r iwlwifi
    sudo dpkg -i ${debfile}
    # Disable WiFi LED blinking when transmitting data (up/download).
    sudo sh -c 'echo options iwlwifi led_mode=1 >/etc/modprobe.d/iwlwifi.conf'
    sudo modprobe iwlwifi
}

install_b43_fwcutter() {
    local ver=${1:-019-7}
    local app=b43-fwcutter
    local debianftp='http://ftp.us.debian.org/debian/pool'
    local app_ver=${app}_${ver}
    local dstdir=${localstow}/${app_ver}
    local debfile="${app}_${ver}_amd64.deb"
    local url="${debianftp}/contrib/b/${app}/${debfile}"

    cd ${softsrc}
    ${wget} ${url}

    ${mkdir} ${dstdir}
    local datafile=$(ar t ${debfile} | grep 'data.tar')
    datafile_ext=${datafile##*.} # from beg (##) del all (*) thru .
    local taropt=$(tar_deflate_opt ${datafile_ext})
    ar p ${debfile} ${datafile} | \
        tar -x ${taropt} -C ${dstdir} --strip-components=2

    cd ${localstow}
    ln -rsf ${app_ver}/bin/* ../bin
    ln -rsf ${app_ver}/share/man/man1/* ../share/man/man1
}

install_wifi_broadcom_b43_NONFREE() {
    local tarbzfile='broadcom-wl-5.100.138.tar.bz2'
    local url="http://www.lwfinger.com/b43-firmware/${tarbzfile}"
    local dir=${softsrc}/firmware/non-free
    ${mkdir} ${dir} && cd $_
    ${wget} "${url}"
    tar -xf ${tarbzfile}
    b43-fwcutter -w ${dir} broadcom-wl-5.100.138/linux/wl_apsta.o
}

install_emacs() {
    local app=emacs
    local ver=${1:-28.2}
    local app_ver=${app}-${ver}
    local app_dir=${app_ver}
    local full_dir=${localopt}/${app_ver}

    if [[ ${minimal} != t && ${install_deps} == t ]]; then
        # libjansson: JSON processing (e.g., for LSP)
        sudo apt install                                    \
             libncurses-dev libgnutls28-dev                 \
             libjansson-dev
        # libm17n (multilingualization) and libotf (OpenType
        # fonts): for font shaping, e.g., "joined characters" in
        # Persian.
        # liblcms2: little color management support
        # libgmp: GNU multi precision lib (e.g., for calc)
        sudo apt install                                    \
             libgtk-3-dev libwebkit2gtk-4.0-dev             \
             libjpeg-dev libgif-dev libtiff5-dev libxpm-dev \
             libm17n-dev libotf-dev liblcms2-dev libgmp-dev

        # libharfbuzz: font layout engine and shaping engine
        if ver_gt ${ver} 26.3; then
            sudo apt install libharfbuzz-dev
        fi
    fi

    local with_opts="--with-x=yes --with-xwidgets --with-x-toolkit=gtk3 \
--with-mailutils --with-gnutls --with-lcms2"
    # --with-modules: build Emacs with support for dynamic modules.
    # This needs a C compiler that supports '__attribute__ ((cleanup
    # (...)))', as in GCC 3.4 and later.
    with_opts+=" --with-modules"

    local with_opts_specific_to_emacs2x=""
    if ver_gt ${ver} 26.3; then
        with_opts_specific_to_emacs2x="--with-threads --with-json \
--with-harfbuzz --with-libgmp"
    fi

    # Emacs will autolaunch a D-Bus session bus, when the environment
    # variable DISPLAY is set, but no session bus is running. This
    # might be inconvenient for Emacs when running as daemon or
    # running via a remote ssh connection. In order to completely
    # prevent the use of D-Bus, configure Emacs with the options
    # below:
    local without_opts="--without-dbus --without-gconf \
--without-gsettings"
    without_opts+=" --without-sound"

    local opts="${with_opts} ${with_opts_specific_to_emacs2x} ${without_opts}"

    local conf_flags=('')
    if [[ ${minimal} == t ]]; then
        opts="--without-all --without-x --with-gnutls=ifavailable"
        app_dir+=_minimal
        full_dir+=_minimal

        # See https://www.reddit.com/r/emacs/comments/6nnd6x/comment/dkbbs6h
        # Latest version as of 2024-04-27. Tested w/ Emacs 28.2.
        local ncurses_ver=6.5
        local ncurses_dir=ncurses-${ncurses_ver}
        local tarfile=${ncurses_dir}.tar.gz
        local ldflags="-L${full_dir}/lib"
        if [[ ${own_libs} == t ]]; then
            cd ${softsrc}
            ${wget} "https://ftp.gnu.org/gnu/ncurses/${tarfile}"
            tar -xf ${tarfile}
            ${mkdir} builds/${ncurses_dir} && cd $_
            ../../${ncurses_dir}/configure \
                 --prefix=${full_dir} \
                 --with-termlib=tinfo --with-shared
            ${make} && ${make} install || exit $?
            ldflags+=" -Wl,-rpath=\\\$\$ORIGIN/../lib"
        fi
        conf_flags+=(CFLAGS="-O3 -I${full_dir}/include" LDFLAGS="${ldflags}")
    fi

    cd ${softsrc}
    ${wget} "https://mirrors.kernel.org/gnu/emacs/${app_ver}.tar.xz"
    tar -xf ${app_ver}.tar.xz
    ${mkdir} builds/${app_dir} && cd $_

    ../../${app_ver}/configure --prefix=${full_dir} ${opts} "${conf_flags[@]}"
    ${make} && ${make} install || exit $?

    cd ${localopt}
    for fname in ${app_dir}/bin/{ctags,etags}; do
        [[ -f ${fname} ]] && mv ${fname} ${fname}_emacs
    done
    ${mkdir} ../bin ../share/man/man1 ../share/applications
    ln -rsf ${app_dir}/bin/emacs* ../bin
    ln -rsf ${app_dir}/share/man/man1/* ../share/man/man1
    ln -rsf ${app_dir}/share/applications/* ../share/applications
}

install_rofi() {
    local ver=${1:-1.7.0}
    local app=rofi
    local app_ver=${app}-${ver}

    if [[ ${install_deps} == t ]]; then
        # xcb: X C Binding, xkeyboard extension
        sudo apt install bison flex \
             libpango1.0-dev libxcb-xkb-dev libxcb-util-dev \
             libxkbcommon-x11-dev libxcb-ewmh-dev libxcb-icccm4-dev \
             libxcb-xrm-dev libxcb-xinerama0-dev librsvg2-dev \
             libstartup-notification0-dev libxcb-randr0-dev \
             libxcb-cursor-dev
    fi

    cd ${softsrc}
    ${wget} "https://github.com/davatorium/rofi/releases/download/${ver}/${app_ver}.tar.xz"
    tar -xf ${app_ver}.tar.xz
    ${mkdir} builds/${app_ver} && cd $_
    ../../${app_ver}/configure --prefix=${localstow}/${app_ver} \
          --disable-check
    ${make} && ${make} install && \
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_youtube_dl() {
    pipx_install_upgrade yt-dlp
    cd ${localbin}
    ln -sf yt-dlp youtube-dl
}

install_protonvpn_cli() {
    # As protonvpn-cli needs sudo (to run openvpn), and we're
    # installing in --user env, the env has to be preserved for sudo.
    # That is, run protonvpn-cli as `sudo -E ${localdir}/bin/protonvpn',
    # via an alias perhaps.
    local app=protonvpn-cli
    local url='https://github.com/Rafficer/linux-cli-community'
    cd ${softsrc}
    if [[ ! -d ${app} ]]; then
        git clone "${url}" ${app}
        cd ${app}
    else
        cd ${app}
        git stash && git pull origin master --rebase
    fi
    pip3 install --user --upgrade .
    local protonvpn_exec=${localbin}/protonvpn
    local sudoerfile=/etc/sudoers.d/protonvpn
    [[ -f ${sudoerfile} ]] && return

    echo "Install done.

Writing ${sudoerfile} to allow running 'sudo -E protonvpn' without
having to enter ${USER}'s sudo password again."

    sudo tee ${sudoerfile} <<EOF
# User $USER is allowed in ALL terminals to run as user (root)
# with NOPASSWD the program ${protonvpn_exec}.
# SETENV tag allows $USER to disable the env_reset option (i.e.,
# to preserve his environment) via sudo -E. See man sudoers.
$USER ALL=(root) NOPASSWD:SETENV: ${protonvpn_exec}
EOF
}

install_protonvpn_gui() {
    local app=protonvpn-gui
    local ver=${1:-1.0.3-3}
    local deb="protonvpn-stable-release_${ver}_all.deb"
    local url="https://repo2.protonvpn.com/debian/dists/stable/main/binary-all/${deb}"
    cd ${softsrc}

    if [[ ${install_globally} != t ]]; then
        echo "No local installation func written for ${app} yet.
Use -g to install globally if desired."
        exit $?
    fi

    ${wget} "${url}"
    # This .deb just adds the following; it does NOT by itself install
    # protonvpn_gui:
    #
    # /etc/apt/sources.list.d/protonvpn-stable.list
    # /usr/share/keyrings/protonvpn-stable-archive-keyring.gpg
    sudo apt install ./"${deb}"
    sudo apt update && sudo apt install protonvpn
}

install_texlive_no_docs() {
    sudo apt --no-install-recommends install fonts-lato fonts-lmodern fonts-texgyre javascript-common libfile-homedir-perl libjs-jquery libptexenc1 libruby2.3 libsynctex1 libtexlua52 libtexluajit2 libyaml-tiny-perl libzzip-0-13 lmodern prosper ps2eps rake ruby ruby-did-you-mean ruby-minitest ruby-net-telnet ruby-power-assert ruby-test-unit ruby2.3 rubygems-integration t1utils tex-common tex-gyre texlive-base texlive-binaries texlive-extra-utils texlive-font-utils texlive-fonts-recommended texlive-generic-recommended texlive-latex-base texlive-latex-recommended texlive-pictures texlive-pstricks tipa zip chktex dvidvi dvipng fragmaster lacheck latexdiff latexmk purifyeps psutils dot2tex prerex ruby-tcltk texlive-latex-extra texinfo bibtex2html
}

install_zeal () {
    local ver=${1:-0.7.1}
    local app=zeal
    local app_ver=${app}-${ver}
    local app_ver_appimg=${app_ver}-x86_64.AppImage

    local baseurl="https://github.com/zealdocs/zeal/releases/download"
    local appimg="${app_ver}-x86_64.AppImage"
    local url="${baseurl}/v${ver}/${appimg}"
    cd ${softsrc}
    ${wget} "${url}"
    cd ${localopt}
    ln -f ${softsrc}/${app_ver_appimg} .
    chmod u+x,go= ${app_ver_appimg}
    ln -rsf ${app_ver_appimg} ../bin/${app}
}

install_extra_cmake_modules () {
    local ver=${1:-6.3.0}
    local app=extra-cmake-modules
    local app_ver=${app}-${ver}
    local url="https://github.com/KDE/${app}/archive/v${ver}.tar.gz"

    cd ${softsrc}
    ${wget} "${url}"
    tar -xf v${ver}.tar.gz
    ${mkdir} builds/${app_ver} && cd $_
    cmake_config ${app_ver}
    ${make} && ${make} install
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_ag() {
    local ver=${1:-2.2.0}
    local app=ag
    local app_ver=ag-${ver}
    local tarfile=${ver}.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/ggreer/the_silver_searcher/archive/${tarfile}"

    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ../${tarfile}
    ./autogen.sh && cd ..
    ${mkdir} builds/${app_ver} && cd $_
    ../../${app_ver}/configure --prefix=${localstow}/${app_ver}
    ${make} && ${make} install && \
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

# Free Software (GPLv3) as of 2020-04-15 (ver 1.2.6-1). Check current
# version for GNU/Linux at
# https://protonmail.com/download/current_version_linux.json
# and stable release notes at
# https://protonmail.com/download/bridge/stable_releases.html
install_protonmail_bridge() {
    local ver=${1:-3.5.4-1}
    local app=protonmail-bridge
    local app_ver_up=${app}_${ver}
    local app_ver=${app}-${ver}
    local deb="protonmail-bridge_${ver}_amd64.deb"
    local url="https://proton.me/download/bridge/${deb}"

    cd ${softsrc}

    ${wget} "${url}"
    if [[ ${install_globally} == t ]]; then
        sudo apt install ./${deb}
        exit $?
    fi

    ${mkdir} ${localstow}/${app_ver}
    local datafile=$(ar t ${deb} | grep 'data.tar')
    datafile_ext=${datafile##*.} # from beg (##) del all (*) thru .

    local taropt=$(tar_deflate_opt ${datafile_ext})

    ar p ${deb} ${datafile} \
        | tar -x ${taropt} -C ${localstow}/${app_ver} \
              --strip-components=2
    cd ${localstow} && unstow ${app}*
    cd ${localstow}/${app_ver}/bin
    ln -sf ../lib/protonmail/bridge/bridge-gui ${app}
    cd ${localstow} && stow ${app_ver}
}

install_mu() {
    local ver=${1:-1.10.9}      # from 1.10 stable branch
    local app=mu
    local app_ver=${app}-${ver}
    local rlsurl="https://github.com/djcb/mu/releases"

    if [[ ${install_deps} == t ]]; then
        sudo apt install libxapian-dev meson
    fi

    cd ${softsrc}
    ${wget} "${rlsurl}/download/v${ver}/${app_ver}.tar.xz"
    tar -xf ${app_ver}.tar.xz && cd ${app_ver}
    meson setup --prefix=${localstow}/${app_ver} builds/${app_ver}
    ninja -C builds/${app_ver}
    ninja -C builds/${app_ver} install
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_wire() {
    local ver=${1:-3.27.2944}
    local App=Wire
    local app=${App,,}          # convert to all lower case
    local App_ver_appimg=${App}-${ver}_x86_64.AppImage

    cd ${softsrc}
    ${wget} "https://github.com/wireapp/wire-desktop/releases/download/linux/${ver}/${App_ver_appimg}"

    cd ${localopt}
    ln -f ${softsrc}/${App_ver_appimg} .
    chmod u+x,go= ${App_ver_appimg}
    ln -rsf ${App_ver_appimg} ../bin/${app}
}

# Gives option to update from within the app. Consider removing
# version number from dir.
install_telegram() {
    local ver=${1:-4.14.13}
    local app=telegram
    local app_ver=${app}_${ver}
    local tarxzfile=tsetup.${ver}.tar.xz
    local tdesktopurl='https://github.com/telegramdesktop/tdesktop'
    local url="${tdesktopurl}/releases/download/v${ver}/${tarxzfile}"
    cd ${softsrc}
    ${wget} "${url}"
    local dstdir=${localopt}/${app_ver}
    ${mkdir} ${dstdir}
    tar -C ${dstdir} --strip-components=1 -xf ${tarxzfile}
    cd ${dstdir}
    chmod u=rwx,go= *
    ln -rfs Telegram ../../bin/telegram
}

# symlinked manually, rather than using `stow', hence installed under
# ${localopt}, rather than under ${localstow}.
install_calibre() {
    local ver=${1:-7.13.0}
    local app=calibre
    local app_ver=${app}-${ver}

    cd ${softsrc}
    ${wget} "https://download.calibre-ebook.com/${ver}/${app_ver}-x86_64.txz"
    ${mkdir} ${localopt}/${app_ver}
    tar -C ${localopt}/${app_ver} -xf ${app_ver}-x86_64.txz
    cd ${localopt}/../bin
    find ../opt/${app_ver}/ -maxdepth 1 -type f -exec ln -sf '{}' . \;
}

install_freetube() {
    local ver=${1:-0.23.2}
    local ver_full=${ver}-beta
    local App=FreeTube
    local app=${App,,}          # convert to all lower case
    local app_ver_appimg=${app}-${ver}-amd64.AppImage
    local baseurl="https://github.com/FreeTubeApp/FreeTube"
    local iconfilename=iconColor.png
    local iconurl="${baseurl}/raw/development/_icons/${iconfilename}"

    cd ${softsrc}
    ${wget} "${baseurl}/releases/download/v${ver_full}/${app_ver_appimg}"
    ${wget} "${iconurl}"
    mv -f ${iconfilename} ${app}-${iconfilename}

    cd ${localopt}
    ln -f ${softsrc}/${app_ver_appimg} .
    ln -f ${softsrc}/${app}-${iconfilename} .
    chmod u+x,go= ${app_ver_appimg}
    ln -rsf ${app_ver_appimg} ../bin/${app}

    cat > ${localdir}/share/applications/${app}.desktop <<EOF
[Desktop Entry]
Type=Application
Name=${App}
Comment=The Private YouTube Client
Exec=${localbin}/${app}
Icon=${localopt}/${app}-${iconfilename}
Terminal=false
EOF
}

install_element() {
    local app=element

    if [[ ${install_globally} != t ]]; then
        echo "No local installation func written for ${app} yet.
Use -g to install globally if desired."
        exit $?
   fi

    local gpgkeyurl="https://packages.element.io/debian/element-io-archive-keyring.gpg"
    local gpgkeyfile="/usr/share/keyrings/element-io-archive-keyring.gpg"

    sudo wget -O "${gpgkeyfile}" "${gpgkeyurl}"
    echo "deb [signed-by=${gpgkeyfile}] https://packages.element.io/debian/ default main" \
        | sudo tee /etc/apt/sources.list.d/element-io.list
    sudo apt update
    sudo apt install element-desktop
}

install_signal() {
    local app=signal-desktop

    if [[ ${install_globally} != t ]]; then
        echo "No local installation func written for ${app} yet.
Use -g to install globally if desired."
        exit $?
   fi

    local asckeyurl='https://updates.signal.org/desktop/apt/keys.asc'
    local gpgkeyfilename=signal-desktop-keyring.gpg
    local gpgkeyfile=/usr/share/keyrings/${gpgkeyfilename}
    local listfile=/etc/apt/sources.list.d/${app}.list
    local distro=xenial

    cd ${softsrc}
    wget -O- "${asckeyurl}" | gpg --dearmor > ${gpgkeyfilename}
    sudo cp -u ${gpgkeyfilename} ${gpgkeyfile}
    echo "deb [arch=amd64 signed-by=${gpgkeyfile}] https://updates.signal.org/desktop/apt ${distro} main" \
        | sudo tee "${listfile}"
    sudo apt update
    sudo apt install ${app}
}

install_nodejs() {
    local ver=${1:-12.18.3}
    local app=nodejs
    local app_ver=${app}-${ver}

    cd ${softsrc}
    ${wget} "https://nodejs.org/dist/v${ver}/node-v${ver}-linux-x64.tar.xz"

    cd ${localstow} && unstow ${app}*
    ${mkdir} ${app_ver}
    tar -C ${app_ver} --strip-components=1 \
        -xf ${softsrc}/node-v${ver}-linux-x64.tar.xz
    stow ${app_ver}
}

install_lsp_python() {
    # For use with Emacs Language Server Protocol for Python
    pipx_install_upgrade python-lsp-server
}

install_lsp_bash() {
    npm config set prefix ${localstow}/nodejs-modules
    npm install --global bash-language-server
    cd ${localstow} && stow nodejs-modules
}

install_clangllvm() {
    local ver=${1:-18.1.8}
    local app=clang-llvm
    local ubuntu_postfix=ubuntu-18.04
    local app_ver=${app}-${ver}-${ubuntu_postfix}
    local appfile=clang+llvm-${ver}-x86_64-linux-gnu-${ubuntu_postfix}.tar.xz

    if [[ ${install_deps} == t ]]; then
        if [[ ${appfile} =~ 'clang+llvm-16.0.0-x86_64-linux-gnu-ubuntu-18.04' ]]; then
            if cat /etc/*-release | grep -q 'VERSION_CODENAME=bullseye'; then
                sudo apt install libtinfo5
            fi
        fi
    fi

    cd ${softsrc}
    ${wget} "https://github.com/llvm/llvm-project/releases/download/llvmorg-${ver}/${appfile}"

    ${mkdir} ${localstow}/${app_ver}
    tar -C ${localstow}/${app_ver} --strip-components=1 -xf ${appfile}
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_julia() {
    local ver=${1:-1.10.4}
    local vermajmin=${ver%.*} # del all (*) thru . from end (%)
    local app=julia
    local app_ver=${app}-${ver}
    local apptgz=${app_ver}-linux-x86_64.tar.gz

    cd ${softsrc}
    ${wget} "https://julialang-s3.julialang.org/bin/linux/x64/${vermajmin}/${apptgz}"

    local appdir=${localstow}/${app_ver}
    ${mkdir} ${appdir}
    tar -C ${appdir} --strip-components=1 -xf ${softsrc}/${apptgz}
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_syncthing() {
    local app=syncthing

    if [[ ${install_globally} == t ]]; then
        local relkeyurl='https://syncthing.net/release-key.gpg'
        local relkey='/usr/share/keyrings/syncthing-archive-keyring.gpg'
        sudo curl -o ${relkey} ${relkeyurl}
        sudo tee /etc/apt/sources.list.d/syncthing.list <<EOF
deb [signed-by=${relkey}] https://apt.syncthing.net/ syncthing stable
EOF
        # To make sure the distribution packages (older) do not take
        # preference over those in this repository (newer), increase
        # the preference of this origin.
         sudo tee /etc/apt/preferences.d/syncthing <<EOF
Package: *
Pin: origin apt.syncthing.net
Pin-Priority: 990
EOF

        sudo apt update && sudo apt install syncthing
        exit $?
    fi

    local ver=${1:-1.27.9}
    local app_ver=${app}-${ver}
    local appfile=${app}-linux-amd64-v${ver}.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/${app}/${app}/releases/download/v${ver}/${appfile}"

    ${mkdir} ${localopt}/${app_ver} && cd $_
    tar --strip-components=1 -xf ${softsrc}/${appfile}
    ln -rsf syncthing ../../bin
}

install_pandoc() {
    local ver=${1:-3.2.1}
    local app=pandoc
    local app_ver=${app}-${ver}
    local appfile=${app_ver}-linux-amd64.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/jgm/pandoc/releases/download/${ver}/${appfile}"

    cd ${localstow} && unstow ${app}*
    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ${softsrc}/${appfile}
    cd ${localstow} && stow ${app_ver}
}

install_tmate() {
    local ver=${1:-2.4.0}
    local app=tmate
    local app_ver=${app}-${ver}
    local appfile=${app_ver}-static-linux-amd64.tar.xz

    cd ${softsrc}
    ${wget} "https://github.com/tmate-io/tmate/releases/download/${ver}/${appfile}"

    ${mkdir} ${localopt}/${app_ver} && cd $_
    tar --strip-components=1 -xf ${softsrc}/${appfile}
    ln -rsf tmate ../../bin

    cat > ~/.tmate.conf <<EOF
if-shell '[ -f ~/.tmux.conf ]' \\
  "source-file ~/.tmux.conf"
EOF
}

install_keepassxc() {
    local ver=${1:-2.6.6}
    local app=KeePassXC
    local app_ver_appimg=${app}-${ver}-x86_64.AppImage
    local urlbase="https://github.com/keepassxreboot/keepassxc"
    local app_url=${urlbase}/releases/download/${ver}/${app_ver_appimg}

    cd ${softsrc}
    ${wget} ${app_url}{.sig,}
    gpg --fetch-keys https://keepassxc.org/keepassxc_master_signing_key.asc

    gpg --verify ${app_ver_appimg}{.sig,} 2>&1 | grep 'Good signature'
    if [[ $? != 0 ]]; then
        # integrity or authenticity of ${app_ver_appimg} could not be
        # verified; exit with error.
        echo "GPG signature veriftication FAILED.
Do NOT use ${app_ver_appimg} before further investigation."
        exit $?
    fi

    cd ${localopt}
    ln -f ${softsrc}/${app_ver_appimg} .
    ln -f ${softsrc}/${app_ver_appimg} ~/.keys
    chmod u+x,go= ${app_ver_appimg}
    ln -rsf ${app_ver_appimg} ../bin/keepassxc
}

install_smtube() {
    local ver=${1:-21.7.0-1+5.1}
    local app=smtube
    local app_ver_up=${app}_${ver}
    local app_ver=${app}-${ver}
    # Debian_10 works in Debian 11 too.
    local app_url="https://download.opensuse.org/repositories/home:/smplayerdev/Debian_10/amd64/${app_ver_up}_amd64.deb"
    cd ${softsrc}
    ${wget} "${app_url}"

    ${mkdir} ${localstow}/${app_ver}
    local datafile=$(ar t ${app_ver_up}_amd64.deb | grep 'data.tar')
    datafile_ext=${datafile##*.} # from beg (##) del all (*) thru .

    local taropt=$(tar_deflate_opt ${datafile_ext})

    ar p ${app_ver_up}_amd64.deb ${datafile} \
        | tar -x ${taropt} -C ${localstow}/${app_ver} --strip-components=2
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_tor_browser() {
    # do not stop on error, which may, e.g., happen if gpg --list-key
    # or --auto-key-locate fail due to filtering.
    set +e
    local ver=${1:-14.0.3}
    local app=tor-browser
    local app_ver_up=${app}-linux-x86_64-${ver}.tar.xz
    cd ${softsrc}

    if ! gpg --list-key torbrowser@torproject.org; then
        torkeyfingerprint=EF6E286DDA85EA2A4BA7DE684E2C6E8793298290
        openpgurl="https://keys.openpgp.org/vks/v1/by-fingerprint"
        if gpg --auto-key-locate nodefault,wkd \
               --locate-keys torbrowser@torproject.org; then
            : # bash no op
        else
            curl -s "${openpgurl}/${torkeyfingerprint}" | \
                gpg --import -
        fi
    fi

    # https://www.torproject.org/getinvolved/mirrors.html.en
    # https://archive.org/details/@gettor
    # For more mirrors, send an email to gettor@torproject.org.
    for app_url in 'https://'{dist.torproject.org,{tor.eff.org,tor.ccc.de}/dist}/torbrowser/${ver}/${app_ver_up}; do
        ${wget} ${app_url}{.asc,}
        if gpg --verify ${app_ver_up}{.asc,}; then
            goodsig=t
            break
        fi
    done
    if [[ ${goodsig} != t ]]; then
        # integrity or authenticity of ${app_ver_up} could not be
        # verified; exit with error.
        echo "GPG signature veriftication FAILED.
Do NOT use ${app_ver_up} before further investigation."
        exit $?
    fi
    if [[ ${download_only} == t ]]; then exit; fi

    cd ${localopt} && ${mkdir} ${app} && cd $_
    tar --strip-components=1 -xf ${softsrc}/${app_ver_up}
    ./start-tor-browser.desktop --register-app
    cat > ${localbin}/tor.sh <<EOF
#!/bin/bash

# Call this script with
## --new-tab --allow-remote 'url0'
# as arguments to start Tor Browser and open url0 in it. Afterwards,
# call with
## --new-tab --allow-remote 'url1'
# to open url1 in a new tab of the previously opened Tor Browser.

${localopt}/tor-browser/Browser/start-tor-browser --detach "\$@"
EOF
    chmod u=rwx,go= ${localbin}/tor.sh
}

install_endlessh() {
    local ver=${1:-1.1}
    local app=endlessh
    local app_ver=${app}-${ver}

    cd ${softsrc}
    ${wget} "https://github.com/skeeto/${app}/archive/${ver}.tar.gz"
    ln -sf {${ver},${app_ver}}.tar.gz
    tar -xf ${app_ver}.tar.gz
    cd ${app_ver}
    ${make} install PREFIX=${localstow}/${app_ver}
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
    ${mkdir} ${XDG_CONFIG_HOME}/systemd/user && cd $_
    cp -a ${softsrc}/${app_ver}/util/endlessh.service .
    sed -i "s#/usr/local/bin/endlessh#${localbin}/endlessh#g" \
        endlessh.service
    systemctl --user enable endlessh
}

install_ctags() {
    local ver=${1:-6.1.0}
    local app=ctags
    local app_ver=${app}-${ver}
    local tarfile=universal-ctags-${ver}.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/universal-ctags/ctags/releases/download/v${ver}/${tarfile}"

    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ../${tarfile}
    ./autogen.sh && cd ..
    ${mkdir} builds/${app_ver} && cd $_
    ../../${app_ver}/configure --prefix=${localstow}/${app_ver}
    ${make} && ${make} install && \
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_git() {
    local ver=${1:-2.40.1}
    local app=git
    local app_ver=${app}-${ver}
    local tarfile=v${ver}.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/git/git/archive/refs/tags/${tarfile}"

    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ../${tarfile}

    # git configure does NOT support out of source builds!
    make configure
    ./configure --prefix=${localstow}/${app_ver}
    ${make} all doc && ${make} install install-doc && \
        cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

# build ear
install_bear() {
    # I was only able to build this on a BEEFY (16+ core) machine with
    # GLIBC 2.34 and GCC 11 that satisfies its 'std::filesystem'
    # dependency; GLIBC 2.28 and GCC 8.4.1 (which supposedly has full
    # C++17 support) was not enough. The build 'hangs' on GNU/Linux
    # Debian on 4-core (hyperthreaded) Tiger Lake as well as on
    # several other systems. This is badly designed or badly
    # implemented software (with giant dependencies galore, with build
    # artifacts over 2GB). I will probably not keep it.
    local ver=${1:-3.1.3}
    local app=bear
    local app_ver=${app}-${ver}
    local tarfile=${ver}.tar.gz

    cd ${softsrc}
    ${wget} "https://github.com/rizsotto/Bear/archive/refs/tags/${tarfile}"

    ${mkdir} ${app_ver}
    tar --strip-components=1 -C ${app_ver} -xf ${tarfile}

    ${mkdir} builds/${app_ver} && cd $_
    cmake -DENABLE_UNIT_TESTS=OFF -DENABLE_FUNC_TESTS=OFF \
          -DCMAKE_INSTALL_PREFIX=${localstow}/${app_ver} \
          ../../${app_ver}
    ${make} all && ${make} install && \
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_cmake() {
    local ver=${1:-3.30.0}
    local app=cmake
    local app_ver=${app}-${ver}
    local tarfile=${app}-${ver}-linux-x86_64.tar.gz
    local stowdir=${localstow}/${app_ver}

    cd ${softsrc}
    ${wget} "https://github.com/Kitware/CMake/releases/download/v${ver}/${tarfile}"

    ${mkdir} ${stowdir}
    tar --strip-components=1 -C ${stowdir} -xf ${tarfile}
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_valgrind() {
    local ver=${1:-3.17.0}
    local app=valgrind
    local app_ver=${app}-${ver}
    local appfile=${app}-${ver}.tar.bz2

    cd ${softsrc}
    ${wget} "https://sourceware.org/pub/valgrind/${appfile}"

    tar -xf ${appfile}
    ${mkdir} builds/${app_ver} && cd $_
    ../../${app_ver}/configure --prefix=${localstow}/${app_ver}
    ${make} && ${make} install && \
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_git_filter_repo() {
    # Or with pip3, as below, but that doesn't install the man pages.
    # pip3 install --user --upgrade git-filter-repo

    local ver=${1:-2.38.0}
    local app=git-filter-repo
    local app_ver=${app}-${ver}
    local appfile=${app}-${ver}.tar.xz
    local repourl="https://github.com/newren/${app}"
    local appurl=${repourl}/releases/download/v${ver}/${appfile}

    cd ${softsrc}
    ${wget} "${appurl}"

    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ${softsrc}/${appfile}
    cd ${localopt} && ${mkdir} ${app_ver} && cd $_
    ln -f ${softsrc}/${app_ver}/git-filter-repo .
    ln -f ${softsrc}/${app_ver}/Documentation/man1/git-filter-repo.1 .
    ln -rsf git-filter-repo ../../bin/
    ln -rsf git-filter-repo.1 ../../share/man/man1/
}

# python-vipaccess: Free Software replacement for Symantec's VIP
# Access client [https://github.com/dlenski/python-vipaccess].
install_python_vipaccess() {
    pipx_install_upgrade python-vipaccess
    pipx_install_upgrade qrcode[pil]
}

install_flameshot() {
    local ver=${1:-12.1.0}
    local App=Flameshot
    local app=${App,,}          # convert to all lower case
    local App_ver_appimg=${App}-${ver}.x86_64.AppImage

    cd ${softsrc}
    ${wget} "https://github.com/flameshot-org/flameshot/releases/download/v${ver}/${App_ver_appimg}"
    cd ${localopt}
    ln -f ${softsrc}/${App_ver_appimg} .
    chmod u+x,go= ${App_ver_appimg}
    ln -rsf ${App_ver_appimg} ../bin/${app}
}

install_rustdesk_server() {
    local ver=${1:-1.1.11-1}
    local app=rustdesk-server
    local app_ver=${app}-${ver}
    local zip=${app}-linux-amd64.zip
    local zip_ver=${app_ver}.zip
    local url=https://github.com/rustdesk/${app}/releases/download/${ver}/${zip}

    cd ${softsrc}
    [[ -f ${zip_ver} ]] || (${wget} "${url}" && mv ${zip} ${zip_ver})

    cd ${localstow} && unstow ${app}*
    unzip -d ${app_ver} ${softsrc}/${zip_ver} && mv ${app_ver}/{amd64,bin}
    stow ${app_ver}

    # By default, hbbs listens on 21115 (TCP) and 21116 (TCP/UDP),
    # 21118 (TCP), and hbbr listens on 21117 (TCP), 21119 (TCP). Be
    # sure to open these ports in the firewall. Please note that 21116
    # should be enabled both for TCP and UDP. 21115 is used for NAT
    # type test, 21116/UDP is used for ID registration and heartbeat
    # service, 21116/TCP is used for TCP hole punching and connection
    # service, 21117 is used for Relay services, and 21118 and 21119
    # are used to support web clients. If you do not need web client
    # (21118, 21119) support, the corresponding ports can be disabled.
}

install_rustdesk() {
    local ver=${1:-1.2.3-2}
    local App=RustDesk
    local app=${App,,}          # convert to all lower case
    local app_ver=${app}-${ver}
    local deb=${app}-${ver}.deb
    local app_ver_appimg=${app_ver}-x86_64.AppImage

    cd ${softsrc}

    if [[ ${install_globally} == t ]]; then
        ${wget} "https://github.com/rustdesk/rustdesk/releases/download/${ver}/${deb}"
        sudo apt install ./"${deb}"
        exit $?
    fi

    local baseurl=https://github.com/rustdesk/rustdesk
    local rawurl=https://raw.github.com/rustdesk/rustdesk
    local iconfilename=icon.png
    local iconurl=${rawurl}/master/res/${iconfilename}

    cd ${softsrc}
    ${wget} "${baseurl}/releases/download/${ver}/${app_ver_appimg}"
    ${wget} "${iconurl}"
    mv -f ${iconfilename} ${app}-${iconfilename}

    cd ${localopt}
    ln -f ${softsrc}/${app_ver_appimg} .
    ln -f ${softsrc}/${app}-${iconfilename} .
    chmod u+x,go= ${app_ver_appimg}
    ln -rsf ${app_ver_appimg} ../bin/${app}

    cat > ${localdir}/share/applications/${app}.desktop <<EOF
[Desktop Entry]
Type=Application
Name=${App}
Exec=${localbin}/${app}
Icon=${localopt}/${app}-${iconfilename}
Terminal=false
EOF
}

adb_check() {
    cmdensure adb

    adb devices -l
    adb shell pm list users
}

# Keep fdroid install here as stand-alone just in case all else
# (fdroidcl) fail.
install_apk_fdroid() {
    local apk='F-Droid.apk'
    local url="https://f-droid.org/${apk}"

    cd ${softsrc_android}
    ${wget} "${url}"

    adb_check || exit $?

    # No need to transfer the apk explicitly; adb does it through a
    # 'Streamed Install').
    adb install ${apk}
}

install_apk_qdict_dicts() {
    local destdir=/sdcard/QDict/dicts
    adb shell "mkdir -p ${destdir}"
    for dict in \
        "stardict-Concise_Oxford_English_Dictionary-2.4.2" \
        "stardict-Concise_Oxford_Thesaurus-2.4.2" \
        "stardict-MHM_Advance_Eng_Per"
    do
        adb push ~/software/dictionaries/"${dict}" ${destdir}
    done
}

install_apk_signal() {
    # Check https://signal.org/android/apk (or
    # https://updates.signal.org/android/latest.json).
    local ver=${1:-7.15.4}
    local app='org.thoughtcrime.securesms'
    local apk="Signal-Android-website-prod-universal-release-${ver}.apk"
    local url="https://updates.signal.org/android/${apk}"

    cd ${softsrc_android}
    ${wget} "${url}"

    # If your apksigner command is out-of-date and doesn't support the
    # latest Android APK Signature Scheme, you may see the following
    # SHA-256 fingerprint for the 1024-bit signing certificate:
    local sha256_expected='29:F3:4E:5F:27:F2:11:B4:24:BC:5B:F9:D6:71:62:C0'
    sha256_expected+=':EA:FB:A2:DA:35:AF:35:C1:64:16:FC:44:62:76:BA:26'

    sha256_got=$(keytool -printcert -jarfile "${apk}" \
                         2>/dev/null | awk '/SHA256:/ {print $2}')

    sha256_got=${sha256_got^^}  # convert to UPPERCASE

    if [[ ${sha256_got} != ${sha256_expected} ]]; then
        echo "Certificate signature veriftication FAILED.
Do NOT use ${apk} before further investigation."
        exit 1
    fi
    if [[ ${download_only} == t ]]; then exit; fi

    if ! android_appexists "${app}"; then
        adb install ${apk}
    fi
}

install_fdroidcl() {
    local ver=${1:-0.7.0}
    local app='fdroidcl'
    local appfile=${app}_v${ver}_linux_amd64
    local app_ver=${app}-${ver}
    local url="https://github.com/mvdan/fdroidcl/releases/download/v${ver}/${appfile}"

    cd ${softsrc}
    ${wget} "${url}"

    cd ${localopt}
    ln -f ${softsrc}/${appfile} ${app_ver}
    chmod u+x,go= ${app_ver}
    ln -rsf ${app_ver} ../bin/${app}
}

install_stow() {
    if [[ ${install_globally} == t ]]; then
        sudo apt install stow
        return
    fi
    local ver=${1:-2.4.1}
    local app=stow
    local appfile=${app}-${ver}.tar.gz
    local app_ver=${app}-${ver}
    local url="http://ftpmirror.gnu.org/stow/${appfile}"

    if ! cmdexists make; then
        sudo apt install make
    fi

    cd ${softsrc}
    ${wget} "${url}"
    ${mkdir} ${app_ver}
    tar -C ${app_ver} --strip-components=1 -xf ${appfile}

    ${mkdir} builds/${app_ver} && cd $_
    ../../${app_ver}/configure --prefix=${localstow}/${app_ver}
    ${make} && ${make} install
    if [[ $? != 0 ]]; then
        echo 'make install' returned with a non-zero status; NOT stowing stow.
        echo Investigate and retry.
        return
    fi
    cd ${localstow}
    d=${app_ver}/share/info/dir
    [[ -d ${d} ]] && mv ${d}{,.bak}
    ${app_ver}/bin/stow ${app_ver} ${app_ver}

}

install_apkeep() {
    local ver=${1:-0.15.0}
    local app='apkeep'
    local appfile=${app}-x86_64-unknown-linux-gnu
    local app_ver=${app}-${ver}
    local url="https://github.com/EFForg/apkeep/releases/download/${ver}/${appfile}"

    cd ${softsrc}
    ${wget} "${url}"

    cd ${localopt}
    ln -f ${softsrc}/${appfile} ${app_ver}
    chmod u+x,go= ${app_ver}
    ln -rsf ${app_ver} ../bin/${app}
}

write_fdroid_apks_csv() {
    local fdroid_csvfile=$(mktemp -q -p /tmp fdroid-apks_XXX.csv)
    cat >${fdroid_csvfile} <<EOF
org.fdroid.fdroid
com.aurora.store

eu.faircode.netguard
org.mozilla.fennec_fdroid

im.vector.app # Element (running on Matrix)
EXCLUDE cx.ring
EXCLUDE org.jitsi.meet
org.telegram.messenger

EXCLUDE io.github.muntashirakon.AppManager
EXCLUDE com.inspiredandroid.linuxcommandbibliotheca
EXCLUDE org.shadowice.flocke.andotp
org.liberty.android.freeotpplus
EXCLUDE de.nulide.findmydevice
com.kunzisoft.keepass.libre
com.termux
com.ghostsq.commander
me.zhanghai.android.files # Material Files
xyz.myachin.saveto
org.sufficientlysecure.keychain
ch.protonvpn.android
org.calyxinstitute.vpn
com.wireguard.android
info.dvkr.screenstream
com.carriez.flutter_hbb # Rustdesk
EXCLUDE com.jim.sharetocomputer
com.akansh.fileserversuit # ShareX
EXCLUDE com.caydey.ffshare # ffmpeg pipe to process multimedia
EXCLUDE com.kgurgul.cpuinfo
org.sufficientlysecure.viewer
EXCLUDE com.tachibana.downloader
EXCLUDE org.catrobat.paintroid

com.fsck.k9 # K9 Mail
EXCLUDE host.stjin.anonaddy
EXCLUDE io.simplelogin.android.fdroid

com.menny.android.anysoftkeyboard
com.anysoftkeyboard.languagepack.persian

EXCLUDE org.solovyev.android.calculator
cz.martykan.forecastie
EXCLUDE org.woheller69.gpscockpit
EXCLUDE de.kaffeemitkoffein.imagepipe
EXCLUDE com.jarsilio.android.scrambledeggsif
com.artifex.mupdf.viewer.app
com.foobnix.pro.pdf.reader
org.documentfoundation.libreoffice

net.osmand.plus # OpenStreetMaps
EXCLUDE com.mirfatif.mylocation
EXCLUDE com.kylecorry.trail_sense

EXCLUDE com.orgzly
com.simplemobiletools.notes.pro
com.simplemobiletools.calendar.pro
com.minar.birday

com.simplemobiletools.contacts.pro
com.simplemobiletools.dialer

org.petero.droidfish # Chess
com.google.android.stardroid
EXCLUDE com.nononsenseapps.feeder
EXCLUDE com.github.premnirmal.tickerwidget
EXCLUDE de.tap.easy_xkcd

com.simplemobiletools.gallery.pro
free.rm.skytube.oss
org.schabi.newpipe
is.xyz.mpv
org.videolan.vlc
com.annie.dictionary.fork

EXCLUDE de.cloneapps.crypto_prices
EXCLUDE com.tnibler.cryptocam
EXCLUDE net.sourceforge.opencamera
com.benarmstead.simplecooking
EOF
    echo ${fdroid_csvfile}
}

write_apkpure_apks_csv() {
    local apkpure_csvfile=$(mktemp -q -p /tmp apkpure-apks_XXX.csv)
    cat >${apkpure_csvfile} <<EOF
org.thoughtcrime.securesms
org.mozilla.firefox
org.torproject.torbrowser
ch.protonmail.android
EOF
    echo ${apkpure_csvfile}
}

install_apks() {
    install_apks_fdroid
    # install_apks_apkpure
}

install_apks_fdroid() {
    if ! cmdexists fdroidcl; then install_fdroidcl; fi

    local fdroid_csvfile=$(write_fdroid_apks_csv)
    trap "rm '${fdroid_csvfile}'" INT TERM EXIT

    if [[ ${exclude_some} == t ]]; then
        # Delete from # to EOL or from a beginning EXCLUDE to EOL;
        # i.e., remove comments and apps that are to be excluded from
        # install for others.
        app_list=$(sed -E 's/#.*$|^EXCLUDE.*$//g' ${fdroid_csvfile})
    else
        # Otherwise, just remove comments and the EXCLUDE word, but
        # NOT the app that follows it.
        app_list=$(sed 's/#.*$//g; s/^EXCLUDE//g' ${fdroid_csvfile})
    fi

    # do NOT exit on error (canceling the, fail safe, 'set -e' up
    # top).
    set +e

    # For 'fdroidcl download' to put the .apk's and its cache under
    # ${XDG_CACHE_HOME}/fdroidcl, instead of under ~/.cache/fdroidcl.
    export XDG_CACHE_HOME=~/software/android

    # Update the index file
    fdroidcl update
    fdroidcl devices

    if [[ ${update_only} == t ]];then
        # -u can only be used without arguments
        fdroidcl install -u
        exit $?
    fi

    if [[ ${download_only} == t ]];then
        for app in ${app_list}; do
            # With adb installed and one (and only one) device
            # connected, this downloads the apk version (and in
            # particular ABI, as given in 'adb shell getprop')
            # compatible with the connected device. Otherwise it falls
            # back to the latest version, which may turn out to be
            # incompatible with a device later connected.
            fdroidcl download ${app}
        done
        exit $?
    fi

    adb_check || exit $?

    # fdroidcl will NOT continue to the next item after encountering
    # an error with the current item; so, do one item at a item.
    for app in ${app_list}; do
        if ! android_appexists "${app}"; then
            fdroidcl install ${app}
        fi
    done
}

install_apks_apkpure() {
    if ! cmdexists apkeep; then install_apkeep; fi

    local apkpure_csvfile=$(write_apkpure_apks_csv)
    trap "rm '${apkpure_csvfile}'" INT TERM EXIT

    cd ${softsrc_android}

    if [[ ${skip_download} != t ]]; then
        apkeep -r1 -d apk-pure -c ${apkpure_csvfile} .
    fi
    if [[ ${download_only} == t ]]; then exit; fi

    adb_check || exit $?
    local tmpdir=/data/local/tmp
    adb shell "mkdir -p ${tmpdir}"

    # exclude blank lines (^$) and lines starting with # (^#)
    exclude='^$|^#'
    if [[ ${exclude_some} == t ]]; then
        exclude+=''
    fi

    app_list=$(grep -vE "${exclude}" ${apkpure_csvfile})

    # do NOT exit on error (canceling the, fail safe, 'set -e' up
    # top).
    set +e

    for app in ${app_list}; do
        local apk=${app}.apk
        adb shell "pm list packages | grep -q -w -i ${app}"
        if [[ $? != 0 ]]; then  # if ${app} is NOT already installed
            echo ${app}
            adb shell "ls ${tmpdir}/${apk}" || adb push ${apk} ${tmpdir}
            adb shell "pm install ${tmpdir}/${apk}"
        fi
    done
}

install_gnome_shell_dash_to_dock() {
    # for GNOME Shell version 43.9 coming with Debian 12, bookworm,
    # (Debian 11 version auto updated after first login to Debian 12).
    version=84
    zip=dash-to-dockmicxgx.gmail.com.v${version}.shell-extension.zip
    cd ${softsrc}
    ${wget} "https://extensions.gnome.org/extension-data/${zip}"
    extdir=${localdir}/share/gnome-shell/extensions
    extname='dash-to-dock@micxgx.gmail.com'
    ${mkdir} "${extdir}/${extname}"
    unzip -d "${extdir}/${extname}" ${zip}
    gnome-extensions enable "${extname}"
}

install_gnome_shell_settings() {
    # To get the following list, dump all modified gnome settings
    # (gsettings) keys and their values: dconf dump /

    dconf load / <<EOF
[org/gnome/desktop/calendar]
show-weekdate=false

[org/gnome/desktop/input-sources]
mru-sources=[('xkb', 'us'), ('xkb', 'ir')]
sources=[('xkb', 'us'), ('xkb', 'ir')]
xkb-options=['ctrl:menu_rctrl', 'ctrl:nocaps', 'ctrl:swap_rwin']

[org/gnome/desktop/interface]
clock-show-weekday=true
enable-animations=false
gtk-key-theme='Emacs'

[org/gnome/desktop/notifications]
show-in-lock-screen=false

[org/gnome/desktop/peripherals/touchpad]
edge-scrolling-enabled=false
natural-scroll=true
tap-to-click=true
two-finger-scrolling-enabled=true

[org/gnome/desktop/privacy]
disable-microphone=false
old-files-age=uint32 7
remember-recent-files=false
remove-old-temp-files=true
remove-old-trash-files=true
report-technical-problems=false

[org/gnome/desktop/screensaver]
lock-delay=uint32 1800
lock-enabled=true

[org/gnome/desktop/session]
idle-delay=uint32 300

[org/gnome/desktop/wm/preferences]
button-layout='appmenu:minimize,maximize,close'

[org/gnome/nautilus/preferences]
default-folder-viewer='list-view'

[org/gnome/settings-daemon/plugins/power]
power-button-action='interactive'

[org/gnome/shell]
disabled-extensions=@as []
enabled-extensions=['drive-menu@gnome-shell-extensions.gcampax.github.com', 'dash-to-dock@micxgx.gmail.com']
favorite-apps=['firefox-esr.desktop', 'start-tor-browser.desktop', 'org.gnome.Nautilus.desktop', 'goldendict.desktop', 'org.gnome.Terminal.desktop', 'element-desktop.desktop', 'rustdesk.desktop', 'org.keepassxc.KeePassXC.desktop', 'sshRvnc.desktop']

[org/gnome/shell/extensions/dash-to-dock]
animate-show-apps=false
dash-max-icon-size=42
dock-fixed=true
extend-height=true
hot-keys=false
icon-size-fixed=true
show-trash=false

[org/gnome/system/location]
enabled=true

[org/gnome/terminal/legacy/keybindings]
copy='<Alt>w'
paste='<Primary>y'

[org/gtk/settings/file-chooser]
date-format='regular'
location-mode='path-bar'
show-hidden=false
show-size-column=true
show-type-column=true
sort-column='name'
sort-directories-first=true
sort-order='ascending'
type-format='category'
EOF
}

install_debian_sources() {
    local ver=${1:-bookworm}    # Debian 12
    sudo tee /etc/apt/sources.list <<EOF
deb https://deb.debian.org/debian ${ver} main
deb-src https://deb.debian.org/debian ${ver} main

deb https://deb.debian.org/debian-security/ ${ver}-security main
deb-src https://deb.debian.org/debian-security/ ${ver}-security main

deb https://deb.debian.org/debian ${ver}-updates main
deb-src https://deb.debian.org/debian ${ver}-updates main

# See https://backports.debian.org/Instructions/
# apt -t ${ver}-backports install "package"
# deb https://deb.debian.org/debian ${ver}-backports main
EOF
}

install_grub_timeout() {
    local timeout=${1:-1}       # seconds
    sudo sed -i "/GRUB_TIMEOUT=/ s/[0-9]\+/${timeout}/" \
         /etc/default/grub && sudo update-grub
}

install_X_Wayland_override() {
    # Logout/login is needed to take effect (Alt+F2 r, which would restart
    # X and any desktop environment on top of it, does NOT work for
    # Wayland and desktop environments on top it).
    local daemon_conf=/etc/gdm3/daemon.conf
    if [[ ! -e ${daemon_conf} ]]; then
        sudo tee ${daemon_conf} <<EOF
[daemon]
WaylandEnable=false
DefaultSession=gnome-xorg.desktop
EOF
    else
        sudo sed -i.bak \
             "/\[daemon\]/ a WaylandEnable=false\nDefaultSession=gnome-xorg.desktop" \
             ${daemon_conf}
    fi
}

install_gdm3_auto_login() {
    local daemon_conf=/etc/gdm3/daemon.conf
    if [[ ! -e ${daemon_conf} ]]; then
        sudo tee ${daemon_conf} <<EOF
[daemon]
AutomaticLoginEnable=True
AutomaticLogin=$USER
EOF
    else
        sudo sed -i.bak \
             "/\[daemon\]/ a AutomaticLoginEnable=True\nAutomaticLogin=$USER" \
             ${daemon_conf}
    fi
}

install_common_desktop_env() {
    set +e
    sudo apt update
    sudo apt upgrade

    sudo apt install \
	 wget rsync git tmux zile bc info openssh-server \
	 vlc mpv keepassxc adb chromium

    sudo apt purge mlterm-common xiterm+thai
    rm -fd ~/{Templates,Public}

    cd ~
    if [[ ! -d ~/dotfiles ]]; then
        git clone 'https://gitlab.com/0mid/dotfiles.git'
    fi
    cd ~/dotfiles && git pull --rebase
    ./install.sh dotshells
    ./install.sh keyboard
    ./install.sh Xresources
    mkdir -p ~/bin
    for fname in appinstall.sh dotfiles_common.sh; do
        ln -sf ~/dotfiles/bin/${fname} ~/bin/
    done

    cd bin
    ./appinstall.sh grub_timeout

    ./appinstall.sh freetube
    ./appinstall.sh goldendict
    ./appinstall.sh firefox_addons
    ./appinstall.sh chromium_addons
    ./appinstall.sh tor_browser

    ./appinstall.sh element -g
    ./appinstall.sh protonvpn_gui -g
    ./appinstall.sh rustdesk -g

    # Decide whether or not to do the following:
    # ./appinstall.sh gdm3_auto_login
    # ./appinstall.sh X_Wayland_override

    sudo apt autoremove
    sudo apt autoclean
}

install_gnome_shell_desktop_env() {
    set +e
    cd ~/dotfiles/bin
    ./appinstall.sh gnome_shell_dash_to_dock
    ./appinstall.sh gnome_shell_settings
}

install_goldendict() {
    # This is a RECENT version of Goldendict (July 19, 2023), only
    # bundled with/based on an OLDER Qt, 4.8.7, (GCC 5.4.0 20160609,
    # 64 bit) to resolve the garbled-display issue for Right-to-Left
    # languages (e.g., Persian/Farsi). Also, main repo does NOT
    # provide an AppImage.
    local ver=${1:-661dd4d}
    local App=GoldenDict
    local app=${App,,}          # convert to all lower case
    local App_ver_appimg="${App}-${ver}-x86_64.AppImage"
    local baseurl="https://github.com/Abs62/goldendict"
    local url="${baseurl}/releases/download/continuous/${App_ver_appimg}"
    local iconfilename=programicon.png
    local iconurl="${baseurl}/raw/master/icons/${iconfilename}"

    cd ${softsrc}
    ${wget} "${url}"
    ${wget} "${iconurl}"
    mv -f ${iconfilename} ${app}-${iconfilename}

    cd ${localopt}
    ln -f ${softsrc}/${App_ver_appimg} .
    ln -f ${softsrc}/${app}-${iconfilename} .
    chmod u+x,go= ${App_ver_appimg}
    ln -rsf ${App_ver_appimg} ../bin/${app}

    cat > ${localdir}/share/applications/${app}.desktop <<EOF
[Desktop Entry]
Type=Application
Name=${App}
Exec=${localbin}/${app}
Icon=${localopt}/${app}-${iconfilename}
Terminal=false
EOF
}

install_goldendict_dicts() {
    local app=goldendict_dicts
    local binaries_gitlab='https://gitlab.com/0mid/binaries'
    local binaries_branch='master'
    local binaries_url=${binaries_gitlab}/-/raw/${binaries_branch}
    local dir=${app}
    cd ${softsrc}
    ${mkdir} "${dir}"
    local zip url
    for zip in dicts_en.7z dicts_fa.7z dicts_sp.7z; do
        url="${binaries_url}/dicts/${zip}"
        ${wget} "${url}" && 7z x -y "${zip}" -o"${dir}"
    done
}

# To disable "Sign in with Google" popups on all domains, add the
# following filter (see https://superuser.com/a/1773215) in uBlock
# Origin, Dashboard, My filters:
#
# accounts.google.com/gsi/*
install_firefox_addons() {
    local firefoxnewtab="firefox --new-tab --allow-remote"
    local baseurl="https://addons.mozilla.org/en-US/firefox/addon"
    local addonlist=("ublock-origin" "privacy-badger17" "decentraleyes")
    addonlist+=("tridactyl-vim" "darkreader" "multi-account-containers")
    addonlist+=("cookie-autodelete" "downthemall")
    addonlist+=("istilldontcareaboutcookies" "noscript")
    addonlist+=("privacy-redirect" "old-reddit-redirect")
    addonlist+=("torproject-snowflake" "clearurls" "single-file")
    addonlist+=("popupoff")

    ${firefoxnewtab} "about:debugging#/runtime/this-firefox" &
    for addon in "${addonlist[@]}"; do
        local addonurl="${baseurl}/${addon}"
        ${firefoxnewtab} "${addonurl}" &
    done
}

install_chromium_addons() {
    local chromiumnewtab="chromium"
    local baseurl="https://chrome.google.com/webstore/detail"
    local addonlist=("ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm")
    addonlist+=("privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp")
    addonlist+=("decentraleyes/ldpochfccmkkmhdbclfhpagapcfdljkj")
    addonlist+=("dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh")
    addonlist+=("cookie-autodelete/fhcgjolkccmbidfldomjliifgaodjagh")
    addonlist+=("downthemall/nljkibfhlpcnanjgbnlnbjecgicbjkge")
    addonlist+=("i-still-dont-care-about-c/edibdbjcniadpccecjdfdjjppcpchdlm")
    addonlist+=("snowflake/mafpmfcccpbjnhfhjnllmmalhifmlcie")
    addonlist+=("privacy-redirect/pmcmeagblkinmogikoikkdjiligflglb")
    addonlist+=("noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm")
    addonlist+=("google-input-tools/mclkkofklkfljcocdinagocijmpgbhab")
    addonlist+=("clearurls/lckanjgmijmafbedllaakclkaicjfmnk")
    addonlist+=("singlefile/mpiodijhokgodhhofbcjdecpffjipkle")
    addonlist+=("popupoff-popup-and-overla/ifnkdbpmgkdbfklnbfidaackdenlmhgh")

    for addon in "${addonlist[@]}"; do
        local addonurl="${baseurl}/${addon}"
        ${chromiumnewtab} "${addonurl}" &
    done
}

install_sshRvnc_helpee() {
    local helpeeIdx=$1
    if [[ -z ${helpeeIdx} ]]; then
        echo "error: helpee index missing; run as
${prog} sshRvnc_helpee=N
where N is 0, 1, ..., a unique index identifying this helpee.
This index MUST BE MATCHED on the HELPER side to connect to
this helpee."
        exit 1
    fi
    local app=sshRvnc

    # For the sake of others who may not wish to do a
    # './install.sh bin' to get all my dotfiles/bin/*.
    chmod u+x ~/dotfiles/bin/sshRvnc.sh
    for fname in sshRvnc.sh dotfiles_common.sh; do
        ln -sf ~/dotfiles/bin/${fname} ~/bin/
    done

    # In the 'Exec' key of a .desktop, the reserved characters are
    # space (" "), tab, newline, double quote, single quote ("'"),
    # backslash character ("\"), greater-than sign (">"), less-than
    # sign ("<"), tilde ("~"), vertical bar ("|"), ampersand ("&"),
    # semicolon (";"), dollar sign ("$"), asterisk ("*"), question
    # mark ("?"), hash mark ("#"), parenthesis ("(") and (")") and
    # backtick character ("`"). That is, they may NOT appear in Exec.
    # Below, $HOME is expanded by the shell (bash) running this script
    # (appinstall.sh) first and the result will be written to the
    # .desktop file; hence, there will NOT be a "$" in the .desktop
    # file.
    #
    #  In 'Icon' key, if the name is an absolute path, the given file
    #  will be used. If the name is not an absolute path, the
    #  algorithm described in the Icon Theme Specification will be
    #  used to locate the icon.
    # https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html

    cat > ${localdir}/share/applications/${app}.desktop <<EOF
[Desktop Entry]
Type=Application
Name=${app}
Comment=Remote support via SSH and VNC
# We are explicitly invoking xterm (to be able to set its font
# size, -fs); so, no implicit terminal is needed, which we denote
# with Terminal=false. Otherwise (i.e., with Terminal=true), we'll
# have an extra empty xterm opened, which will invoke our Exec
# command line, itself explicitly invoking another xterm.)
Exec=xterm -fs 14 -e "HELPEEIDX=${helpeeIdx} $HOME/bin/sshRvnc.sh"
Icon=gnome-remote-desktop
Terminal=false
EOF
}

install_sshRvnc_helper_with_jumphost() {
    local IPandUsers=$1
    if [[ -z ${IPandUsers} ]]; then
        echo "error: jump host IP and helpee usernames missing; run as
${prog} sshRvnc_helper_with_jumphost=IP,username0,username1,...
where the order of helpee usernames determines their index N
(username0 is indexed 0, username1 is indexed 1, etc.),
which MUST MATCH the index N set during
'appinstall.sh sshRvnc_helpee=N' of the corresponding helpee."
        exit 1
    fi
    IFS=',' read -ra IPandUsersArr <<< "${IPandUsers}"
    local jumphostIP=${IPandUsersArr[0]}
    local sshConfigFile=~/.ssh/config
    local sshRvncConfigDir=~/.ssh
    ${mkdir} ${sshRvncConfigDir}
    local sshRvncConfigFilename=sshRvnc.conf
    local sshRvncConfigFile=${sshRvncConfigDir}/${sshRvncConfigFilename}

    echo "
Host *
# Include MUST be at the top, or inside a Match or Host block.
# Include requires ssh > 7.3p1; see man 5 ssh_config for details.
Include ${sshRvncConfigFilename}" >>${sshConfigFile}


    cat >${sshRvncConfigFile} <<EOF
Host	       tunneller_jumphost
HostName       ${jumphostIP}
IdentityFile   ~/.ssh/id_rsa
User           tunneller

EOF

for (( idx=1; idx < ${#IPandUsersArr[*]}; idx++ )) ; do
    local username=${IPandUsersArr[idx]}
    # Identify (i.e., index) each helpee with a unique port on remote,
    # so that a helper could attach to multiple helpees at the same
    # time. (index helpees from 0, because that's how we roll!)
    helpeeIdx=$((${idx} - 1))
    local sshport=$((2200 + ${helpeeIdx}))
        cat >>${sshRvncConfigFile} <<EOF
# equiv. one-liner (standalone, i.e., w/o this ~/.ssh/config entry):
#
# ssh -C -J tunneller@${jumphostIP} ${username}@localhost -p ${sshport}
#
# equiv. two-step cmd;
# 1: fwd connections to Local port ${sshport} (-L${sshport}:)
# to ${jumphostIP}'s port ${sshport} (localhost:${sshport}).
# 2: ssh to localhost on port ${sshport} (localhost -p${sshport})
# as the user ${username}.
#
# ssh -vvv -N -L${sshport}:localhost:${sshport} tunneller@${jumphostIP}
# ssh ${username}@localhost -p${sshport}
Host	       ${username}_with_jumphost
ProxyJump      tunneller_jumphost
HostName       localhost
Port           ${sshport}
User           ${username}
IdentityFile   ~/.ssh/id_rsa
Compression    yes
EOF
    ln -sf ~/dotfiles/bin/vncviewerL.sh ~/bin/
    done
}

install_sshRvnc_helper_direct() {
    ln -sf ~/dotfiles/bin/sshRvnc-init.sh ~/bin/
    bash ~/bin/sshRvnc-init.sh
}

install_signalbackup_tools() {
    local app=signalbackup-tools
    local url="https://github.com/bepaald/${app}"
    cd ${softsrc}
    if [[ ! -d ${app} ]]; then
        git clone "${url}"
        cd ${app}
    else
        cd ${app}
        git pull
    fi
    if [[ ${install_deps} == t ]]; then
        sudo apt install libssl-dev
    fi
    bash BUILDSCRIPT_MULTIPROC.bash44
    ln -sf ${softsrc}/${app}/signalbackup-tools ${localbin}
}

# See .bashrc for usage.
install_bash_preexec() {
    local ver=${1:-0.5.0}
    local app=bash-preexec.sh
    local app_ver=${app}-${ver}
    local tarfile=${ver}.tar.gz
    cd ${softsrc}
    ${wget} "https://github.com/rcaloras/bash-preexec/archive/refs/tags/${tarfile}"

    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ../${tarfile}

    cp -af ${app} ~/.${app}
}

install_overdrive_sh() {
    local ver=${1:-2.4.0}
    local app=overdrive.sh
    local app_ver=${app}-${ver}
    local tarfile=${ver}.tar.gz
    cd ${softsrc}
    ${wget} "https://github.com/chbrown/overdrive/archive/refs/tags/${tarfile}"
    ln -sf ${tarfile} ${app_ver}.tar.gz
    ${mkdir} ${app_ver} && cd $_
    tar --strip-components=1 -xf ../${tarfile}
    ln -f ${app} ${localbin}
}

install_cryptocam_companion_cli() {
    local ver=${1:-a825cc79}    # Apr 14, 2023
    local app=cryptocam-companion-cli
    local app_ver=${app}-${ver}
    local url="https://gitlab.com/cryptocam/${app}"

    cd ${softsrc}
    if [[ ! -d ${app} ]]; then
        git clone "${url}"
        cd ${app}
    else
        cd ${app}
        git stash && git checkout master && git pull
    fi
    git checkout ${ver}

    if [[ ${install_deps} == t ]]; then
        sudo apt install \
             cargo libavcodec-dev libavformat-dev \
             libswscale-dev pinentry-tty
    fi
    cargo build --release # downloads everything and the kitchen sink 1st

    ln -f target/release/cryptocam ${localopt}
    cd ${localbin}
    ln -sf ../opt/cryptocam .
    local keyringdir= ~/.keys/cryptocam
    local device=oksmphone
    cryptocam --keyring ${keyringdir} key-gen cryptocam_${device}
}

install_likwid() {
    local ver=${1:-5.3.0}
    local app=likwid
    local app_ver=${app}-${ver}
    local blddir=builds/${app_ver}
    local tarfile=${app}-${ver}.tar.gz
    local url="https://ftp.fau.de/pub/${app}/${tarfile}"

    cd ${softsrc}
    ${wget} "${url}"

    ${mkdir} ${blddir}
    tar --strip-components=1 -C ${blddir} -xf ${tarfile}
    make_common_args=("PREFIX=${localstow}/${app_ver}" 'ACCESSMODE=perf_event')
    cd ${blddir}
    ${make} "${make_common_args[@]}"
    ${make} "${make_common_args[@]}" install
    cd ${localstow} && unstow ${app}* && stow ${app_ver}

    # Use appinstall.sh -e likwid to skip
    # ACCESSMODE=accessdaemon build, which requires 'root'.
    [[ ${exclude_some} == t ]] && return

    cd ${blddir}
    ${make} "${make_common_args[@]}" distclean
    make_common_args=("PREFIX=${localstow}/${app_ver}" 'ACCESSMODE=accessdaemon')
    ${make} "${make_common_args[@]}"
    sudo ${make} "${make_common_args[@]}" install
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_debian() {
    local ver=${1:-12.5.0}
    local app=debian-live
    local app_ver=${app}-${ver}
    local urlbase='https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid'

    [[ ${download_only} == t ]] && return
    mkdir -p ${softsrc}/debian && cd $_
    ${wget} "${urlbase}/${app_ver}-amd64-lxde.iso"
    ${wget} "${urlbase}/SHA256SUMS"
    local doctxt=install.en.txt
    ${wget} "https://www.debian.org/releases/stable/amd64/${doctxt}"
    mv -f "${doctxt}" "${app_ver}_${doctxt}"

    sha256sum -c --ignore-missing SHA256SUMS || return
}

install_virtualbox() {
    local ver=${1:-7.0.18}
    local version=${ver}-162988
    local vermajmin=${ver%.*} # del all (*) thru . from end (%)
    local app=virtualbox
    local app_ver=${app}-${ver}
    local deb="${app}-${vermajmin}_${version}~Debian~bullseye_amd64.deb"
    local url="https://download.virtualbox.org/virtualbox/${ver}/${deb}"

    cd ${softsrc}

    ${wget} "${url}"
    if [[ ${install_globally} == t ]]; then
        sudo apt install ./${deb}
        exit $?
    fi

    ${mkdir} ${localstow}/${app_ver}
    local datafile=$(ar t ${deb} | grep 'data.tar')
    datafile_ext=${datafile##*.} # from beg (##) del all (*) thru .

    local taropt=$(tar_deflate_opt ${datafile_ext})

    ar p ${deb} ${datafile} \
        | tar -x ${taropt} -C ${localstow}/${app_ver} \
              --strip-components=2
    cd ${localstow} && unstow ${app}* && stow ${app_ver}
}

install_intel_gpu_drivers_NONFREE() {
    local pubkey=intel-graphics.key
    local pubkeyurl=https://repositories.intel.com/gpu/${pubkey}
    local gpgkeypath=/usr/share/keyrings/intel-graphics.gpg

    cd ${softsrc}
    wget -qO - "${pubkeyurl}" | \
        sudo gpg --yes --dearmor --output "${gpgkeypath}"

    echo "deb [arch=amd64,i386 signed-by=${gpgkeypath}] https://repositories.intel.com/gpu/ubuntu jammy client" | \
    sudo tee /etc/apt/sources.list.d/intel-gpu-jammy.list

    sudo apt update
    # intel-media-va-driver-non-free: VAAPI driver for the Intel GEN8+
    # Graphics family. The VA-API (Video Acceleration API) enables
    # hardware accelerated video decode/encode at various entry-points
    # (VLD, IDCT, Motion Compensation etc.) for the prevailing coding
    # standards today (MPEG-2, MPEG-4 ASP/H.263, MPEG-4 AVC/H.264, and
    # VC-1/WMV3). It provides an interface to fully expose the video
    # decode capabilities in today's GPUs.
    sudo apt install \
         intel-opencl-icd intel-level-zero-gpu level-zero \
         intel-level-zero-gpu-raytracing \
         intel-media-va-driver-non-free libmfx1 libmfxgen1 libvpl2 \
         libegl-mesa0 libegl1-mesa libegl1-mesa-dev libgbm1 \
         libgl1-mesa-dev libgl1-mesa-dri \
         libglapi-mesa libgles2-mesa-dev libglx-mesa0 \
         libigdgmm12 libxatracker2 mesa-va-drivers \
         mesa-vdpau-drivers mesa-vulkan-drivers va-driver-all \
         vainfo hwinfo
}

pipx_install_upgrade() {
    pipx install "$@"
    pipx upgrade "$@"
}

software_list=(utils emacs rofi mps_youtube youtube_dl protonvpn_cli)
software_list+=(protonvpn_gui texlive_no_docs extra_cmake_modules smtube)
software_list+=(zeal ag protonmail_bridge mu wire calibre freetube nodejs)
software_list+=(lsp_python lsp_bash clangllvm julia syncthing pandoc)
software_list+=(keepassxc tor_browser prereqs tmate endlessh ctags)
software_list+=(valgrind python_vipaccess git_filter_repo flameshot)
software_list+=(element fdroidcl apkeep apk_fdroid apk_signal signal)
software_list+=(apk_qdict_dicts apks rustdesk rustdesk_server)
software_list+=(gnome_shell_dash_to_dock gnome_shell_settings)
software_list+=(debian_sources grub_timeout X_Wayland_override)
software_list+=(gdm3_auto_login goldendict)
software_list+=(gnome_shell_desktop_env common_desktop_env)
software_list+=(sshRvnc_helpee sshRvnc_helper_with_jumphost)
software_list+=(firefox_addons chromium_addons signalbackup_tools)
software_list+=(python_utils stow cmake bear git bash_preexec)
software_list+=(overdrive_sh wifi_realtek_rtl88XXau wifi_intel_NONFREE)
software_list+=(b43_fwcutter wifi_broadcom_b43_NONFREE)
software_list+=(baseutils telegram)
software_list+=(cryptocam_companion_cli likwid debian virtualbox)
software_list+=(intel_gpu_drivers goldendict_dicts pipx)

usage() {
    cat <<EOF
Usage: ${prog} SOFTWARE[=VERSION] [OPTIONS]
where SOFTWARE is one of "${software_list[@]} ALL".
In particular, ALL means install all of the software in the list.

If no VERSION is provided, the default in install_SOFTWARE() function
is used.

Unless otherwise noted, for SOFTWARE installed with pip or npm the
latest version of SOFTWARE and its dependencies are installed. Note:
python-language-server reverts to an older version of jedi due to a
problem in the latest version.

OPTIONS, which may come before or after SOFTWARE,

-h, --help
    show this help

--install-deps
    install dependecies if SOFTWARE is being installed from source and
    it has dependencies to be installed (with sudo apt as of now).

EOF
    exit $1
}

main() {
    set -e
    set -o pipefail

    prog="$(basename ${BASH_SOURCE[0]})"

    wget="wget --no-clobber --"
    make="make -j${nproc}"
    mkdir="mkdir -p --"

    shortopts=h,d,u,s,e,g
    longopts=help,install-deps,dry-run,download-only,update-only
    longopts+=,skip-download,exclude-some,install-globally
    longopts+=,minimal,base-dir:,remove-conflicts,from-binaries
    longopts+=,own-libs
    args="$(getopt --name ${prog} -o ${shortopts} --long ${longopts} -- "$@")"
    [[ $? -ne 0 ]] && usage 1
    eval set -- "${args}"

    install_software='install_${software}'
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -h | --help)
                usage 0
                ;;
            --install-deps)
                install_deps=t
                ;;
            --dry-run)
                install_software="echo ${install_software}"
                # This will not be used when dryrun is t, but we set
                # it to /tmp to be safe, in case of bugs in this
                # script.
                basedir=/tmp
                ;;
            -d | --download-only)
                download_only=t
                ;;
            -u | --update-only)
                update_only=t
                ;;
            -s | --skip-download)
                skip_download=t
                ;;
            -e | --exclude-some)
                exclude_some=t
                ;;
            -g | --install-globally)
                install_globally=t
                ;;
            --remove-conflicts)
                remove_conflicts=t
                ;;
            --from-binaries)
                from_binaries=t
                ;;
            --minimal)
                minimal=t
                ;;
            --own-libs)
                own_libs=t
                ;;
            --base-dir)
                shift
                basedir=$1
                ;;
            --)
                # end of options
                shift
                break
                ;;
        esac
        shift
    done

    : ${basedir:=$HOME}
    localdir=${basedir}/.local
    localstow=${localdir}/stow
    localopt=${localdir}/opt
    localbin=${localdir}/bin
    softsrc=${basedir}/software
    softsrc_android=${softsrc}/android
    softsrc_binaries=${softsrc}/binaries

    export PIPX_HOME=${localdir}/pipx
    export PIPX_BIN_DIR=${localbin}

    if [[ $# -gt 0 ]]; then
        # process remaining args (i.e., non '-' or '--')
        for arg in "$@"; do
            if [[ ${arg} == ALL ]]; then
                mkbasedirs
                prereqs_ensure
                for software in ${software_list[@]}; do
                    eval ${install_software}
                done
                exit $?
            fi
            # if ${arg} contains = (e.g., emacs=26.3)
            if [[ ${arg} =~ '=' ]]; then
                software=${arg%=*}  # from end (%) del all (*) thru =
                version=${arg#*=}   # from beg (#) del all (*) thru =
            else
                software=${arg}
            fi
            # if ${software_list} contains ${software}
            if [[ " ${software_list[@]} " =~ " ${software} " ]]; then
                mkbasedirs
                prereqs_ensure
                eval ${install_software} ${version}
            else
                usage 1
            fi
        done
    else
        usage 1
    fi
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi

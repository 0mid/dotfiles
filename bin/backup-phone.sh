#!/bin/bash

# 1. Install termux from FDroid.
# 2. In termux install openssh (for sshd) and rsync:
#    termux: > pkg upgrade && pkg install openssh rsync
# 3. Make sure sshd is run in termux; do a one-time
#    termux: > echo sshd >>~/.profile
#    termux: > exit
#    then, this script starts termux automatically, which in turn
#    starts sshd automatically (thanks to the .profile written above).
# 4. Add a public key of this computer (e.g., ~/.ssh/id_rsa.pub)
#    to ~/.ssh/authorized_keys in termux:
#    laptop: > adb push ~/.ssh/id_rsa.pub /sdcard
#    termux: > cat /sdcard/id_rsa.pub >>~/.ssh/authorized_keys

source dotfiles_common.sh

cmdensure adb

# If termux is already running, the following gives a warning
# indicating so; this is harmless: 'Warning: Activity not started,
# intent has been delivered to currently running top-most instance.'
adb shell am start -n com.termux/.HomeActivity
trap "adb shell am force-stop com.termux" INT TERM EXIT

# wait for termux to have opened and started sshd before proceeding
sleep 2s

# adb forward local remote
adb forward tcp:8022 tcp:8022

mkdir -p ~/backups/phone/{int,ext}

# phone Host is defined in ~/.ssh/config

# rsync to/from both work out of the box for both Internal storage and
# External storage below. Here only 'from' (read, backup) is used.

# Internal storage
# /sdcard -> /storage/self/primary
# /storage/self/primary -> /storage/emulated/0
rsync -trP phone:/sdcard/{DCIM,Pictures} ~/pictures/phone/
rsync -trP\
      --exclude 'DCIM/' \
      --exclude 'Pictures/' \
      phone:/sdcard/ ~/backups/phone/int/

# External storage
extsdcard=47BB-73FB
rsync -trP phone:/storage/${extsdcard}/{DCIM,Pictures} ~/pictures/phone/
rsync -trP\
      --exclude 'DCIM/' \
      --exclude 'Pictures/' \
      phone:/storage/${extsdcard}/ ~/backups/phone/ext/

#!/bin/bash

ncpu_cores=$(lscpu | \
    grep 'CPU(s):' | head -n 1 | awk -F' ' '{ print $2 }')

echo "Set the scaling governor for all CPU cores."
read -p "Enter o for Ondemand | s for powerSave | p for Performance: " sel
case ${sel} in
    o)
	cpugov=ondemand
	;;
    s)
	cpugov=powersave
	;;
    p)
	cpugov=performance
	;;
esac
	
for cpun in $(seq 0 $((${ncpu_cores}-1))); \
do sudo sh -c \
"echo ${cpugov} > /sys/devices/system/cpu/cpu${cpun}/cpufreq/scaling_governor"; \
done

cat /proc/cpuinfo | grep -i "CPU MHz"

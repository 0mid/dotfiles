#!/bin/bash

# To get the trackpoint device name to set-prop on, do
#
# xinput --list | grep -i trackpoint
# xinput list-props "TPPS/2 IBM TrackPoint"
xinput set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" -0.40

#!/bin/bash

# ver_gt $v2 $v1 returns true if $v2 > $v1. See
# https://stackoverflow.com/a/24067243.
ver_gt() {
    test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1";
}

cmdexists() {
    command -v $1 &>/dev/null
}

cmdensure() {
    local cmd=$1
    local pkg=${2:-${cmd}}
    if ! cmdexists ${cmd}; then
        sudo apt update
        sudo apt install ${pkg}
    fi
}

android_appexists() {
    local app="$1"
    # ending $ to make sure we don't pick a.b.c when grep'ing for a.b
    adb shell pm list packages | grep -q -w -i -E "${app}"'$'
}

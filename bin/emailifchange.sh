#!/bin/bash

# To run as a persistent background process use
#
# nohup emailifchange.sh > /dev/null 2>&1 &
#
# (drop the `> /dev/null' redirection to debug)

colsep='@##@'			# choose sth weird not in your filenames

watchdir="$HOME/temp/watchdir"
toaddress='name@domain.com'
ccaddress=''
subject='Change in watchdir detected'

emailfilecontents(){
    mutt \
	-s "${subject}" \
	-c "${ccaddress}" \
	"${toaddress}" \
	< "${filename}"
}

setupwatches(){
    inotifywait --recursive --monitor --event modify \
	--format "%e${colsep}%w%f" "${watchdir}" |
    while read line; do
	event=$(echo "${line}" | awk -F"${colsep}" ' {print $1} ')
	filename=$(echo "${line}" | awk -F"${colsep}" ' {print $2} ')
	case ${event^^} in		# ${event^^} is a bashism for UPPERCASEing
	    MODIFY)
		emailfilecontents
	esac
    done
}

setupwatches
exit 0

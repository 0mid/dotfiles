#!/bin/bash

# This script can only handle at most 1 arg (which may be a file or a
# dir). Invoke feh itself for any more general need (e.g., with a mix
# of multiple files and dirs).

# If no arg is given, just start feh.
if [ -z "$1" ]; then
    feh
    exit
fi

fname="$1"

# If a dir arg is given, call feh on it directly.
if [ -d "$1" ]; then
    feh "$1"
    exit
fi

case "$fname" in
  /*) fname="$fname";;
  *) fname="$PWD/$fname";;
esac

fpath="$(dirname "$fname")"

feh --action1 \;'echo -n %F | xclip -selection clipboard' \
    --start-at "$fname" "$fpath"

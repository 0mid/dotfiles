#!/bin/bash

# Copyright 2014 Omid
#
# ffaudio.sh is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

usage="Usage: $(basename "$0") SOURCE
Extract audio stream of SOURCE using ffmpeg.
Write result to SOURCE.ext, where ext is determined based on audio stream.

EXAMPLES
========
Extract audio from all .webm files in current directory:

  for fname in *.webm; do ffaudio.sh \"\$fname\"; done

If GNU parallel is installed, the following may be faster:

  ls -1 *.webm | parallel ffaudio.sh {}"

[[ $# == 0 ]] && { echo "$usage"; exit 0; }

fname=$1
audioext=$(ffprobe "${fname}" 2>&1 | egrep 'Audio:' | awk '{ print $4 }' | sed 's/,//')

case ${audioext} in
    vorbis)
	audioext=ogg
	;;
esac

ffmpeg -i "${fname}" -vn -acodec copy "${fname}.${audioext}"

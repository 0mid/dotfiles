#!/bin/bash

# Copyright 2014 Omid
#
# ffcut.sh is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

usage="Usage: $(basename "$0") [OPTION] SOURCE [TARGET]
Cut SOURCE audio/video file into a shorter clip TARGET.

   -s,--start[=hh:mm:ss] Time to start clip (e.g., 01:08:03 for 1h 8min 3s)
			 Optional. If not provided, or '0', start clip at beginning of SOURCE.
   -e,--end[=hh:mm:ss]   Time to end clip
			 Optional. If not provided, or 'inf', end clip at end of SOURCE.
   SOURCE                Input file name.
   TARGET	 	 Output file name.
			 Optional. If not provided, '-cut' is appended to SOURCE."

[[ $# -le 1 ]] && { echo "$usage"; exit 0; }

params="$(getopt -o s:e: --long start:,end: --name "$0" -- "$@")"
eval set -- "$params"

while true
do
    case "$1" in
        -s|--start)
	    start_hms=$2
            shift 2
            ;;
        -e|--end)
            end_hms=$2
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Unknown option: $1" >&2
            exit 1
            ;;
    esac
done

in_file=$1
out_file=$2

if [ -z "${start_hms}" ] || [ "${start_hms}" == "0" ]; then
    start_hms=00:00:00
fi

if [ -z "${end_hms}" ] || [ "${end_hms}" == "inf" ]; then
    end_hms=$(ffmpeg -i "${in_file}" 2>&1 | \
	grep Duration | cut -f 4 -d ' '| sed 's/,//')
fi

if [ -z "${out_file}" ] || [ "$in_file" == "$out_file" ]; then
	file_ext=$(echo "${in_file}" | sed 's/^.*\.\([a-zA-Z0-9]\+\)$/\1/')
	file_name=$(echo "${in_file}" | sed 's/^\(.*\)\.[a-zA-Z0-9]\+$/\1/')
	out_file="${file_name}-cut.${file_ext}"
fi

start_h=10#$(echo "${start_hms}" | cut -d ':' -f 1)
start_m=10#$(echo "${start_hms}" | cut -d ':' -f 2)
start_s=10#$(echo "${start_hms}" | cut -d ':' -f 3 | cut -d '.' -f 1)
let "start = ( start_h * 60 + start_m ) * 60 + start_s"

end_h=10#$(echo "${end_hms}" | cut -d ':' -f 1)
end_m=10#$(echo "${end_hms}" | cut -d ':' -f 2)
end_s=10#$(echo "${end_hms}" | cut -d ':' -f 3 | cut -d '.' -f 1)
let "end = ( end_h * 60 + end_m ) * 60 + end_s"

let "duration = (end - start)"

ffmpeg \
    -ss ${start} -t ${duration} \
    -i "${in_file}" \
    -acodec copy -vcodec copy -async 1 \
    "${out_file}"

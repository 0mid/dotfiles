#!/bin/bash

# Run this script in the directory of the LaTeX project under git version control.

dotgit_hooks=./.git/hooks
dotgit_hooks_post_xxx="
${dotgit_hooks}/post-commit
${dotgit_hooks}/post-checkout
${dotgit_hooks}/post-merge"
cmd2append='bash $HOME/software/gitinfo2/post-xxx-sample.txt'

for fname in $dotgit_hooks_post_xxx; do
    if [ ! -f "$fname" ]; then
	echo '#!/bin/bash' >> "$fname"
    fi

    echo "$cmd2append" >> "$fname"
    chmod u+x "$fname"
done

echo -e 'Example Usage:
\\documentclass{article}
\\usepackage{gitinfo2}
\\begin{document}
Hello, World!
\\gitAuthorName, \\gitAuthorEmail
(revision: \\gitAbbrevHash, \\gitAuthorDate, \\gitDirty)
\\end{document}
'

#!/bin/sh

# Call in a git repository directory.

git filter-branch -f --env-filter '

old_email="omidlink@gmail.com"
new_name="Omid"
new_email=""

if [ "$GIT_COMMITTER_EMAIL" = "$old_email" ]
then
    export GIT_COMMITTER_NAME="$new_name"
    export GIT_COMMITTER_EMAIL="$new_email"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$old_email" ]
then
    export GIT_AUTHOR_NAME="$new_name"
    export GIT_AUTHOR_EMAIL="$new_email"
fi
' --tag-name-filter cat -- --branches --tags

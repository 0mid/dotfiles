#!/bin/bash

# own phone:
# lessmal-android.sh

# others:
# lessmal-android.sh -k contacts -k youtube

source dotfiles_common.sh

uninstall_or_disable() {
    local fullapp=$1

    # Android 7 calls them by the first two names.
    contacts_re='com.android.contacts|com.android.dialer'
    contacts_re+='|com.android.providers.contacts'
    contacts_re+='|com.google.android.contacts'
    contacts_re+='|com.google.android.syncadapters.contacts'

    youtube_re='com.google.android.youtube'

    echo -n "Searching for ${fullapp} ... "
    android_appexists "${fullapp}"
    if [[ $? == 0 ]]; then      # if ${fullapp} exists
        if [[ ${keep_contacts} == t ]]; then
            if [[ ${fullapp} =~ ${contacts_re} ]]; then
                echo "KEEPING, due to -k"
                return
            fi
        fi
        if [[ ${keep_youtube} == t ]]; then
            if [[ ${fullapp} =~ ${youtube_re} ]]; then
                echo "KEEPING, due to -k"
                return
            fi
        fi

        if [[ ${fullapp} =~ googlequicksearchbox ]]; then
           read -e -p "
If you're on Android 7, uninstalling googlequicksearchbox makes
EVERYTHING DISAPPEAR. Continue [y/N]? " yn
           [[ "${yn}" == [Yy]* ]] || return
        fi
        echo -e "\nUNINSTALLING ${fullapp} ..."
        adb shell "pm uninstall --user 0 ${fullapp}" || \
        adb shell "pm disable-user ${fullapp}"
    else
        echo "NOT installed"
    fi
}

# Use, e.g.,
#
# adb shell pm list packages | grep -i 'samsung'
#
# or App Manager (FDroid) to find the full name (e.g.,
# com.android.chrome) for an app to uninstall (if possible) or at
# least disable (if they made it so non-free that root is needed to
# uninstall).

lessmal_android_base() {
# `# ...`: comments in multi-line bash commands,
# https://stackoverflow.com/a/12797512. Each invokes a sub-shell, so
# they are 'expensive comments'. Use only in non-performance critical
# places.

# com.android
for app in \
  chrome \
  `# Google Play Store` \
  vending \
  sprint.hiddenmenuapp sdm.plugins.sprintdm printspooler dreams.basic \
  facelock calllogbackup wallpaper.livepicker egg contacts dialer \
  captiveportallogin providers.{contacts,calendar,partnerbookmarks} \
  hotwordnerollment.{okgoogle,xgoogle} bookmarkprovider
do
    fullapp=com.android.${app}
    uninstall_or_disable ${fullapp}
done
}

lessmal_google() {
# com.google
for app in \
  mainline.telemetry
do
    fullapp=com.google.${app}
    uninstall_or_disable ${fullapp}
done

# com.google.android
for app in \
  gm  `# Gmail` \
  dialer contacts \
  syncadapters.{calendar,contacts} launcher \
  youtube videos music drive calendar calculator feedback \
  marvin.talkback talk gallery3d backuptransport \
  printservice.recommendation \
  `# This is y/N checked in uninstall_or_disable, as on Android 7` \
  `# uninstalling googlequicksearchbox makes EVERYTHING DISAPPEAR` \
  googlequicksearchbox \
  apps.youtube.music{,.setupwizard} \
  `# tachyon: Duo` \
  apps.{googleassistant,photos,messaging,tachyon,docs} \
  apps.{turbo,wellbeing,pixelmigrate,subscriptions.red} \
  apps.{inputmethod.{hindi,zhuyin,korean,pinyin},cloudprint} \
  apps.nbu.files \
  `# Google Private Compute Services (added in ~ April 2022 update)` \
  as.oss \
  projection.gearhead `# Android Auto` \
  tts `# Text to Speech`
do
    fullapp=com.google.android.${app}
    uninstall_or_disable ${fullapp}
done
}

lessmal_samsung() {
# com.samsung
for app in \
  systemui.bixby2 `# NOT cause of 'menu currently not available'` \
  knox.securefolder \
  storyservice `# Gallary stories` \
  app.newtrim `# video trimmer` \
  SMT `# Text to Speech engine`
do
    fullapp=com.samsung.${app}
    uninstall_or_disable ${fullapp}
done

# com.samsung.android
for app in \
  app.reminder dialer aremoji aremojieditor ardrawing arzone \
  beaconmanager easysetup `# both: nearby device scanning` \
  app.contacts app.galaxyfinder \
  app.camera.sticker.facearavatar.preload stickercenter livestickers \
  coldwalletservice `# blockchain keystore (w/ 4 trackers no less)` \
  mobileservice `# group sharing (Samsung experience)` \
  forest `# digital wellbeing` \
  mdx `# Link to Windows Service` \
  da.daagent `# dual messenger` \
  video calendar opencalendar \
  app.samsungapps appthemestore themecenter \
  privateshare app.simplesharing allshare.service.fileshare \
  app.sharelive allshare.service.mediashare \
  scloud messaging \
  mateagent `# Galaxy Friends` \
  honeyboard `# keyboard` \
  kidsinstaller \
  {tapack.,}authfw spayfw `# PaymentFramework` \
  samsungpass samsungpassautofill \
  knox.analytics.uploader \
  app.spage `# "Samsung Free (w/ 4 trackers no less)"` \
  fast `# "Secure WiFi" (proprietary VPN)` \
  ipsgeofence `# "Visit In" (proprietary location)` \
  mdecservice `# Call & test on other devices` \
  fmm `# find my mobile` \
  svoiceime `# voice input` \
  aware.service `# quick share` \
  service.health \
  watchmanagerstub `# wearable manager installer` \
  game.gametools game.gamehome game.gos `# optim serv` \
  rubin.app `# customization service (w/ 2 trackers no less)` \
  bixby.{service,agent} `# {dictation,voice}` \
  knox.container{agent,core} `# "Work profile"` \
  app.settings.bixby \
  mdx.quickboard `# Media and devices` \
  app.routines \
  bixby.wakeup \
  visionintelligence \
  bixbyvision.framework
  # do NOT uninstall com.samsung.android.providers.contacts; call
  # history won't be stored (!) and saving contacts available in one
  # app (e.g., Signal) to a contacts app (Simple Contacts Pro) will
  # break, throwing 'No appropriate app found'.
  #
  # do NOT uninstall com.samsung.android.providers.media; (native)
  # Screenshot (volume down+power) and Samsung Camera Video will
  # BREAK: the former gives 'Cannot take screenshot due to security
  # policy' and the latter just crashes (although taking regular
  # pictures w/ Samsung camera will continue to work). However, both
  # functionality could be replaced with Free Software (ScreenShot
  # Tile and Open Camera from FDroid).
do
    fullapp=com.samsung.android.${app}
    uninstall_or_disable ${fullapp}
done
}

lessmal_motorola() {
# com.motorola
for app in \
  moto{,care{,.internal},tour} faceunlock photoeditor screenshoteditor \
  contacts.preloadcontacts{,.overlay.vzw} comcastext \
  comcast.settings.extensions gamemode \
  demo discovery attvowifi att.phone.extensions help genie bug2go \
  android.{fmradio,nativedropboxagent} fmplayer \
  android.providers.chromehomepage brapps lifetimedata ccc.notification
do
    fullapp=com.motorola.${app}
    uninstall_or_disable ${fullapp}
done

# com.lenovo
for app in lsf.user
do
    fullapp=com.lenovo.${app}
    uninstall_or_disable ${fullapp}
done
}

lessmal_misc() {
# Samsung account
fullapp=com.osp.app.signin
uninstall_or_disable ${fullapp}

# com.sec
for app in \
  spp.push `# Samsung Push Service (w/ 2 trackers no less)`
do
    fullapp=com.sec.${app}
    uninstall_or_disable ${fullapp}
done

# com.sec.android
for app in \
  app.sbrowser mimage.avatarstickers app.billing `# Samsung checkout` \
  app.ve.vebgm `# Samsung Editing Assets` \
  autodoodle.service
do
    fullapp=com.sec.android.${app}
    uninstall_or_disable ${fullapp}
done

# com.facebook
for app in \
    services katana system appmanager
do
    fullapp=com.facebook.${app}
    uninstall_or_disable ${fullapp}
done

# com.verizon
for app in loginengine.unbranded
do
    fullapp=com.verizon.${app}
    uninstall_or_disable ${fullapp}
done
}

usage() {
    cat <<EOF
Usage: ${scriptname} [OPTIONS]
1. Ensure Developer mode is enabled on the Android device:
   in "Settings/About phone" tap on "Build number" enough times
   (7 as of now). Ensure "USB Debugging" is allowed in
   - Moto + Android 7: "Settings/Developer options"
   - Moto + Android 11: "Settings/System/Developer options".
   - Samsung + Android 12: "Settings/Developer options"
2. Ensure the Android device is connected, e.g., via the USB cable.
   On first connection, check and accept the (RSA) key
   used for communications from this host machine to the given Android
   device.

OPTIONS

Mandatory arguments to long options are mandatory for short options
too.

-k, --keep=VAL
    do NOT uninstall or disable VAL, where, as of now, VAL may be
    contacts
       e.g., for WhatsApp (which others may insist to install and be
       subjugated by), which will ONLY allow its 'users' to
       communicate with entries coming from Google Contacts, not even
       allowing them to enter a phone number manually to send/receive
       messages to/from, as opposed to what Signal does.

    youtube
       e.g., for casting to a TV (which does not require login)
       connected to the same WiFi as one's phone.
EOF
    exit $1
}

process_keep_opt() {
    local keep="$1"
    case "${keep}" in
        '') return;;
        youtube) keep_youtube=t;;
        contacts) keep_contacts=t;;
        *)
            echo "unknown value for -k, --keep, '${keep}'."
            usage 1;;
    esac
}

main() {
    scriptname="$(basename ${BASH_SOURCE[0]})"

    cmdensure adb

    # pm: package manager
    adb shell pm list users || usage $?

    # query for connected devices
    adb devices -l

    # Each long option name in longopts may be followed by one colon (:)
    # to indicate it has a required argument, and by two colons (::) to
    # indicate it has an optional argument.
    shortopts=k:
    longopts=keep:
    args="$(getopt --name ${scriptname} -o ${shortopts} --long ${longopts} -- "$@")"
    [[ $? -ne 0 ]] && usage 1
    eval set -- "${args}"

    while [[ $# -gt 0 ]]; do
        case "$1" in
            -k | --keep)
                shift
                keep=$1
                process_keep_opt "${keep}"
                ;;
            --)
                # end of options
                shift
                break
                ;;
        esac
        shift
    done

    lessmal_android_base
    lessmal_google
    lessmal_samsung
    lessmal_motorola
    lessmal_misc
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi

#!/bin/bash

using_xfce_pwr_mngr=no

# Do not use own script (below) if using xfce power manager.
# Otherwise, upon resume from hibernation the system goes back to
# sleep because, via xfce power manager and the script below, we are
# basically doing the same thing twice.
if [[ ${using_xfce_pwr_mngr} == 'yes' ]]; then
    exit 0
fi

grep -q closed /proc/acpi/button/lid/LID/state && systemctl suspend

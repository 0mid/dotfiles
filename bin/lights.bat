@echo off
set homebindir=%userprofile%\bin
set cygwindir=%userprofile%\opt\cygwin
set minttydir=%cygwindir%\bin

"%minttydir%"\mintty --hold error --exec /bin/bash "%homebindir%"\lights.sh %*

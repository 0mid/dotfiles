#!/bin/bash

# To ignore changes in the files changed by this script run
#
# git update-index --skip-worktree _config/gtk-3.0/settings.ini _config/lxsession/LXDE/desktop.conf
#
# to undo above run
# git update-index --no-skip-worktree _config/gtk-3.0/settings.ini _config/lxsession/LXDE/desktop.conf

_rofi() {
    command -v rofi &>/dev/null || return

    local theme="$1"
    local roficonfig=~/.config/rofi/config.rasi
    if [[ $theme == light ]]; then
        # default theme is light
        [[ -f ${roficonfig} ]] && mv -f ${roficonfig}{,.bak}
    else
        cd ~/.local/stow/rofi-*
        ln -sfr share/rofi/themes/solarized.rasi \
           ${roficonfig}
    fi
}

_emacs() {
    command -v emacsclient &>/dev/null || return

    local theme="$1"
    local emacs_theme="solarized-${theme}"
    emacsclient -e "(load-theme '${emacs_theme} t)" &>/dev/null &
}

_lxde() {
    command -v lxpanelctl &>/dev/null || return

    local theme="$1"
    local gtk_theme="Adwaita"
    local lxde_desktop_conf=~/.config/lxsession/LXDE/desktop.conf
    [[ ${theme} == dark ]] && gtk_theme="Adwaita-dark"

    # Firefox and LibreOffice (but maybe more applications), which are
    # non-LXDE-specific, GTK applications, only need the LXDE-specific
    # desktop.conf theme to be flipped to light/dark to change to their
    # respective light/dark themes. Strangely, they do NOT respond to/care
    # about the theme state of either of the GTK2 or GTK3 rc/settings
    # files.

    # Without --follow-symlinks, the symlink will be 'undone': sed -i
    # reads and makes a temp copy of the original file, modifies the temp
    # copy, and renames (mv) the temp copy to the original file,
    # effectively overwriting the original symlink with an ordinary file.

    # The unique key name we're looking for is /ThemeName (identifying
    # sNet/ThemeName), hence the beginning / in the sed's regexp #address#
    # we're specifying.

    sed --follow-symlinks -i \
        "\#/ThemeName=# s#=.*#=${gtk_theme}#" "${lxde_desktop_conf}"

    # Touch the symlink itself (and not its destination) for that to be
    # seen as a "change" in the file by LXDE. Otherwise, changing the
    # target of the link cannot be detected by just looking at the link
    # file (through inotify, e.g.).
    touch --no-dereference "${lxde_desktop_conf}"
    lxpanelctl restart
}

_gtk() {
    local gtk3_settings_ini=~/.config/gtk-3.0/settings.ini
    [[ -f "${gtk3_settings_ini}" ]] || return

    local theme="$1"
    local gtk_theme="Adwaita"
    [[ ${theme} == dark ]] && gtk_theme="Adwaita-dark"

    sed --follow-symlinks -i "
\#gtk-theme-name=# s#=.*#=${gtk_theme}#" "${gtk3_settings_ini}"

    touch --no-dereference "${gtk3_settings_ini}"
}

_term() {
    local theme="$1"
    ~/bin/term-${theme}.sh
}

_keepassxc() {
    command -v keepassxc &>/dev/null || return

    local theme="$1"
    local qtstyle='Adwaita'
    [[ ${theme} == dark ]] && qtstyle='Adwaita-Dark'
    local pid_keepassxc=$(pgrep keepassxc)
    if [[ -n ${pid_keepassxc} ]]; then
        kill ${pid_keepassxc}
        # output the tail of /dev/null (i.e., nothing) until
        # ${pid_keepassxc} dies; i.e., effectively, wait for
        # ${pid_keepassxc} to terminate (due to above kill) before
        # proceeding [https://stackoverflow.com/a/41613532].
        tail --pid=${pid_keepassxc} -f /dev/null
    fi
    QT_STYLE_OVERRIDE=${qtstyle} keepassxc &
    disown
}

theme=light
[[ $1 == --off || $1 == off ]] && theme=dark

_term ${theme}
_emacs ${theme}
_rofi ${theme}
_lxde ${theme}
_gtk ${theme}
_keepassxc ${theme}

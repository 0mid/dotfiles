#!/bin/bash

# This script is bound to a key, in .config/openbox/lxde-rc.xml.

xrandr --output DP-3   --off	\
       --output DP-2   --off	\
       --output DP-1   --off	\
       --output HDMI-3 --off	\
       --output HDMI-2 --off	\
       --output HDMI-1 --off	\
       --output VGA-1  --off	\
       --output LVDS-1 --auto

lxpanelctl restart && openbox --restart

#! python3
# Moves the mouse pointer on a square.
# To stop move mouse to a corner of the screen.


import pyautogui, time

# Time taken to traverse one side of a square
travelTime = 0.25

# Time between pointer moves
sleepTime = 10

# Side length of the square
distance = 50

while True:
    time.sleep(sleepTime)
    pyautogui.move(distance, 0, duration = travelTime)
    time.sleep(sleepTime)
    pyautogui.move(0, distance, duration = travelTime)
    time.sleep(sleepTime)
    pyautogui.move(-distance, 0, duration = travelTime)
    time.sleep(sleepTime)
    pyautogui.move(0, -distance, duration = travelTime)

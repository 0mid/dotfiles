#!/bin/bash

# .desktop files under ~/.config/autostart/ are launched automatically
# during the startup of the user's desktop environment after the user
# has logged in. This file, opensession.sh, is specified to be
# executed (Exec) by ~/.config/autostart/opensession.desktop.

synclient TouchpadOff=1
keepassxc &
firefox &
goldendict &
emacs --maximized &

# guake has a gconf-based config and it does NOT read or write the
# following file by default.
guake && \
    guake --save-preferences ~/.config/guake/prefs.conf && \
    guake --restore-preferences ~/.config/guake/prefs.conf &

pcmanfm &
nohup watchliterature.sh --watch-only > /dev/null 2>&1 &

# for syncing primary (mouse middle) and clipboard
diodon &

# for adjusting the color temperature of screen
redshift-gtk &

# /etc/X11/Xsession.d/30x11-common_xresources runs 'xrdb -merge
# $USRRESOURCES', which (for some reason) does not apply my settings
# (at least colors) due to '-merge'.
xrdb -I$HOME ~/.Xresources 2> /dev/null

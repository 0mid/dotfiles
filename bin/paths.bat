@echo off
rem Name chosen to be 'paths' rather than path or env to differentiate
rem from 'path' and 'env' commands.

set action=%~1
call :%action%
exit /b %errorlevel%

:delAppPaths
rem Windows imbeciles add entries (e.g., python.exe) to the following
rem key that in cmd take priority over, and hence shadow, those in
rem PATH! For example, without the following deletion, running
rem 'python' gives
rem
rem Python was not found; run without arguments to install from the
rem Microsoft Store, or disable this shortcut from Settings > Manage
rem App Execution Aliases.
set key=HKCU\Software\Microsoft\Windows\CurrentVersion\App Paths
reg query "%key%" 1>nul 2>&1 && reg delete "%key%" /f
exit /b

rem WARNING: destructive; resets PATH to Windows defaults
:default
:reset
set WindowsAppsdir=%APPDATA%\..\Local\Microsoft\WindowsApps
rem Windows will FORCE-PREPEND
rem C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\System32\WindowsPowerShell\v1.0\;C:\WINDOWS\System32\OpenSSH\
rem to PATH if its not there in that exact order. So, we only need to
rem explicitly add to PATH anything additional to that.
set key=HKCU\Environment
set val=Path
reg add "%key%" /f /v "%val%" /t REG_SZ /d %WindowsAppsdir% >nul
reg query "%key%" /v "%val%"

rem Trigger a broadcast of WM_SETTINGCHANGE message.
setx USERNAME %USERNAME% >nul
exit /b

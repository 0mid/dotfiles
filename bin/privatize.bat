@echo off
setlocal

set arg=%~1

pushd "%arg%" 2>nul
if "%errorlevel%"=="0" (
   @rem Traverse to subdirs if %arg% is a dir
   set takeown_dir_opts=/r /d y
   set icacls_dir_opts=/t
)
popd

rem Set ownership to %username%
takeown %takeown_dir_opts% /f "%arg%" >nul

rem Replace with default inherited accesses
rem https://serverfault.com/a/628532
icacls "%arg%" /reset /q /c %icacls_dir_opts% >nul

rem Disable inheritance and copy the accesses
icacls "%arg%" /inheritance:d /q /c %icacls_dir_opts% >nul

rem Grant full (F) access to %username% and remove all inherited accesses
rem https://serverfault.com/a/578170
icacls "%arg%" /grant:r %username%:F /inheritance:r /q /c %icacls_dir_opts% >nul

rem Remove all occurences of users (g: and their granted accesses)
icacls "%arg%" /remove:g "NT AUTHORITY\SYSTEM" BUILTIN\Administrators /q /c %icacls_dir_opts% >nul

rem Verify
icacls "%arg%" %icacls_dir_opts% /q

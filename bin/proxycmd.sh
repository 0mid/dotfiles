#!/bin/sh

is_cygwin() {
    uname -s | grep -iq cygwin
}

have_connect() {
    command -v connect >/dev/null 2>&1
}

have_nmap_nc() {
    nc --version 2>&1 | grep -iq ncat
}

# socks_proxy env var is set in dotfiles_'s .bash_local_env and
# init-local.bat.

if is_cygwin || have_connect; then
    # On Windows, connect will be from <gitdir>\mingw64\bin\connect.exe,
    # linked under %USERPROFILE%\bin, which is put on the PATH by
    # appinstall.bat git.
    #
    # NOT using Cygwin's nc (version BSD February 7, 2012), as it has a heap corruption
    # issue :
    #
    # 0 [main] nc (8156) C:\Users\...\cygwin\bin\nc.exe: *** fatal error -
    # cygheap base mismatch detected - 0x18034B408/0x180346408.
    # This problem is probably due to using incompatible versions of the
    # cygwin DLL.
    #
    # Deleting all other versions of cygwin1.dll (i.e., one that came with
    # ConEmu), restarting, or adding an Exploit Protection bottom-up ASLR
    # exception (off) for nc did NOT help. Might need an exception for
    # every prog that loads msys2 dll and uses fork
    # (https://github.com/desktop/desktop/issues/3096,
    # https://cygwin.com/faq/faq.html#faq.api.fork).
    cmd="connect -a none -S ${socks_proxy}"
elif have_nmap_nc; then
    # Nmap's nc, e.g., in GNU/Linux Redhat 8 or Debian 11/12.
    # WSL.
    cmd="nc --proxy-type socks5 --proxy ${socks_proxy}"
else
    # BSD's nc, e.g., in GNU/Linux Ubuntu 20.04.
    cmd="nc -X 5 -x ${socks_proxy}"
fi

${cmd} "$@"

@echo off
setlocal

rem N.B. If missing, error is cryptic: !pscmd! : The term '!pscmd!' is not recognized as the name of a cmdlet, function, script file, or operable program.
setlocal EnableDelayedExpansion

rem This could be put into, e.g., File Explorer's Address Bar to go to
rem 'Home'.
set quickaccess_location=shell:::{679f85cb-0220-4080-b29b-5540cc05aab6}

set quickaccess_storage_folder=%AppData%\Microsoft\Windows\Recent\AutomaticDestinations
set quickaccess_storage_file=f01b4d95cf55d32a.automaticDestinations-ms

set "pscmd=$shellapp = New-Object -ComObject shell.application"

rem This for command assigns the 1st parameter to "i" and the rest
rem (denoted by '*') are assigned to "j"
rem [https://stackoverflow.com/a/18594306].
for /f "usebackq tokens=1*" %%i in (`echo %*`) do (
    set action=%%i
    set folders=%%j
)

call :%action%
exit /b

:pin
for %%f in (%folders%) do (
    if not exist "%%~f" mkdir "%%~f"
    set "pscmd=!pscmd!; $shellapp.NameSpace('%%~f').Self.InvokeVerb('pintohome')"
)
powershell -Command "%pscmd%"
exit /b

:unpin
rem In a command block (parentheses) all percent variables are
rem expanded BEFORE the block will be executed, so "%VAR%" expands to
rem the contents of VAR BEFORE entering the block (e.g., the FOR-Loop
rem below). !VAR! expands to the content of VAR during the FOR-loop.
rem This 'delayed expansion' behavior must be enabled, and is needed,
rem e.g., for concatenation.
for %%f in (%folders%) do (
    set "pscmd=!pscmd!; $item=$shellapp.Namespace('%quickaccess_location%').Items() | where {$_.Path -eq '%%~f'}; if ($item) { $item.InvokeVerb('unpinfromhome') }"
)
powershell -Command "%pscmd%"
exit /b

:default
copy /y "%quickaccess_storage_folder%\%quickaccess_storage_file%"  "%quickaccess_storage_folder%\%quickaccess_storage_file%.bak" >nul 2>&1 && del /f /q "%quickaccess_storage_folder%\%quickaccess_storage_file%"
exit /b

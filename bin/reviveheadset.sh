#!/bin/bash

# Revive the headset jack when there is no audio output or input through
# it. Once the system is up, the sound module appears to be extensively
# employed by the goings-on of the kernel, so simply entering =sudo
# modprobe -r snd_hda_intel= results in the following message: =modprobe:
# FATAL: Module snd_hda_intel is in use.= This is because either the
# module is in fact still in use, or some function from the module has
# crashed; the kernel Linux has recovered, but the module is locked in
# memory. The kernel is only willing to unload modules if their
# =module_exit= function returns successfully. We may be able to force
# kill such a module (to unlock it from memory) and then remove it as
# follows. First, make sure the headset is not jacked in. Otherwise, the
# =snd_hda_intel= module will be in use and cannot be remove. Then, list
# all open files (=lsof -t=) under =/dev/snd/*=, where =-t= specifies
# that =lsof= should produce terse output with process identifiers only
# and no header, e.g., so that the output may be piped to kill. Then,
# immediately after a successful kill, remove the =snd_hda_intel=
# kernel module, wait, and add the module back. This should reinitialize
# all the devices controlled by the module and in particular resolve the
# headset jack problem.

until modprobe -r -v snd_hda_intel
do
    sndpid=$(lsof -t /dev/snd/*)
    if [[ ! -z ${sndpid} ]]; then
	# cat /proc/${sndpid}/stack
	# cat /proc/${sndpid}/status
	# ps -Fl -p ${sndpid}

	kill ${sndpid}
    fi
done

sleep 2s
modprobe -v snd_hda_intel

#!/bin/bash

# If run with -k, do a 'hard' revive, killing lxpanel first.
if [[ "$1" == "-k" ]]; then
    killall lxpanel && sleep 1s && \
        lxpanelctl restart && openbox --restart

    # Load the panel profile under the LXDE directory
    # (i.e., load the file ~/.config/lxpanel/LXDE/panels/panel)
    nohup lxpanel --profile LXDE > /dev/null 2>&1 &

else
    lxpanelctl restart && openbox --restart
fi

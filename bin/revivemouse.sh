#!/bin/bash

# Revive touch pad or (red) nob by removing and adding back the
# =psmouse= kernel module. Then, turn touch pad off.

modprobe -r psmouse
sleep 2s
modprobe psmouse
sleep 1s
synclient TouchpadOff=1

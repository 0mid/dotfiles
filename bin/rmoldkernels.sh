#!/bin/sh

# Remove kernels except for the current and next to current ones.
#
# Usage:
# rmoldkernels.sh
#
# Details:
# http://tuxtweaks.com/2010/10/remove-old-kernels-in-ubuntu-with-one-command/

rmkernels () {
    local
    cur_kernel=$(uname -r|sed 's/-*[a-z]//g'|sed 's/-386//g')

    local
    pre_kernel=$(echo ${cur_kernel} | \
	tr '' \\\n | awk 'BEGIN{FS="-";OFS="-"} {print $1,$2-1}')

    local
    kernel_pkg="linux-(image|headers|ubuntu-modules|restricted-modules)"

    local
    meta_pkg="${kernel_pkg}-(generic|i386|server|common|rt|xen|ec2)"

    echo ""
    echo "*********************** W A R N I N G ***********************"
    echo "After entering your password, and before answering Y,"
    echo "MAKE SURE the following versions are NOT listed to be removed:"
    echo "Current Kernel: ${cur_kernel}"
    echo "Previous Kernel: ${pre_kernel}"
    echo "*************************************************************"
    echo ""

    sudo -k apt-get purge \
	$(dpkg --list | \
	egrep $kernel_pkg | \
	egrep --invert-match "${cur_kernel}|${pre_kernel}|${meta_pkg}" | \
	awk '{print $2}')
}

rmkernels
exit 0

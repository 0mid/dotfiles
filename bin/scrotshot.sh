#!/bin/bash

dirName=~/pictures/screenshots
mkdir -p ${dirName}
qualityPercent=80
delaySeconds=1

scrot --quality ${qualityPercent} \
      --delay ${delaySeconds}     \
      --count                     \
      --silent                    \
      ${dirName}/'%Y-%m-%d_%H-%M-%S_$wx$h.png'

#!/bin/bash
# Parse ~/.keyboard, extracting the xkb settings, and run equivalent
# setxkbmap command(s) in X.

source ~/.keyboard
setxkbmap_cmd=(setxkbmap -model "${XKBMODEL}")
setxkbmap_cmd+=(-layout "${XKBLAYOUT}")

# Clear previously-set options first; otherwise, ours will be appended
# to whatever is already there. That is, 'setxkbmap -option' will
# reset the options to default.
setxkbmap_cmd+=(-option)

# -r      : do not allow backslashes to escape any characters
# -a array: assign the words read to sequential indices of the array
#           variable ARRAY, starting at zero
IFS=',' read -r -a xkbopts <<< "${XKBOPTIONS}"
for opt in "${xkbopts[@]}"; do
    setxkbmap_cmd+=(-option "${opt}")
done

"${setxkbmap_cmd[@]}"

#!/bin/bash

# This script need be run only once, but it is idempotent.

u=tunneller
u_home=/home/$u
u_pubkeysdir=~/.keys/$u
u_authorized_keys=${u_home}/.ssh/authorized_keys

isownsystem() {
    [[ $(hostname -s) == gnupower ]]
}

# to run separately:
# (source $(which sshRvnc-init.sh); add_keys)
add_keys() {
    # if the OWNERSHIPS are wrong (i.e., if $u is not the owner, but,
    # e.g., root is, which would be the case without -u $u in sudo),
    # https://superuser.com/a/1145465, on client side (ssh -vvv)
    #
    # send packet: type 50
    # receive packet: type 51
    # Permission denied (publickey)
    #
    # on host side (sshd), NOTHING useful in /var/log/auth.log
    #
    # error: kex_exchange_identification: Connection closed by remote host
    #
    # With correct ownership:
    #
    # send packet: type 50
    # receive packet: type 60
    # Server accepts key: /home/clientuser/.ssh/id_rsa
    # sign_and_send_pubkey
    # send packet: type 50
    # receive packet: type 52
    # Authentication succeeded (publickey)
    sudo -u $u sh -c "
mkdir -p -m u=rwx,go= ${u_home}/.ssh &&
umask u=rw,go= && touch ${u_authorized_keys}"

    for pubkeyfile in ${u_pubkeysdir}/*.pub; do
        # load in pubkey (removing any newlines)
        pubkey=$(tr -d '\n' < ${pubkeyfile})
        # if ${pubkey} is not already in ${u_authorized_keys}
        if ! sudo grep -qxF "${pubkey}" ${u_authorized_keys}; then
            sudo -u $u sh -c "echo ${pubkey} >>${u_authorized_keys}"
        fi
    done

    # Set the ownership and permissions, in case the dirs and files were
    # previously created (outside the above line with the right ownership
    # and permissions), in which case the above will NOT have changed the
    # ownership and permissions to the right ones.
    sudo chown $u:$u /home/$u{,/.ssh{,/authorized_keys}}
    sudo chmod u=rwx,go= /home/$u{,/.ssh}
    sudo chmod u=rw,go= /home/$u/.ssh/authorized_keys
}

adduser_tunneller() {
    # GECOS field is a comma separated list as such: Full name,Room
    # number,Work phone,Home phone
    sudo adduser --gecos '' --disabled-password --shell /bin/false $u
    sudo chmod go= ${u_home}
}

sshd_config_allowusers() {
    if ! $(sudo which sshd &>/dev/null); then
        sudo apt update && sudo apt install openssh-server
    fi

    sshdconfig=/etc/ssh/sshd_config
    sshports="Port 2718"
    allowusers="$u"
    if ! isownsystem; then
        sshports+="\nPort 22"
        allowusers+=" root"
    fi

    if grep -q '#Port 22' ${sshdconfig}; then
        sudo sed -i'.bak' \
             "s/#Port 22/${sshports}/" ${sshdconfig}
    fi

    if ! isownsystem; then
        if grep -q '#PasswordAuthentication yes' ${sshdconfig}; then
            sudo sed -i'.bak' \
                 's/#PasswordAuthentication yes/PasswordAuthentication no/' \
                 ${sshdconfig}
        fi
    fi

    if isownsystem; then
        if grep -q '#PermitRootLogin prohibit-password' ${sshdconfig}; then
            sudo sed -i'.bak' \
                 's/#PermitRootLogin prohibit-password/PermitRootLogin no/' \
                 ${sshdconfig}
        fi
    fi

    if ! grep -q "AllowUsers ${allowusers}" ${sshdconfig}; then
        # Port is not allowed within a Match block; if there, it is a
        # syntax error in sshd_config, resulting in refused
        # connections from outside or below error messages from inside
        # upon a restart attempt for sshd:

        # root@vultr:~/dotfiles/bin# sudo systemctl restart sshd
        # Job for ssh.service failed because the control process exited with error code.
        # See "systemctl status ssh.service" and "journalctl -xe" for details.
        # root@vultr:~/dotfiles/bin# systemctl status ssh.service
        # ● ssh.service - OpenBSD Secure Shell server
        #      Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
        #      Active: failed (Result: exit-code) since Wed 2022-08-10 06:53:47 UTC; 4s ago
        #        Docs: man:sshd(8)
        #              man:sshd_config(5)
        #     Process: 22602 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=255/EXCEPTION)
        #         CPU: 6ms

        # Aug 10 06:53:47 vultr systemd[1]: ssh.service: Control process exited, code=exited, status=255/EXCEPTION
        # Aug 10 06:53:47 vultr systemd[1]: ssh.service: Failed with result 'exit-code'.
        # Aug 10 06:53:47 vultr systemd[1]: Failed to start OpenBSD Secure Shell server.
        # Aug 10 06:53:47 vultr systemd[1]: ssh.service: Scheduled restart job, restart counter is at 5.
        # Aug 10 06:53:47 vultr systemd[1]: Stopped OpenBSD Secure Shell server.
        # Aug 10 06:53:47 vultr systemd[1]: ssh.service: Start request repeated too quickly.
        # Aug 10 06:53:47 vultr systemd[1]: ssh.service: Failed with result 'exit-code'.
        # Aug 10 06:53:47 vultr systemd[1]: Failed to start OpenBSD Secure Shell server.
        # root@vultr:~/dotfiles/bin# /usr/sbin/sshd -T
        # /etc/ssh/sshd_config line 129: Directive 'Port' is not allowed within a Match block
        sudo sh -c "cat <<EOF >>${sshdconfig}
AllowUsers ${allowusers}
Match User $u
  AllowTcpForwarding yes
  X11Forwarding no
  AllowAgentForwarding no
  ForceCommand /bin/false
EOF"
    fi

    # on systems before/without systemd: sudo service ssh restart
    sudo systemctl restart sshd
}

main() {
    adduser_tunneller
    sshd_config_allowusers
    add_keys
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi

#!/bin/bash

prog="$(basename ${BASH_SOURCE[0]})"

usage() {
    local errmsg="$2"
    cat <<EOF
${errmsg}
Usage: ${prog} IP [PORT]

Set up a "reverse ssh" connection to remote IP (using ssh port PORT if
PORT is provided, or the default ssh port 22 otherwise), i.e., having
some ports of remote IP forwarded to this machine (localhost). Then,
set up a x11vnc server on localhost to which remote IP can connect by
connecting to the appropriate (VNC) forwarded port on itself.
EOF
    exit $1
}

# West :> ssh East

# makes West (ssh orig) the local/client (L) end of the secure tunnel
# and East (ssh dest) the remote/server (R) end (sshd end).

# so, to denote a port forward from West to East:
# -L Wport:localhost:Eport
# and to denote a port forward from East to West:
# -R Eport:localhost:Wport
# note that 'localhost' represents the machine on the receiving end of
# the port forward in both cases, East in the first case and West in the
# second. It has nothing to do with local (L) or remote (R).

source dotfiles_common.sh

remoteMachineIP=$1
remoteMachinePort=${2:-22}
# written to a sshRvnc.desktop by 'appinstall.sh sshRvnc_helpee=N'
helpeeIdx=${HELPEEIDX:-0}
Ouser=tunneller
# must be open/forwarded in router of ${remoteMachineIP}
Ouserkeydir=~/.ssh
Ouserkeyfile=${Ouserkeydir}/id_rsa

configHome=${XDG_CONFIG_HOME:-$HOME/.config}
sshRvncConfigHome=${configHome}/sshRvnc

if ! [[ -x /usr/sbin/sshd ]] || \
        ! cmdexists autossh  || \
        ! cmdexists x11vnc   || \
        ! cmdexists nc       || \
        ! cmdexists rsync    || \
        ! cmdexists xclip; then
    sudo apt update
    # must have for "remote desktop" using VNC on X
    sudo apt -y install autossh x11vnc
    if ! cmdexists autossh; then
        echo "ERROR ${prog}: autossh is needed but not available."
        exit 1
    fi
    if ! cmdexists x11vnc; then
        echo "ERROR ${prog}: x11vnc is needed but not available."
        exit 1
    fi
    # good to have
    sudo apt -y install netcat xclip rsync openssh-server tmux
fi

if [[ -z ${remoteMachineIP} ]]; then
    readPrompt='IP,PORT of remote machine for ssh -R: '
    remoteMachineConf=${sshRvncConfigHome}/remoteMachine.conf
    if [[ -f ${remoteMachineConf} ]]; then
        # read from file
        remoteMachine=$(cat ${remoteMachineConf})
        # (potential) modify
        read -e -p "${readPrompt}" -i ${remoteMachine} remoteMachine
        # write to file
        echo ${remoteMachine} >${remoteMachineConf}
    else
        # read from user interactively
        read -e -p "${readPrompt}" remoteMachine
        # write to file
        echo ${remoteMachine} >${remoteMachineConf}
    fi
    # parse
    remoteMachineIP=$(echo ${remoteMachine} | awk -F',' '{ print $1 }')
    remoteMachinePort=$(echo ${remoteMachine} | awk -F',' '{ print $2 }')
fi

# Check (-z: zero-I/O mode; scan only) if a TCP connection can be made
# to ${remoteMachineIP} on ${remoteMachinePort}.
if cmdexists nc && ! nc -z ${remoteMachineIP} ${remoteMachinePort}; then
    echo "ERROR ${prog}: nc could not make a TCP connection to ${remoteMachineIP} on port ${remoteMachinePort}."
    exit 1
fi

if [[ ! -f ${Ouserkeyfile} ]]; then
    mkdir -p ${Ouserkeydir}
    ssh-keygen -t rsa -b 4096 -f ${Ouserkeyfile}
fi
chmod u=rwx,go= ${Ouserkeydir}
chmod u=rw,go= ${Ouserkeyfile}{.pub,}

if [[ ! -f ${sshRvncConfigHome}/keyexchange_done ]]; then
mkdir -p ${sshRvncConfigHome}
xclip -selection clipboard -selection primary ${Ouserkeyfile}.pub && \
echo -e "${Ouserkeyfile}.pub (below) copied to primary and clipboard.
SEND it to remote by PASTING it (middle-click or Ctrl+v) into an EMAIL or MESSAGE.\n"
cat ${Ouserkeyfile}.pub

# This is to protect against a man-in-the-middle attack, ensuring that
# the public key we received is in fact from the RDHost.
echo -e '\nREAD the following for remote:\n'
ssh-keygen -lf ${Ouserkeyfile}.pub

read -rsp $'\nIf remote says "ALL OK," press y to continue' \
     -n1 keyPressed

if [[ ${keyPressed} == y ]]; then
    touch ${sshRvncConfigHome}/keyexchange_done
fi

fi

sshPort=$((2200 + ${helpeeIdx}))
vncPort=$((5900 + ${helpeeIdx}))

# autossh -M 0: Setting the monitor port to 0 turns the monitoring
# function off, and autossh will only restart ssh upon ssh's exit. For
# example, if you are using a recent version of OpenSSH, you may wish
# to explore using the ServerAliveInterval and ServerAliveCountMax
# options to have the SSH client exit if it finds itself no longer
# connected to the server. In many ways this may be a better solution
# than the monitoring port.
#
# ssh -f: fork into background just before command execution
# ssh -N: do not execute a remote command; useful for just forwarding
autossh                         \
    -M 0                        \
    -vvv -N                     \
    -R ${sshPort}:localhost:22  \
    -o ServerAliveInterval=30   \
    -o ServerAliveCountMax=6    \
    -o ConnectTimeout=30        \
    -o ExitOnForwardFailure=yes \
    -p ${remoteMachinePort}     \
    -i ${Ouserkeyfile}          \
    ${Ouser}@${remoteMachineIP} &

# separate vnc port forward in case it fails but ssh one succeeds
autossh                          \
    -M 0                         \
    -vvv -N                      \
    -R ${vncPort}:localhost:5900 \
    -o ServerAliveInterval=30    \
    -o ServerAliveCountMax=6     \
    -o ConnectTimeout=30         \
    -o ExitOnForwardFailure=yes  \
    -p ${remoteMachinePort}      \
    -i ${Ouserkeyfile}           \
    ${Ouser}@${remoteMachineIP} &

if [[ $? == 0 ]]; then
    # -forever: Keep listening for more connections rather than exiting
    # as soon as the first client(s) disconnect.
    x11vnc -verbose -nopw -localhost -forever -ncache 10 -display ':0'
fi

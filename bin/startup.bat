setlocal

set home=%userprofile%
set music=%home%\multimedia\music
set startMenuProgsTail=Microsoft\Windows\Start Menu\Programs
set sysStartMenuProgs=%AllUsersProfile%\%startMenuProgsTail%
set userStartMenuProgs=%AppData%\%startMenuProgsTail%
set userStartMenuProgsOpt=%userStartMenuProgs%\opt

set dir=%userStartMenuProgsOpt%
set lnk=debian
rem wslbridge2 is specific to wsltty and if it is running then so is
rem wsltty. wslservice, wslhost, and vmmemWSL (though all wsl-related)
rem remain (in the background) even after a wsl terminal (such as
rem wsltty) is closed, and hence cannot be used to check if wsltty is
rem already running.
set exe=wslbridge2
call :_is-exe-running %exe% || (
     call :_start_if_exist "%dir%" "%lnk%"
)

set dir=%userStartMenuProgsOpt%
set lnk=cygwin
set exe=mintty
call :_is-exe-running %exe% || (
     call :_start_if_exist "%dir%" "%lnk%"
)

set dir=%userStartMenuProgsOpt%
set lnk=firefox-esr
set exe=firefox
call :_is-exe-running %exe% || (
     call :_start_if_exist "%dir%" "%lnk%"
)

set dir=%userStartMenuProgsOpt%
set lnk=ungoogled-chromium
set exe=chrome
call :_is-exe-running %exe% || (
     call :_start_if_exist "%dir%" "%lnk%"
)

set exe=cmd
cmd /c start /d "%home%\bin" "" "%exe%"

set dir=%userStartMenuProgsOpt%
set lnks=emacs conemu sumatrapdf zeal flameshot goldendict
call :_start

set dir=%userStartMenuProgsOpt%
set lnks=vlc
for /f "delims=" %%f in ('dir /b "%music%\*.mp4"') do (
    rem We don't care which .mp4 is picked; so, let the for loop go
    rem through (no efficient or elegant way of breaking out of a cmd
    rem for loop). Our set sticks to (the full path of) the last .mp4
    rem returned by dir.
    if not "%%f" == "" set arg=%music%\%%f

    rem To access a var set INSIDE a for (), need delayed expansion
    rem and !var!, i.e.,
    rem
    rem setlocal enabledelayedexpansion
    rem echo "!arg!"
)
call :_start

set dir=%userStartMenuProgs%
set lnks=teams
call :_start

set dir=%sysStartMenuProgs%
set lnks=outlook
call :_start

exit /b

:_start
for %%p in (%lnks%) do (
    call :_is-exe-running %%p || (
         call :_start_if_exist "%dir%" "%%p" "%arg%"
    )
)
rem Clear 'stack' for next call of :_start 'func'
set dir=
set lnks=
set arg=
exit /b

:_is-exe-running
set exe=%~1
tasklist | find /i "%exe%" >nul
exit /b

:_start_if_exist
set dir=%~1
set prog=%~2
set arg=%~3
cd /d "%dir%"
rem "delims=": do NOT break on space in file names
for /f "delims=" %%f in ('dir /b "*%prog%*.lnk"') do if exist "%%f" (
    rem cmd (joke of a) shell does not remove "" from "%arg%" if %arg%
    rem is empty. Some programs (like conemu) behave differently
    rem whether they have an "" input (conemu "") vs. no input at all
    rem (conemu). So, treat these two cases differently explicitly.
    if "%arg%" == "" (
       cmd /c start /d "%home%" "" "%%f"
    ) else (
       cmd /c start /d "%home%" "" "%%f" "%arg%"
    )
)
exit /b

#!/bin/sh

tmux1=''
tmux2=''
if [ -n "$TMUX" ]; then tmux1='tmux;\e'; tmux2='\e\\'; fi

printf '\eP%b\e]10;#657B83\a%b'   $tmux1 $tmux2  # Foreground   -> base00
printf '\eP%b\e]11;#FDF6E3\a%b'   $tmux1 $tmux2  # Background   -> base3
printf '\eP%b\e]12;#DC322F\a%b'   $tmux1 $tmux2  # Cursor       -> red
printf '\eP%b\e]4;0;#073642\a%b'  $tmux1 $tmux2  # black        -> Base02
printf '\eP%b\e]4;8;#002B36\a%b'  $tmux1 $tmux2  # bold black   -> Base03
printf '\eP%b\e]4;1;#DC322F\a%b'  $tmux1 $tmux2  # red          -> red
printf '\eP%b\e]4;9;#CB4B16\a%b'  $tmux1 $tmux2  # bold red     -> orange
printf '\eP%b\e]4;2;#859900\a%b'  $tmux1 $tmux2  # green        -> green
printf '\eP%b\e]4;10;#586E75\a%b' $tmux1 $tmux2  # bold green   -> base01 *
printf '\eP%b\e]4;3;#B58900\a%b'  $tmux1 $tmux2  # yellow       -> yellow
printf '\eP%b\e]4;11;#657B83\a%b' $tmux1 $tmux2  # bold yellow  -> base00 *
printf '\eP%b\e]4;4;#268BD2\a%b'  $tmux1 $tmux2  # blue         -> blue
printf '\eP%b\e]4;12;#839496\a%b' $tmux1 $tmux2  # bold blue    -> base0 *
printf '\eP%b\e]4;5;#D33682\a%b'  $tmux1 $tmux2  # magenta      -> magenta
printf '\eP%b\e]4;13;#6C71C4\a%b' $tmux1 $tmux2  # bold magenta -> violet
printf '\eP%b\e]4;6;#2AA198\a%b'  $tmux1 $tmux2  # cyan         -> cyan
printf '\eP%b\e]4;14;#93A1A1\a%b' $tmux1 $tmux2  # bold cyan    -> base1 *
printf '\eP%b\e]4;7;#EEE8D5\a%b'  $tmux1 $tmux2  # white        -> Base2
printf '\eP%b\e]4;15;#FDF6E3\a%b' $tmux1 $tmux2  # bold white   -> Base3

#!/bin/bash

# This script is bound to a key, in ~/.config/openbox/lxde-rc.xml.

# https://unix.stackexchange.com/questions/50440/how-to-create-script-that-toggles-one-value-in-synclient

if synclient -l | egrep "TouchpadOff.*= *0">/dev/null; then
    synclient TouchpadOff=1
else
    synclient TouchpadOff=0
fi

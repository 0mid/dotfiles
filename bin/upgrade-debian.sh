#!/bin/bash

cat /etc/debian_version

# https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#upgrading-full

sudo sed -i.bak 's/bullseye/bookworm/g' /etc/apt/sources.list

# Clean /var/cache/apt/archives (packages previously downloaded for
# installation).
sudo apt clean

sudo apt update

# In some cases, doing the full upgrade directly might remove large
# numbers of packages that you will want to keep. We therefore
# recommend a two-part upgrade process: first a minimal upgrade to
# overcome these conflicts, then a full upgrade. This has the effect
# of upgrading those packages which can be upgraded without requiring
# any other packages to be removed or installed. The minimal system
# upgrade can also be useful when the system is tight on space and a
# full upgrade cannot be run due to space constraints.
sudo apt upgrade --without-new-pkgs
sudo apt autoremove
sudo apt autoclean

# Ensure system has enough space before continuing.
sudo apt -o APT::Get::Trivial-Only=true full-upgrade

# This will perform a complete upgrade of the system, installing the
# newest available versions of all packages, and resolving all
# possible dependency changes between packages in different releases.
# If necessary, it will install some new packages (usually new library
# versions, or renamed packages), and remove any conflicting obsoleted
# packages.
sudo apt full-upgrade

sudo apt autoremove
sudo apt autoclean

cat /etc/debian_version

# List obsolete packages, and purge them
sudo apt list '~o'
sudo apt purge '~o'

# List removed packages w/ leftover config files, and purge them.
sudo apt list '~c'
sudo apt purge '~c'

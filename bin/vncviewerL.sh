#!/bin/bash

source dotfiles_common.sh

usejumphost() {
    [[ ${directconnect} != 1 ]]
}

cmdensure ssvncviewer ssvnc
cmdensure vinagre

if usejumphost; then
    helpeeIdx=${1:-0}
    vncport=$((5900 + ${helpeeIdx}))
    jumphost=${2:-tunneller_jumphost}

    ssh -vvv -fN                           \
        -L ${vncport}:localhost:${vncport} \
        -o ServerAliveInterval=30          \
        -o ServerAliveCountMax=6           \
        -o ConnectTimeout=30               \
        -o ExitOnForwardFailure=yes        \
        ${jumphost}
fi

ssvncviewer localhost::${vncport}
# vinagre localhost::${vncport}

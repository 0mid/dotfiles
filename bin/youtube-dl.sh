#!/bin/bash

cd ~/videos
local URLS=()
for url in "$@"; do
    # in url, replace these substrings, as youtube-dl doesn't
    # recognize the URL if they are present.
    url=${url/ytdl:\/\//}       # drop ytdl://
    URLS+=("${url}")
done
parallel --bar youtube-dl {} ::: "${URLS[@]}"

@echo off
setlocal

net session >nul 2>&1 || (echo This script requires Admin.&goto :eof)

rem If srv is not previously installed, 'sc query srv' gives:
rem
rem [SC] EnumQueryServicesStatus:OpenService FAILED 1060:
rem The specified service does not exist as an installed service.
sc query cron >nul
if ERRORLEVEL 1060 (
   %USERPROFILE%\opt\cygwin\bin\cygrunsrv -I cron -p /usr/sbin/cron -a -n
   net start cron
) else (
   rem Although the cron service is started automatically (having been
   rem installed as a service above) after a Windows restart, it
   rem appears that it does NOT pick up the info from
   rem /etc/nsswitch.conf
   rem (https://cygwin.com/cygwin-ug-net/ntsec.html#ntsec-mapping-nsswitch),
   rem necessary for, e.g., ~ to expand to $HOME, as needed by ssh run
   rem (in a shell script called) by cron. It does pick up that info
   rem if the cron service is stopped and started again.
   rem
   rem TODO: Figure out why.
   net stop cron
   net start cron
)

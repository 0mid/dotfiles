@echo off
setlocal

rem set the HOME environment variable for the current user permanently
rem (setx) (written in registry) to be used by, e.g., Cygwin, Emacs,
rem etc. The 'set' one is in case we need to use it within this cmd
rem process (as the 'setx' one becomes available only in new cmd
rem processes).
set "home=%USERPROFILE%"
setx HOME "%home%"

for %%f in (bashrc bash_profile shell_aliases gitconfig gitignore tridactylrc) do (
  call :_ln-f "_%%f" "%home%\.%%f"
)

set "dstdir=%APPDATA%\mpv"
call :_mkdir "%dstdir%"
set "srcdir=_config\mpv"
for %%f in (mpv.conf input.conf) do (
  call :_ln-f "%srcdir%\%%f" "%dstdir%\%%f"
)

for %%f in (.minttyrc .wslconfig) do (
    call :_ln-f "windows-only\%%f" "%home%\%%f"
)

set "f=ConEmu.xml"
call :_ln-f "windows-only\%f%" "%APPDATA%\%f%"

call :_mkdir "%home%\bin"
for %%f in (lights.sh) do (
  call :_ln-f "windows-only\%%f" "%home%\bin\%%f"
)

for %%f in (startup.bat quickaccess.bat paths.bat privatizedir.bat term-light.sh term-dark.sh proxycmd.sh mouse-automation.py lights.bat) do (
  call :_ln-f "bin\%%f" "%home%\bin\%%f"
)

exit /b

:_mkdir
set "d=%~1"
if not exist "%d%" mkdir "%d%"
exit /b

:_ln-f
set "src=%~1"
set "dst=%~2"
if exist "%dst%" del "%dst%"
:_ln
set "src=%~1"
set "dst=%~2"
rem symlink (no /h) would require admin! (Windows: proprietary &
rem stupid, exhibit N)
mklink /h "%dst%" "%src%"
exit /b

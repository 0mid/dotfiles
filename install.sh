#!/bin/bash

# Copyright (C) 2022 Omid (gitlab.com/0mid)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

basedir="$(dirname "$(readlink -f "$0")")"
if [[ $(pwd -P) != ${basedir} ]]; then
    echo 'install.sh must be called directly under the dotfiles dir:'
    echo 'cd ~/dotfiles && ./install.sh'
    exit 1
fi

rmbrokenlinks() {
    # With GNU find, one can use the -xtype predicate to detect broken
    # symbolic links (-xtype uses the type of the target for symbolic
    # links, and reports l for broken links).
    local d
    for d in ~ ~/bin; do
        [[ -d $d ]] && find $d -maxdepth 1 -xtype l -delete
    done
    d=~/.config; [[ -d $d ]] && find $d -xtype l -delete
}

install_bin() {
    echo "${installing} bin scripts"

    bindir_src=${basedir}/bin
    bindir_dst=~/bin
    if [[ ${dryrun} != yes ]]; then
        mkdir -p -m 700 "${bindir_dst}"
    fi

    for script in \
        $(find "${bindir_src}" -type f -o -type l \
        | grep -vE '*.\.bat$|*.\.org$'); do
        echo "${installing} ${script}"

        if [[ ${dryrun} != yes ]]; then
            chmod 700 "${script}"
            ln -sf "${script}" "${bindir_dst}"
        fi
    done
}

install_firefox_userChrome_css() {
    echo "${installing} Firefox userChrome.css"

    dotmozfirefoxdir=~/.mozilla/firefox

    for profilename in $(grep 'Path=' ${dotmozfirefoxdir}/profiles.ini \
                             | sed 's/^Path=//'); do
        profiledir=${dotmozfirefoxdir}/${profilename}
        chromedir=${profiledir}/chrome
        echo "${installing} userChrome.css in ${chromedir}"

        if [[ ${dryrun} != yes ]]; then
            mkdir -p -m 700 ${chromedir}
            chmod 600 "userChrome.css"
            ln -sf "$(pwd)/userChrome.css" "${chromedir}"
        fi
    done
}

install_keyboard() {
    echo "${installing} X and TTY (virtual console) keyboard configs"
    if [[ ${dryrun} != yes ]]; then
        install_dotpatterns '_keyboard'

        # Make ./install.sh keyboard independent of ./install.sh bin.
        mkdir -p -m 700 ~/bin
        ln -sf ${basedir}/bin/setxkbmap.sh ~/bin
        ln -sf ${basedir}/bin/configtrackpoint.sh ~/bin

        bash bin/setxkbmap.sh

        # We MUST be superuser and at a virtual console (aka TTY) to
        # run setupcon. If we are either not superuser or not at a
        # console, setupcon will have no effect. Without superuser,
        # loadkeys step of setupcon will fail (silently, unless run
        # with -v); without being at a console test_console function
        # in $(which setupcon) will fail. As a result of having to be
        # superuser, to have setupcon read the current user's
        # $HOME/.keyboard, we have to preserve that user's environment
        # (in particular their $HOME), e.g., with sudo -E.
        #
        # setupcon steps: {~/.keyboard or /etc/default/keyboard}
        # ---ckbcomp---> console keymap type ---loadkeys---> kernel
        # console(s)
        #
        # WARNING: Note that because the changes affect all the
        # virtual consoles, they also outlive your session. This means
        # that even at the login prompt the key bindings may not be
        # what the user expects [man loadkeys].
        sudo -E setupcon
    fi
}

install_trackpoint() {
    echo "${installing} track point (knob) config (speed, sensitivity, etc.)"
    if [[ ${dryrun} != yes ]]; then
        bin/configtrackpoint.sh
    fi
}

install_passes() {
    echo "${installing} passes"
    if [[ ${dryrun} != yes ]]; then
        ln -sf ~/.keys/.authinfo.gpg ~/
    fi
}

# Install an individual _file
install_dotfile() {
    local _file="$1"
    local _filestriped="${_file#./*}"
    local src_file="$(pwd)/${_filestriped}"
    local dotfile=".${_filestriped#_*}"
    # As maybe seen by running this script with set -x, using ~
    # instead of $HOME below leads to bash single quoting dstdotfile
    # and hence ln below not working, e.g.:
    #
    # local 'dstdotfile=~/.zshrc'
    # ln -f -s /home/omid/dotfiles/.zshrc '~/.zshrc'
    local dstdotfile="$HOME/${dotfile}"

    echo "${installing} ${_file}"

    if [[ ${dryrun} != yes ]]; then
        mkdir -p -m 700 "$(dirname "${dstdotfile}")"
        chmod 600 "${_file}"

        if [[ "${src_file}" =~ 'gtk-3.0/bookmarks' ]]; then
            sed $(printf 's/$USER/%s/g' $USER) "${src_file}" >"${dstdotfile}"
        else
            ln -sf "${src_file}" "${dstdotfile}"
        fi
    fi
}

install_dotpatterns() {
    for pattern in "$@"; do

    # Install each _-prefixed file (creating directories as needed)
    for src in $(find . -name "${pattern}*" | sort); do
        if [ -d "${src}" ]; then
            # f: regular file, -o: or, l: symbolic link
            for _file in $(find "${src}" -type f -o -type l | sort); do
                install_dotfile "${_file}"
            done
        else
            install_dotfile "${src}"
        fi
    done

    done
}

install_dotshells() {
    install_dotpatterns '_shell' '_bash' '_zsh' '_tmux'
}

install_dotfiles() {
    install_dotpatterns '_'
    reload_Xresources
}

install_Xresources() {
    install_dotpatterns '_Xresources'
    echo "#define FONT_SIZE 12" > ~/.Xresources.h

    reload_Xresources
}

reload_Xresources() {
    # Reload .Xresources
    if [[ -n "$DISPLAY" ]]; then
        xrdb -I$HOME ~/.Xresources 2> /dev/null
    fi
}

# {dotshells, Xresources} is a subset of dotfiles
part_list=(dotshells dotfiles bin firefox_userChrome_css keyboard trackpoint passes Xresources)

part_list_disjoint=(dotfiles bin firefox_userChrome_css keyboard passes)

prog="$(basename ${BASH_SOURCE[0]})"

usage() {
    cat << EOF
Usage: ${prog} [OPTIONS] PART
where PART is one of "${part_list[@]} ALL".
In particular, ALL means install all of the parts in the list.

OPTIONS:
  --dry-run
      dry run
  -h, --help
      show this message
EOF
    exit $1
}

shortopts=h
longopts=help,dry-run
args="$(getopt --name ${prog} -o ${shortopts} --long ${longopts} -- "$@")"
[[ $? -ne 0 ]] && usage 1
eval set -- "${args}"

installing="Installing"
install_part='install_${part}'
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h | --help)
            usage 0
            ;;
        --dry-run)
            dryrun=yes
            installing="[dry-run] Installing"
            ;;
        --)
            # end of options
            shift
            break
            ;;
    esac
    shift
done

if [[ $# -gt 0 ]]; then
    # process remaining args (i.e., non '-' or '--')
    for arg in "$@"; do
        if [[ ${arg} == ALL ]]; then
            for part in ${part_list_disjoint[@]}; do
                eval ${install_part}
            done
            rmbrokenlinks
            exit $?
        fi
        part=${arg}
        # if ${part_list} contains ${part}
        if [[ " ${part_list[@]} " =~ " ${part} " ]]; then
            eval ${install_part}
        else
            usage 1
        fi
    done
    rmbrokenlinks
else
    usage 1
fi

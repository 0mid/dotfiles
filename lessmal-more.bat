@echo off
setlocal

rem Hack to check if run by Admin [https://stackoverflow.com/a/16285248].
net session >nul 2>&1 || (echo This script requires Admin.&goto :eof)

rem Yes, this 'HKCU' key needs Admin!
set key=HKCU\Software\Policies\Microsoft\Windows\DataCollection
reg delete "%key%"                       /f /va

set key=HKLM\Software\Policies\Microsoft\Windows\DataCollection
reg add "%key%"\AllowDesktopAnalyticsProcessing             /f /v Value /t REG_DWORD /d 0
reg add "%key%"\LimitEnhancedDiagnosticDataWindowsAnalytics /f /v Value /t REG_DWORD /d 1
reg add "%key%"\AllowTelemetry                              /f /v Value /t REG_DWORD /d 0
reg add "%key%"\AllowDeviceNameInTelemetry                  /f /v Value /t REG_DWORD /d 0
reg add "%key%"\DisableTelemetryOptInChangeNotification     /f /v Value /t REG_DWORD /d 0
reg add "%key%"\AllowWUfBCloudProcessing                    /f /v Value /t REG_DWORD /d 0
reg add "%key%"\AllowUpdateComplianceProcessing             /f /v Value /t REG_DWORD /d 0
reg add "%key%"\DisableTelemetryOptInSettingsUx             /f /v Value /t REG_DWORD /d 0

set key=HKLM\Software\Policies\Microsoft\Windows\Personalization
reg add "%key%"\NoChangingLockScreen                        /f /v Value /t REG_DWORD /d 0

for %%s in (A180WD DiagTrack) do (
rem A180WD (Aternity collects and transmits all you do in your browser
rem and Windows ("no personal/sensitive/identifying/etc. information
rem is transmitted", of course) "to help resolve your computer issues,
rem keep you safe from attacks, data breaches, ...".
    sc query state=all | find /i "%%s" && sc stop "%%s" && sc delete "%%s"
)

for %%i in ("rsiguard.exe" "A180WD.exe") do (
tasklist | find /i "%%~i" >nul && taskkill /f /im "%%~i"
)

for %%d in ("%programfiles(x86)%\Aternity Information Systems" "%programfiles(x86)%\RSIGuard") do (
if exist "%%~d" rmdir /s /q "%%~d"
)

@rem https://learn.microsoft.com/en-us/archive/blogs/josebda/windows-server-2012-file-server-tip-disable-8-3-naming-and-strip-those-short-names-too
fsutil 8dot3name set C: 1

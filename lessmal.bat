@echo off
rem A one-shot, first-time Windows configuration script that disables some
rem Windows mal/misfeatures [based on Chris Wellons'].
rem
rem Posted to https://superuser.com/a/1724944/86394

setlocal

set WindowsCurrentVersion=HKCU\Software\Microsoft\Windows\CurrentVersion
set ControlPanel=HKCU\Control Panel

set key=%WindowsCurrentVersion%\Explorer
reg add "%key%" /f /v AltTabSettings              /t REG_DWORD /d 1
reg add "%key%" /f /v DisableSearchBoxSuggestions /t REG_DWORD /d 1
set key=%WindowsCurrentVersion%\Explorer\Taskband
reg delete "%key%" /f /v Favorites
set key=%WindowsCurrentVersion%\Explorer\Advanced
reg add "%key%" /f /v TaskbarGlomLevel          /t REG_DWORD /d 2
reg add "%key%" /f /v HideFileExt               /t REG_DWORD /d 0
reg add "%key%" /f /v ShowCortanaButton         /t REG_DWORD /d 0
reg add "%key%" /f /v ShowTaskViewButton        /t REG_DWORD /d 0
reg add "%key%" /f /v StoreAppsOnTaskbar        /t REG_DWORD /d 0
reg add "%key%" /f /v MultiTaskingAltTabFilter  /t REG_DWORD /d 3
reg add "%key%" /f /v JointResize               /t REG_DWORD /d 0
reg add "%key%" /f /v SnapFill                  /t REG_DWORD /d 0
reg add "%key%" /f /v SnapAssist                /t REG_DWORD /d 0
reg add "%key%" /f /v TaskbarAnimations         /t REG_DWORD /d 0
reg add "%key%" /f /v ShowCopilotButton         /t REG_DWORD /d 0
set key=%WindowsCurrentVersion%\Explorer\Advanced\TaskbarDeveloperSettings
reg add "%key%" /f /v TaskbarEndTask            /t REG_DWORD /d 1
set key=%WindowsCurrentVersion%\Feeds
reg add "%key%" /f /v ShellFeedsTaskbarViewMode /t REG_DWORD /d 2
set key=%WindowsCurrentVersion%\Search
reg add "%key%" /f /v SearchboxTaskbarMode      /t REG_DWORD /d 0
reg add "%key%" /f /v BingSearchEnabled         /t REG_DWORD /d 0
reg add "%key%" /f /v AllowSearchToUseLocation  /t REG_DWORD /d 0
reg add "%key%" /f /v CortanaConsent            /t REG_DWORD /d 0
set key=%WindowsCurrentVersion%\SearchSettings\IsDynamicSearchBoxEnabled
reg add "%key%" /f /v IsDynamicSearchBoxEnabled /t REG_DWORD /d 0
set key=%ControlPanel%\Desktop
reg add "%key%" /f /v CursorBlinkRate           /t REG_SZ    /d -1
reg add "%key%" /f /v MenuShowDelay             /t REG_SZ    /d 0
reg add "%key%" /f /v UserPreferencesMask       /t REG_BINARY /d 9012078010000000
set key=%ControlPanel%\Desktop\WindowMetrics
reg add "%key%" /f /v MinAnimate                /t REG_SZ    /d 0
set key=%ControlPanel%\Accessibility
reg add "%key%" /f /v DynamicScrollbars         /t REG_DWORD /d 0
set key=%WindowsCurrentVersion%\CapabilityAccessManager\ConsentStore
reg add "%key%"\location                 /f /v Value /t REG_SZ /d 0
reg add "%key%"\webcam                   /f /v Value /t REG_SZ /d 0
reg add "%key%"\microphone               /f /v Value /t REG_SZ /d 0
reg add "%key%"\userNotificationListener /f /v Value /t REG_SZ /d 0
reg add "%key%"\activity                 /f /v Value /t REG_SZ /d 0
reg add "%key%"\userAccountInformation   /f /v Value /t REG_SZ /d 0
reg add "%key%"\contacts                 /f /v Value /t REG_SZ /d 0
reg add "%key%"\appointments             /f /v Value /t REG_SZ /d 0
reg add "%key%"\phoneCallHistory         /f /v Value /t REG_SZ /d 0
reg add "%key%"\email                    /f /v Value /t REG_SZ /d 0
reg add "%key%"\userDataTasks            /f /v Value /t REG_SZ /d 0
reg add "%key%"\chat                     /f /v Value /t REG_SZ /d 0
reg add "%key%"\radios                   /f /v Value /t REG_SZ /d 0
reg add "%key%"\bluetoothSync            /f /v Value /t REG_SZ /d 0
reg add "%key%"\appDiagnostics           /f /v Value /t REG_SZ /d 0
reg add "%key%"\documentsLibrary         /f /v Value /t REG_SZ /d 0
reg add "%key%"\picturesLibrary          /f /v Value /t REG_SZ /d 0
reg add "%key%"\videosLibrary            /f /v Value /t REG_SZ /d 0
reg add "%key%"\broadFileSystemAccess    /f /v Value /t REG_SZ /d 0
set key=%ControlPanel%\International\User Profile
reg add "%key%"                          /f /v HttpAcceptLanguageOptOut /t REG_DWORD /d 1
set key=%WindowsCurrentVersion%\AdvertisingInfo
reg delete "%key%"                       /f /va
reg add "%key%"                          /f /v Enabled /t REG_DWORD /d 0
set key=HKCU\Software\Microsoft\Internet Explorer\International
set val=AcceptLanguage
reg query "%key%" /v "%val%" 2>nul && (
reg delete "%key%"                       /f /v "%val%"
)

set firefox_profiles_dir=%AppData%\Mozilla\Firefox\Profiles
set chromium_user_data_dir=%AppData%\..\Local\Chromium\User Data

set firefox_extension_name=Aternity
set firefox_extension_xpis=%firefox_profiles_dir%\*Aternity*.xpi

rem This is to stop other accounts (e.g., Administrators or SYSTEM)
rem from injecting malware into extensions dir.
for /f "delims=" %%d in ('dir /b /s "%firefox_profiles_dir%"\*extensions') do (
call bin\privatizedir "%%d" | findstr "Failed processing"
)

for /f "delims=" %%d in ('dir /b /s "%chromium_user_data_dir%"\*extensions') do (
call bin\privatizedir "%%d" | findstr "Failed processing"
)

set startMenuProgs=%AppData%\Microsoft\Windows\Start Menu\Programs
set startMenu_opt=%startMenuProgs%\opt

call :_del_firefox_extension

set chromium_extension_name=Aternity
set chromium_extension_dirs=%chromium_user_data_dir%\*midemlcjafiljnaicbjpdlpiahpoalen*
call :_del_chromium_extension

rem This 'other' one may also show up in chrome://extensions.
set chromium_extension_name=Aternity
set chromium_extension_dirs=%chromium_user_data_dir%\*fpbbmpnghdjflhjlgdgeepepmhjojmha*
call :_del_chromium_extension

set chromium_extension_name=LayerX
set chromium_extension_dirs=%chromium_user_data_dir%\*dmpgopmhgecgfpbiphgfobeaeaodaidj*
call :_del_chromium_extension

for %%i in ("msedge.exe") do (
tasklist | find /i "%%~i" >nul && taskkill /f /im "%%~i"
)

rem Reload
taskkill /f /im explorer.exe
start explorer.exe

exit /b

rem NB: cmd is brain dead; in particular, it won't find the labels in
rem a batch file unless the file has DOS (CRLF) line endings.

:_del_firefox_extension
rem Proceed only if the .xpi files exist.
dir /b /s "%firefox_extension_xpis%" 2>nul || exit /b

echo Attemping to delete %firefox_extension_name% (spyware) Firefox addon.
echo This requires CLOSING and RE-OPENING Firefox...
taskkill /f /im firefox.exe 1>nul 2>1 && del /f /s "%firefox_extension_xpis%" 2>nul
rem Start Firefox again (this restores previous tabs by default) and
rem open the about:support page, which gives a detailed, textual
rem summary of the innards of Firefox.
set app=firefox-esr
if exist "%startMenu_opt%\%app%" (
   start /d "%startMenu_opt%" "" %app% "about:support"
)
exit /b

:_del_chromium_extension
dir /b /s "%chromium_extension_dirs%" 2>nul || exit /b

echo Attemping to delete %chromium_extension_name% (spyware) Chromium extension.
echo This requires CLOSING and RE-OPENING Chromium...
taskkill /f /im chrome.exe 1>nul 2>1 && (
  rem ^ in 2^>nul is to escape > inside for.
  rem Without "tokens=*" %%d captures only up to the first white space.
  for /f "tokens=*" %%d in ('dir /s /b "%chrome_extension_dirs%" 2^>nul') do (
      if exist "%%d" rmdir /s /q "%%d"
  )
)
rem Start Chromium again (asks to restore pages) and open the
rem chrome://extensions page.
set app=ungoogled-chromium
if exist "%startMenu_opt%\%app%" (
   start /d "%startMenu_opt%" "" %app% "chrome://extensions"
)
exit /b

#!/bin/bash

# Note: bin/term-{light,dark}.sh and hence this script 'HANG' in
# ConEmu.

windows_theme_bat=$TMP/theme-${RANDOM}.bat
emacs_theme_bat=$TMP/emacs-theme-${RANDOM}.bat

if [[ "$1" == "off" || "$1" == "--off" ]]; then
    term_theme='dark'
    emacs_theme='solarized-dark'
    windows_reg_dword=0
    sumatra_txt_color='#839496'
    sumatra_bak_color='#002b36'
else
    # default to lights on
    term_theme='light'
    emacs_theme='solarized-light'
    windows_reg_dword=1
    sumatra_txt_color='#657b83'
    sumatra_bak_color='#fdf6e3'
fi

[[ -f ~/bin/term-${term_theme}.sh ]] && bash ~/bin/term-${term_theme}.sh

# Windows-native Emacs (emacs.exe or emacsclient.exe) cannot be called
# in Cygwin directly.
#
# cd ~/opt/emacs*/bin && ls -lah emacsclient.exe && emacsclient.exe --version
# 67K Mar 26  2021 emacsclient.exe
# bash: emacsclient.exe: command not found
#
# Prefixing with cygstart didn't work, either.
cat <<EOF >"${emacs_theme_bat}"
@echo off
setlocal

cd %USERPROFILE%\opt\emacs*\bin
emacsclient -e "(load-theme '${emacs_theme} t)" >nul 2>&1
EOF
chmod u+x "${emacs_theme_bat}" && "${emacs_theme_bat}"; rm "${emacs_theme_bat}"

cd ~/opt/sumatrapdf*
sed -i.bak "/TextColor/ s/=.*/= ${sumatra_txt_color}/" SumatraPDF-settings.txt
sed -i.bak "/BackgroundColor/ s/=.*/= ${sumatra_bak_color}/" SumatraPDF-settings.txt

# This used to work fully (in Window 10 whatever) without a reload of
# explorer.exe, but now it's broken in Windows 11 whatever: it only
# works partly (File Explorer and Taskbar themes do NOT change;
# Firefox's, via Dark Reader, sometimes does and sometimes does not;
# etc.). I learned by accident that if one kills explorer, this
# setting seems to get propagated fully as it used to, hence the
# Reload part of the script below.
cat <<EOF >"${windows_theme_bat}"
@echo off
setlocal

set "key=HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize"
reg add "%key%" /f /v SystemUsesLightTheme            /t REG_DWORD /d ${windows_reg_dword}
reg add "%key%" /f /v AppsUseLightTheme               /t REG_DWORD /d ${windows_reg_dword}

rem Reload
taskkill /f /im explorer.exe
start explorer.exe
EOF

# Cygwin runs an executable .bat through cmd /c
chmod u+x "${windows_theme_bat}" && "${windows_theme_bat}"; rm "${windows_theme_bat}"

# ConEmu (Free Software) is dumb enough not to update its GUI
# automatically when its config file is updated. So, no point
# including it here.

# Change dir back to $HOME (otherwise, Conemu will start in
# SumatraPDF's dir, as we cd'ed into that above).
cd
